# Dexter Contract Issues

These are currently unresolved and unimplemented issues in the Dexter contracts.

- It is currently using a toy token contract instead of dependening on a 
  standardized one (waiting on Morley to support transferTokens entry point).

- Token to Token swapping is not implemented.

- Need a factory contract to create Dexter exchanges. This is also needed for 
  Token to Token swap to broker the exchange between to Dexter contracts.
  This will probably also require the entry point feature in Morley.

- Dexter is currently using sender, this is considered a bad design choice and 
  does not allow for proxies to init an exchange. We could have a list of 
  permitted contracts that can initiate an exchange. 
