Dexter is a Tezos on-chain, decentralized exchange for the native Tezos token
`tez` (or `XTZ`) and assets built on the Tezos block chain. Currently it supports tokens
from [FA1.2 contracts](https://gitlab.com/tzip/tzip/blob/master/Proposals/TZIP-0007/FA1.2.md),
but will support others in the future.

This tutorial assumes you are familiar with the using the Tezos client. If you 
are not, please read our [first](https://gitlab.com/camlcase-dev/michelson-tutorial/tree/master/01) 
and [second](https://gitlab.com/camlcase-dev/michelson-tutorial/tree/master/02) 
Michelson tutorials. They show you how to download the Tezos client, create 
accounts on the Babylonnet, transfer tokens and originate contracts.

# Originating the Dexter Contract from the Command Line

All of the example are written for using the babylonnet [docker image](https://tezos.gitlab.io/introduction/howtoget.html).
If instead of using the docker image, you use compiled the Tezos source code, 
then make sure that thte `tezos-node` is running, and change all the commands 
below from `~/babylonnet.sh client` to  `tezos-client` and change container 
paths like `container:contracts/fa1.2.tz` to file paths `./contracts/fa1.2.tz`. 

Dexter is composed of a single exchange contract and depends on the 
FA1.2 token contract.

Clone the dexter repository and move to the main directory:

```bash
git clone git@gitlab.com:camlcase-dev/dexter.git
cd dexter/dexter-contracts-ligo
```

The Dexter contract is written in [Pascaligo](https://ligolang.org/) and 
compiled to Michelson, the native smart contract language of Tezos. The 
compiled Michelson versions of the contracts are located at 
`dexter-contracts-ligo/contracts`. You can also compile the `exchange.ligo` 
and `fa1.2.ligo` contracts yourself if you download the ligo compiler.

Compile the contracts with ligo:
```bash
$ ligo compile-contract exchange.ligo main
$ ligo compile-contract fa1.2.ligo main
```

## Originating an FA1.2 Token Contract

First, lookup the address of the account with which you want to originate the 
FA1.2 token and give ownership of the coins to.

```bash
$ ~/babylonnet.sh client list known addresses

simon: tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3 (unencrypted sk known)
bob: tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i (unencrypted sk known)
alice: tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn (unencrypted sk known)
```

I have three accounts on the Babylonnet. I am going to originate a token with
1,000,000 tokens. I will place all of them in Alice's custody. Copy the command 
below and replace `alice` with the name of your account and 
`"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"` with your account's address. You 
can change the amount of tokens to whatever number you like. Make sure the two 
numbers match or there will be untransferrable tokens in the contract.

I am calling this token `Tezos Gold`, but there is no storage field in FA1.2 for
the name or the symbol of the token.

```bash
$ ~/babylonnet.sh client originate contract tezosGold \
                  transferring 0 from alice \
                  running container:contracts/fa1.2.tz \
                  --init 'Pair {Elt "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" (Pair {} 1000000)} 1000000' \ 
                  --burn-cap 5
```

If you want to originate another contract stored as `tezosGold` in your local 
machine, you can overwrite it by adding `--force` to the end of the command. 
Just remember that it will disassociate the old contract address from 
the symbol, but that contract will still exist on the Tezos blockchain.

Now Alice can transfer Tezos Gold tokens to another account. In the command 
below, the first account address is the owner Alice and the second one is the 
to address Simon. The numerical value is the number of Tezos Gold tokens that 
Alice will send to Simon.

```bash
~/babylonnet.sh client transfer 0 
                from alice \
                to tezosGold \
                --entrypoint 'transfer' \
                --arg 'Pair (Pair "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3") 100' \
                --burn-cap 1
```

Finally, you can check the Tezos Gold balance of each account.

```bash
$ ~/babylonnet.sh client get big map value \ 
                  for '"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"' \
                  of type 'address' in tezosGold

$ ~/babylonnet.sh client get big map value \ 
                  for '"tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3"' \
                  of type 'address' in tezosGold
```

You can create multiple token contracts. Just copy the first command and replace 
the values as you like.

## Originate the Dexter Exchange Contract

When we originate the Dexter exchange contract, we need to include the 
address of the FA1.2 token contract we just originated. We can look up the 
address in the command line.

```bash
$ ~/babylonnet.sh client list known contracts

tezosGold: KT1KxdSabTUDVXvnXkyW7udQvp6FjG9FcrBm
```

Now we can originate the exchange contract. Replace `alice` with the name of 
your account, the first address is the broker contract address and the second
address is the token contract address.

```
$ ~/babylonnet.sh client originate contract tezosGoldExchange \
                  transferring 0 from alice \
                  running container:contracts/exchange.tz \
                  --init 'Pair {} (Pair (Pair (Pair None "KT1XjSoibW59pCAELdH9QEEgFzRb59MNbJiH") (Pair 0 0)) {})' \
                  --burn-cap 20
```

Let's add some liquidity. In order to do so, Alice must first approve an 
allowance for Tezos Gold Exchange to spend her Tezos Gold tokens. The first value 
in the parameter pair is the address of whom we want to give an allowance to and 
the second value is the amount of the allowance.

```bash
$ ~/babylonnet.sh client transfer 0 \
                  from alice \
                  to tezosGold \
                  --entrypoint 'approve' \
                  --arg 'Pair "KT1VbT8n6YbrzPSjdAscKfJGDDNafB5yHn1H" 200' \
                  --burn-cap 1
```

In the command below, `transfer 10` means Alice is  adding 10 tez to the 
liquidity pool. The next number is the minimum amount of 
liquidity you want to mint (if that number is not reached it will fail and 
return the tez to you). The liquidity is an internal number, referred to as DEX 
in the contract, that the exchange tracks. Then the next number is the maximum 
number of Tezos Gold tokens you want to add to the liquidity pool. When the 
liquidity pool is empty it will add the maximum amount if the account has that 
many tokens. And finally the date is the deadline for which you would like the 
transaction to occur by. 

The values in the parameter are: address of who will own the liquidity from
tezosGoldExchange, the minimum amount of liquidity the user wants to create,
the maximum number of FA1.2 tokens they want to deposit, and the deadline.

```bash
$ ~/babylonnet.sh client transfer 10 
                  from alice \
                  to tezosGoldExchange \
                  --entrypoint 'addLiquidity' \
                  --arg 'Pair (Pair "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" 1) (Pair 100 "2020-06-29T18:00:21Z")' \
                  --burn-cap 1
```

In this transaction, Alice has initiated the liquidity pool with 10 tez and 100 
Tezos Gold tokens meaning that she has valued Tezos Gold at 10-1. Her ownership
of liquidity in this exchange contract is represented by an internal liquidity
token called LQT.

We can query her ownership with the following command.

```bash
$ ~/babylonnet.sh client get big map value \
                  for '"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"' \
                  of type 'address' \
                  in tezosGoldExchange

Pair (Pair {} 10000000) None
```

Alice holds 10000000 LQT tokens.

With another account you can try buying some Tezos Gold tokens from the exchange. 
`transfer 5` means Simon spends one tez. The address is who will receive the 
Tezos Gold tokens, in this case Simon. `5` means he wants to receive at least 1 
Tezos Gold for his five tez and the date is the deadline for the transaction to 
occur. If the deadline has passed or Simon's minimum request has not been met, 
the transaction will fail.

```bash
$ ~/babylonnet.sh client transfer 5 \
                  from simon \
                  to tezosGoldExchange \
                  --entrypoint 'xtzToToken' \
                  --arg 'Pair (Pair "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" 1) "2020-06-29T18:00:21Z"' \
                  --burn-cap 1
```

After the xtzToToken transaction, you can confirm Simon's current balance of
Tezos Gold with the following command:

```bash
tezos-client get big map value for '"tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3"' \
             of type 'address' in tezosGold
```

We can also purchase XTZ with Tezos Gold tokens. Make sure `transfer` is set to `0` or you
are giving free XTZ to the exchange contract. The first number after 
`transfer 0` is the number of tokens you want to sell, then the minimum amount 
of XTZ (in mutez) that you want to receive (one tez is 1,000,000 mutez), and 
finally the deadline by which the transfer should occur.

```bash
$ ~/babylonnet.sh client transfer 0 
                  from simon \
                  to tezosGoldExchange \
                  --entrypoint 'tokenToXtz' \
                  --arg 'Pair (Pair (Pair "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3") (Pair 40 1)) "2020-06-29T18:00:21Z"' \
                  --burn-cap 1
```

The last thing we can do is remove liquidity from the exchange and receive an 
equivalent amount of XTZ and Tezos Gold. The first value is the address of the
LQT (the internal exchange token for keeping track of liquidity) owner, the 
second is the address of who will receive the XTZ and the Tezos Gold, the third 
is the amount of LQT you want to get rid of, the fourth is the minimum amount 
of XTZ (in mutez) you want to withdraw (one tez is 1,000,000 mutez) and the fifth
is the minimum amount of Tezos Gold tokens you with to withdraw. If the deadline 
is passed of if any of the minimums are not met, then the transaction will fail.

```bash
$ ~/babylonnet.sh client transfer 0 from alice to tezosGoldExchange \
                  --entrypoint 'removeLiquidity' \
                  --arg 'Pair (Pair (Pair "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn") (Pair 5000000 1)) (Pair 1 "2020-06-29T18:00:21Z")' \
                  --burn-cap 1
```

# Appendix A: FA1.2 Token Contract Commands

## Originate a FA1.2 Token Contract

Reference:

```bash
$ ~/babylonnet.sh client originate contract <token-contract-name> \
                                   transferring 0 from <owner> \
                                   running container:contracts/fa1.2.tz \
                                   --init 'Pair {Elt "<owner>" (Pair {} <token-total-amount>)} <token-total-amount>' 
                                   --burn-cap 5
```

Example:

```bash
$ ~/babylonnet.sh client originate contract tezosGold \
                                   transferring 0 from alice \
                                   running container:contracts/fa1.2.tz \
                                   --init '(Pair {Elt "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" (Pair 1000000 {})} (Pair 1000000 (Pair "Tezos Gold" "TGD")))' --burn-cap 2
```

## Transfer FA1.2 Tokens

Reference:

```bash
$ ~/babylonnet.sh client transfer 0 from <sender> \
                                    to <token-contract-name> \
                                    --entrypoint 'transfer' \
                                    --arg 'Pair (Pair "<owner>" "<destination>") <value>' --burn-cap 1
```

The sender must be the owner of the tokens or have an allownace from the owner 
of the tokens.

Example:

```bash
$ ~/babylonnet.sh client transfer 0 from alice \
                                    to tezosGold \ 
                                    --entrypoint 'transfer' \
                                    --arg 'Pair (Pair "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3") 100' \ 
                                    --burn-cap 1
```

## View FA1.2 Account Balance

Reference:

```bash
$ ~/babylonnet.sh client get big map value for '"<owner>"' \
                         of type 'address' in <token-contract-name>
```

Example:

```bash
$ ~/babylonnet.sh client get big map value for '"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"' \
                         of type 'address' in tezosGold
```

## Approve FA1.2 Allowance

Reference:

```bash
tezos-client transfer 0 from alice \
                        to tezosGold \ 
                        --entrypoint 'approve' \ 
                        --arg 'Pair "<spender>" <allowance>' \
                        --burn-cap 1
```

Example:

```bash
tezos-client transfer 0 from alice \
                        to tezosGold \ 
                        --entrypoint 'approve' \ 
                        --arg 'Pair "KT1VbT8n6YbrzPSjdAscKfJGDDNafB5yHn1H" 200' \
                        --burn-cap 1
```

# Appendix B: Dexter Exchange

## Originate a Dexter Exchange

Reference:
```bash
$ ~/babylonnet.sh client originate contract <exchange-contract-name> \
                                   transferring 0 from alice \
                                   running container:contracts/exchange.tz \
                                   --init 'Pair {} (Pair (Pair (Pair None "<token_address>") (Pair 0 0)) {})' \ 
                                   --burn-cap 20
```

Example:
```bash
$ ~/babylonnet.sh client originate contract tezosGoldExchange \
                                   transferring 0 from alice \
                                   running container:contracts/exchange.tz \
                                   --init 'Pair {} (Pair (Pair (Pair None "KT1XjSoibW59pCAELdH9QEEgFzRb59MNbJiH") (Pair 0 0)) {})' \ 
                                   --burn-cap 20
```

## Add Liquidity

Reference:
```bash
$ ~/babylonnet.sh client transfer <xtz> 
                  from <sender> \ 
                  to <exchange-contract-name> \
                  --arg 'Pair (Pair "<owner>" <min_lqt_created>) (Pair <max_tokens_deposited> "<deadline>")' \
                  --burn-cap 1
```

Example:
```bash
$ ~/babylonnet.sh client transfer 10 
                  from alice \
                  to tezosGoldExchange \
                  --entrypoint 'addLiquidity' \
                  --arg 'Pair (Pair "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" 1) (Pair 100 "2020-06-29T18:00:21Z")' \
                  --burn-cap 1
```

## Remove Liquidity

Reference:
```bash
$ ~/babylonnet.sh client transfer 0 from <sender> \
                                    to <exchange-contract-name> \
                                    --entrypoint 'removeLiquidity' \
                                    --arg 'Pair (Pair (Pair "<owner>" "<to>") (Pair <lqt_burned> <min_xtz_withdrawn>)) (Pair <min_tokens_withdrawn> "<deadline>")' \
                                    --burn-cap 1
```

If the sender is not the owner, then they need to have an allowance approval 
from the owner in order to burn the LQT amount specified.

Example:
```bash
$ ~/babylonnet.sh client transfer 0 from alice \
                                    to tezosGoldExchange \
                                    --entrypoint 'removeLiquidity' \
                                    --arg 'Pair (Pair (Pair "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn") (Pair 5000000 1)) (Pair 1 "2020-06-29T18:00:21Z")' \
                                    --burn-cap 1
```

## XTZ to FA1.2 Token

Reference:
```bash
$ ~/babylonnet.sh client transfer <xtz> from <buyer> \ 
                                        to <exchange-contract-name> \
                                        --arg 'Right (Left (Pair <min-tokens-required> "<deadline>"))' \
                                        --burn-cap 1
```

Example:
```bash
$ ~/babylonnet.sh client transfer 2 from bob \ 
                                    to tezosGoldExchange \
                                    --entrypoint 'xtzToToken' \
                                    --arg 'Pair (Pair "tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i" 1) "2020-06-29T18:00:21Z"' \
                                    --burn-cap 1
```

## FA1.2 Token to XTZ

Reference:
```bash
$ ~/babylonnet.sh client transfer 0 from <sender> \
                                    to <exchange-contract-name> \
                                    --entrypoint 'tokenToXtz' \
                                    --arg 'Pair (Pair (Pair "<owner>" "<to>") (Pair <tokens_sold> <min_xtz_bought>)) "<deadline>"' \
                                    --burn-cap 1
```

Example:
```bash
$ ~/babylonnet.sh client transfer 0 from simon \
                                    to tezosGoldExchange \
                                    --entrypoint 'tokenToXtz' \
                                    --arg 'Pair (Pair (Pair "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3") (Pair 40 1)) "2020-06-29T18:00:21Z"' \
                                    --burn-cap 1
```

## Get Exchange Total Liquidity

Reference:
```bash
$ ~/babylonnet.sh client get script storage for <exchange-contract-name>
```

Example:
```bash
$ ~/babylonnet.sh client get script storage for tezosGoldExchange
```

## Get Exchange Total XTZ

Reference:
```bash
$ ~/babylonnet.sh client get balance for <exchange-contract-name>
```

Example:
```bash
$ ~/babylonnet.sh client get balance for tezosGoldExchange
```

## Get Exchange Total LQT

Reference:
```bash
$ ~/babylonnet.sh client get big map value for '"<exchange-contract-address>"' \
                         of type 'address' in tezosGold
```

Example:
```bash
$ ~/babylonnet.sh client get big map value \
                         for '"KT1G2D45tpJ9f1iGVwQHqvupv2syhvMqeWPe"' \
                         of type 'address' in tezosGold
```
