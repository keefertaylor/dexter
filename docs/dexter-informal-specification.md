# An Informal Specification of the Tezos Dexter Exchange

This document is an informal specification for a set of contracts that can be
originated on the Tezos blockchain. Together they constitute a decentralized
token exchange for [FA1.2 tokens](https://gitlab.com/tzip/tzip/blob/master/A/FA1.2.md).
Implementation details will mostly be ignored in this document.

## Terminology

For clarity, we will define and highlight certain terms that are used through
out the document.

- `XTZ`: the official token on the Tezos blockchain. Also known as tez or
  colloquially as tezzies.
 
- `FA1.2`: tokens from a [FA1.2 contract](https://gitlab.com/tzip/tzip/blob/master/A/FA1.2.md).

- `DEX`: a token that is internal to the `exchange` contract. It represents
  the amount of liquidity in a contract relative to the amount `XTZ` and `FA1.2`
  held in that contract.

- `exchange`: the main exchange contract which provides a decentralized exchange
  on Tezos for `XTZ` and a particular `FA1.2`. A `factory` may only produce
  or point to one exchange contract per `XTZ`-`FA1.2` pair.
 
- `factory`: a contract that is responsible for originating `exchange`
  contracts and facilitating interaction between two `exchange` contracts.

- `XTZ pool`: amount of `XTZ` held by an `exchange` contract.

- `FA1.2 pool`: amount of `FA1.2` held by an `exchange` contract.

- `liquidity pool` or `DEX pool`: amount of `DEX` held by an `exchange`
  contract.

- `caller`: the contract that provides the `XTZ` and/or `FA1.2` for the
  entrypoint in the `exchange` contract.

## The Exchange Contract

### Public Entry Points

The `exchange` contract has six public entry points. These are callable by any address.

- add liquidity
- remove liquidity
- XTZ to FA1.2
- FA1.2 to XTZ
- FA1.2 to FA1.2 (unimplemented)
- vote on baker (unimplemented)

### Private Entry Points

Private entry points are currently only callable from the broker contract. The broker contract address is stored in the storage and checked when called. Depending on what we do for the `FA1.2` contract and the babylon entry points, it may be only callable by `FA1.2`.

- continuation

### Storage

#### Mutable

- accounts (address to amount of `DEX` it owns)
- totalSupply (total amount of `DEX` in a single exchange contract)
- continuationStorage (address to continuation storage, used by the private entry point Continuation to retrieve data)

#### Immutable

- brokerAddress
- tokenAddress 

### Fees

Fees are collected in the three types of trade at the following rates:

- XTZ to FA1.2: 0.3% fee paid in XTZ
- FA1.2 to XTZ: 0.3% fee paid in FA1.2
- FA1.2 to FA1.2: 0.3% fee paid in FA1.2 tokens for FA1.2 to XTZ swap on input
  exchange 0.3% fee paid in XTZ for XTZ to FA1.2 swap on output.

Fees collected during trades (XTZ to FA1.2, FA1.2 to XTZ and FA1.2 to FA1.2) add
value to `XTZ` and `FA1.2` without increasing the number of `DEX` tokens.

### Liquidity Entry Points

#### Add Liquidity Entry Point

Parameters:
- minLiquidity natural: minimum number of `DEX` the caller will mint if total DEX is greater than 0.
- maxTokens natural: maximum number of tokens deposited
- deadline timestamp: the time after which this transaction can no longer be executed.

Adding liquidity requires the contract caller to provide an equivalent exchange
value of `XTZ` and `FA1.2`, meaning `x` of `XTZ` can be traded for `y` of
`FA1.2`. The first liquidity provider sets the initial exchange rate of `XTZ`
to `FA1.2`. Adding liquidity requires a check on the [FA1.2 contract](https://gitlab.com/tzip/tzip/blob/master/A/FA1.2.md)
that the provider has the amount of `FA1.2` that they want to deposit in the
exchange contract.

##### First Liquidity Provider

This is called when `DEX liquidity pool` is equal to zero.

The initial `DEX liquidity pool` is equivalent to the amount of `XTZ` sent by
the first liquidity provider. If 

##### Non-First Liquidity Provider

This is called when `DEX liquidity pool` is greater than zero.

The caller provides a minimum amount of `DEX` they want to create, the amount of
`XTZ` they want to provide and the maximum number of `FA1.2` they want to
provide.

The amount of `DEX` created is `DEX_pool * XTZ_deposited / XTZ_pool`. If this
amount is lower than the amount of `DEX` they want to create, the transaction
will fail.

The amount of `FA1.2` to be deposited is `FA1.2_pool * XTZ_deposited / XTZ_pool`.
If this amount is greater than the maximum number they want to provide, the
transaction will fail.

#### Remove Liquidity Entry Point

Parameters: 
- burnAmount natural: amount of `DEX` the caller wants to burn
- minMutez mutez: the minimum amount of XTZ (in Mutez) the caller wants to withdraw
- minTokens natural: the minimum amount of Tokens the sender wants to withdraw.
- deadline timestamp: the time after which this transaction can no longer be executed

`DEX` tokens can be burned (or destroyed) in order to withdraw a proportional
amount of `XTZ` and `FA1.2`. They are withdrawn at the current exchange rate.
Fees collected during trades (XTZ to FA1.2, FA1.2 to XTZ and FA1.2 to FA1.2) add
value to `XTZ` and `FA1.2` without increasing the number of `DEX` tokens.

The caller provides the amount of `DEX` they want to burn and the minimum of
amount of `XTZ` and `FA1.2` they want to withdraw. If either limits are not
met than the transaction will fail.

The amount of `XTZ` withdrawn is `XTZ_pool * DEX_burned / DEX_pool`.

The amount of `FA1.2` withdrawn is `FA1.2_pool * DEX_burned / DEX_pool`.

### Exchange Entry Points

The exchange rate of a particular XTZ-FA1.2 exchange contract is relative to the
size of their pools.

#### XTZ to FA1.2 Entry Point

Parameters:
- minTokens natural: minimum amount of tokens the sender wants to purchase.
- deadline timestamp: the time after which this transaction can no longer be executed.

The `fee` is `XTZ_sold * 0.3`.

The `new_XTZ_pool` is `XTZ_pool + XTZ_sold`.

The `new_FA1.2_pool` is `(XTZ_pool * FA1.2_pool) / (new_XTZ_pool - fee)`.

The user receives `FA1.2` equivalent to `FA1.2_pool - new_FA1.2_pool`.

#### FA1.2 to XTZ Entry Point

- tokensSold natural: the number of tokens the sender wants to sell.
- minMutez mutez: minimum amount of XTZ (in Mutez) the sender wants to purchase.
- deadline timestamp: the time after which this transaction can no longer be executed.

The `fee` is `FA1.2_sold * 0.3`.

The `new_FA1.2_pool` is `FA1.2_pool + FA1.2_sold`.

The `new_XTZ_pool` is `(XTZ_pool * FA1.2_pool) / (new_FA1.2_pool - fee)`.

The user receives `XTZ` equivalent to `XTZ_pool - new_XTZ_pool`.

#### FA1.2 to FA1.2 Entry Point (unimplemented)

Parameters:
(unimplemented)

This is currently unimplemented, but the idea is that when a caller wants to trade
`FA1.2 x` for `FA1.2 y`, they perform FA1.2 to XTZ for `x` and call XTZ to FA1.2
for `y` with the amount of `XTZ` gained from the first transaction and the amount
gained from the second transaction, in `FA1.2 y`, is given to the caller.

We are waiting on full support of Babylon entry points in
[morley](https://gitlab.com/morley-framework/morley).

### Vote on Baker Entry Point (unimplemented)

This is currently unimplemented. We are still discussing the best way to do
this. The idea is that the liquidity providers should have a vote of whom to
delegate the `XTZ` of the exchange contract to.

## Continuation Entry Point

Parameters:
- balance natural: the exchange’s balance from the corresponding FA1.2 token.

First the continuation will check if this entry point is called by the broker (or in the future the FA1.2 contract) and then it will retrieve the continuationStorage by the original caller’s address. The continuationStorage is an or type, depending on the value it will be routed to a corresponding continuation function: Add Liquidity Continuation, Remove Liquidity Continuation, XTZ to FA1.2 Continuation, FA1.2 to XTZ Continuation, FA1.2 to FA1.2 Continuation.

## The Factory Contract (unimplemented)

This is also unimplemented because it requires full support of Babylon entry points in
[morley](https://gitlab.com/morley-framework/morley). This should have the following
entry points:

- Provide a `FA1.2` address, create an `exchange` contract for it if it does not exist yet.
- Get `exchange` by `FA1.2` addresses. This is necessary for the FA1.2 to FA1.2 exchange.
- Upgrade `exchange` contract for `FA1.2`. This would require a manager account
  that has special permission. This would not be for the first version of Dexter.

## What to Formally Prove
