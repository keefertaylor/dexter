# dexter-contracts

Until this [issue](https://github.com/commercialhaskell/stack/issues/4984) is 
resolved, this project must be built with stack version 1 because of the way 
Morley uses hpack. Download [stack-1.9.3.1](https://github.com/commercialhaskell/stack/releases/tag/v1.9.3.1),
unzip it, then build it with stack and move the executable to 
`~/.local/bin/stack-1.9.3.1`. Now you can build and run `dexter-contracts`.

```bash
$ stack-1.9.3.1 build
$ stack-1.9.3.1 exec dexter-contracts
```
