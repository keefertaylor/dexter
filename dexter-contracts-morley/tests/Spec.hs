{-|
Module      : Main
Copyright   : (c) camlCase, 2019
Maintainer  : james@camlcase.io

-}

module Main where

-- import Test.Dexter.Contract.Exchange.AddLiquidity (testAddLiquidity)
import Test.Dexter.Contract.Exchange.Continuation.AddLiquidity (testAddLiquidityContinuation)
-- import Test.Dexter.Contract.Exchange.RemoveLiquidity (testRemoveLiquidity)
-- import Test.Dexter.Contract.Exchange.Continuation.RemoveLiquidity (testRemoveLiquidityContinuation)
import Test.Tasty (defaultMainWithIngredients, testGroup)

import Util.Test.Ingredients (ourIngredients)

main :: IO ()
main =
  sequence [testAddLiquidityContinuation] -- [testAddLiquidity, testAddLiquidityContinuation, testRemoveLiquidity, testRemoveLiquidityContinuation]
    >>= defaultMainWithIngredients ourIngredients . testGroup "Dexter contract tests" . concat
