{-|
Module      : Test.Dexter.Contract.Instances
Copyright   : (c) camlCase, 2019
Maintainer  : james@camlcase.io

Missing type class instances. Since these are defined in testing they will not
accidentally be exported.

-}

{-# LANGUAGE StandaloneDeriving     #-}
{-# OPTIONS_GHC -fno-warn-orphans   #-}

module Test.Dexter.Contract.Instances where

import qualified Dexter.Contract.Exchange as Exchange
import           Michelson.Interpret (ContractEnv(..))

deriving instance Show ContractEnv
deriving instance Show Exchange.Parameter
deriving instance Show Exchange.Storage
