{-|
Module      : Test.Dexter.Contract.Exchange.AddLiquidity
Copyright   : (c) camlCase, 2019
Maintainer  : james@camlcase.io

Unit and property tests for the add liquidity entry point in the Dexter exchange contract.

-}

{-# LANGUAGE DerivingStrategies     #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE ViewPatterns           #-}

module Test.Dexter.Contract.Exchange.AddLiquidity
  ( testAddLiquidity
  ) where

import Test.Hspec.Expectations (shouldSatisfy)
import Test.QuickCheck (Gen, Property, choose, counterexample, forAll, vectorOf, withMaxSuccess, (.&&.))
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)
import Test.Tasty.QuickCheck (testProperty)

import qualified Data.Map.Strict as Map

import Named (arg)
import Util.Named ((.!))

import Lorentz (compileLorentz)

import Michelson.Interpret (ContractEnv(..))
import Michelson.Test (ContractPropValidator, contractProp, concatTestTrees, midTimestamp, dummyContractEnv)
import Michelson.Test.Util (failedProp)
import Michelson.Typed (ToT)
import Michelson.Typed.Haskell.Value (BigMap(..))
import qualified Michelson.Typed as T

import Tezos.Address (Address(..))
import Tezos.Core (unsafeMkMutez, timestampPlusSeconds)


import Dexter.Contract.Exchange
  (ContinuationStorage, Parameter(..), MutableStorage,
   Storage(Storage), exchangeContract)

import Dexter.Contract.Types
  (AddLiquidityParams, Balance)

import Prelude
  (($), (>), (<), (<=), (.), (<$>), (<*>), (++), (==), (&&), IO, flip, fst, snd, one, pure, isLeft,
   isRight, Natural, Bool(..), Either(..), show)

import Test.Dexter.Contract.Gen (genAddress, genMidAmount, genSmallAmount, staticImmutableStorage)
import Test.Dexter.Contract.Mock (emptyExchangeStorage)

-- | Unit and property tests for the add liquidity entry point in the dexter contracts.
testAddLiquidity :: IO [TestTree]
testAddLiquidity = concatTestTrees
  [ one . testGroup "Dexter.Contract.Exchange addLiquidity" <$>
    exchangeTest (compileLorentz exchangeContract)
  ]
  where
    exchangeTest contract =
      pure
      [ testCase "Calling addLiquidity entry path completes" $
          contractProp
            contract     -- Contract
            (flip shouldSatisfy (isRight . fst)) -- ContractPropValidator
            contractEnv  -- ContractEnv
            addLiquidity -- Parameter
            emptyExchangeStorage -- Storage

      , testProperty "Check addLiquidity properties" $ withMaxSuccess 1000 $
          qcProp contract
            
      ]

    qcProp contract =
      forAll ((,) <$> genAddLiquidityParams <*> genStorage) $
        \(arbParam, arbStorage) ->
          let validate = validateAddLiquidity contractEnv arbParam arbStorage
           in contractProp contract validate contractEnv arbParam arbStorage

-- =============================================================================
-- Environment
-- =============================================================================

contractEnv :: ContractEnv
contractEnv =
  dummyContractEnv
    { ceNow     = midTimestamp
    , ceAmount  = unsafeMkMutez 1000000
    , ceBalance = unsafeMkMutez 0
    }

-- =============================================================================
-- Parameter
-- =============================================================================

-- | Low values for addLiquidity that should pass
addLiquidity :: Parameter
addLiquidity =
  AddLiquidity
    ( #minLiquidity .! 1
    , #maxTokens    .! 100
    , #deadline     .! (midTimestamp `timestampPlusSeconds` 1)
    )

-- | Generate AddLiquidityParams with a small amount of minLiquidity and a
-- medium amount of maxTokens. The deadline is set to one second later then
-- the time in the environment. It will always pass.
genAddLiquidityParams :: Gen Parameter
genAddLiquidityParams = do
  ml <- genSmallAmount
  mt <- genMidAmount
  e <- choose (-5,5)
  pure . AddLiquidity $
    (#minLiquidity .! ml,
     #maxTokens .! mt,
     #deadline .! (midTimestamp `timestampPlusSeconds` e)
    )

-- =============================================================================
-- Storage
-- =============================================================================

genBigMap :: Gen (BigMap Address Balance)
genBigMap = do
  k <- choose (0,2)
  BigMap . Map.fromList <$> vectorOf k genPair
  where
    genPair = (,) <$> genAddress <*> genMidAmount
  
genMutableStorage :: Gen Natural -> Gen MutableStorage
genMutableStorage genNatural = (,) <$> ((#totalSupply .!) <$> genNatural) <*> (pure $ #continuationStorage .! Map.empty)

-- | There  is a relation between the accounts (the big map), the totalSupply
-- and the tez amount held by the contract. If one is empty or zero, the rest
-- should be empty or zero. Otherwise they should be non-empty and non-zero.
genStorage :: Gen Storage
genStorage = Storage <$> genBigMap <*> pure staticImmutableStorage <*> genMutableStorage genMidAmount

-- =============================================================================
-- Validation test
-- =============================================================================

-- | This validator checks the result of contracts/exchange.tz execution.
--
-- It checks following properties:
--
-- * The parameter deadline is greater than 'NOW'.
-- * The parameter maxTokens is greater than zero.
-- * The included amount is greater than zero.
-- * If the contract has no DEX (totalSuppply is zero), than the minimum amount
--   is 1 XTZ (1000000 mutez).
validateAddLiquidity
  :: ContractEnv
  -> Parameter
  -> Storage
  -> ContractPropValidator (ToT Storage) Property
validateAddLiquidity env' param storage result =
  case param of
    AddLiquidity p -> validateAddLiquidity' env' p storage result
    _ -> failedProp "Expected AddLiquidity"

validateAddLiquidity'
  :: ContractEnv
  -> AddLiquidityParams
  -> Storage
  -> ContractPropValidator (ToT Storage) Property
validateAddLiquidity' env param (Storage oldBigMap immutableStorage mutableStorage) (resE, _interpreterState) =
  -- expected failures
  bool (deadline <= ceNow env) (counterexample "Expected failure: deadline is less than current time" $ isLeft resE) $
  bool (oldTotalSupply == 0 && ceAmount env < unsafeMkMutez 1000000) (counterexample "Expected failure: amount is less than 1 XTZ" $ isLeft resE) $
  bool (maxTokens == 0) (counterexample "Expected failure: maxTokens is zero" $ isLeft resE) $
  bool (ceAmount env == unsafeMkMutez 0) (counterexample "Expected failure: amount is zero" $ isLeft resE) $

  -- expected successes
  (counterexample ("Accounts should not be empty:\n " ++ showState) assertBigMapAccountAdd) .&&.
  (counterexample ("Transfer contract:\n " ++ showState) assertTransferContract) .&&.
  (counterexample ("Continuation map should not be empty:\n " ++ showState) assertContinuationStorage) .&&.
  (counterexample ("The amount and transfer amount should be the same:\n " ++ showState) assertTransferAmount) .&&.
  (counterexample ("When totalLiquidity is 0, the tokens transferred should be less than or equal to the max:\n " ++ showState) assertTokensTransferred)

  where
    showState = "env: " ++ show env ++ "\nparam:" ++ show param ++ "\nresE:" ++ show resE

    bool condition trueFunction falseFunction =
      case condition of
        True -> trueFunction
        False -> falseFunction

    brokerContract = arg #brokerAddress . fst $ immutableStorage
    tokenContract  = arg #tokenAddress . snd $ immutableStorage
    oldTotalSupply = arg #totalSupply . fst $ mutableStorage :: Natural
    oldContinuationStorage = arg #continuationStorage . snd $ mutableStorage :: Map.Map Address ContinuationStorage

    (arg #minLiquidity -> _minLiquidity, arg #maxTokens  -> maxTokens,
     arg #deadline     -> deadline) = param

    -- each action transfers a different amount of mutez
    assertTransferAmount =
      case resE of
        Right ([T.OpTransferTokens (T.TransferTokens _ amount _)], _) ->
          if oldTotalSupply == 0
          then amount == unsafeMkMutez 0
          else (ceAmount env) == amount
        _ -> False

    -- when totalSupply is zero, it should perform a transfer to the token
    -- contract moving ownership to the exchange contract and make sure that
    -- it is less than or equal to maxTokens as set in the params
    assertTokensTransferred =
      if oldTotalSupply == 0
      then
        case resE of
          Right ([T.OpTransferTokens (T.TransferTokens (T.VOr (Left (T.VPair (_, (T.VPair (_, T.VC (T.CvNat tokens))))))) _ _)], _) ->
            tokens <= maxTokens
          _ -> False
      else
        case resE of
          Right ([T.OpTransferTokens _], _) -> True
          _ -> False
      
    -- the two actions perform a transfer to different contracts from the
    -- immutable storage
    assertTransferContract =
      case resE of
        Right ([T.OpTransferTokens (T.TransferTokens _ _ (T.VContract contract))], _) ->
          if oldTotalSupply == 0
          then contract == tokenContract
          else contract == brokerContract
        _ -> False

    -- if there is zero totalSupply, there should be zero amount held by the
    -- contract and an empty big map. In that case the call to addLiquidity will
    -- create a new account.
    assertBigMapAccountAdd =
      case resE of
        Right (_, T.VPair (T.VBigMap bigMap, _)) ->
          if oldTotalSupply == 0
          then (Map.size bigMap) > (Map.size $ unBigMap oldBigMap)
          else True
        _ -> False

    -- when totalSupply is not zero, it will call the broker contract, and store
    -- the information when there is a response.
    assertContinuationStorage =
      case resE of
        Right (_, T.VPair (_, T.VPair (_, T.VPair (_ , T.VMap continuationStorage)))) ->
          if oldTotalSupply == 0
          then (Map.size oldContinuationStorage) == (Map.size continuationStorage)
          else (Map.size continuationStorage) > 0
        _ -> False
