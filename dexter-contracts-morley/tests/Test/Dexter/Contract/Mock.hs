{-|
Module      : Test.Dexter.Contract.Mock
Copyright   : (c) camlCase, 2019
Maintainer  : james@camlcase.io

Mock values that are useful for testing.

-}

{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE OverloadedStrings      #-}

module Test.Dexter.Contract.Mock where

import qualified Data.Map.Strict as Map

import qualified Dexter.Contract.Exchange as Exchange

import           Michelson.Typed.Haskell.Value (BigMap(..))

import           Test.Dexter.Contract.Instances ()

import           Tezos.Address (Address(..), mkKeyAddress, mkContractAddressRaw)
import           Tezos.Crypto (parsePublicKey)

import           Util.Named ((.!))

-- =============================================================================
-- contracts
-- =============================================================================

broker :: Address 
broker = mkContractAddressRaw "broker"

token :: Address
token = mkContractAddressRaw "token"

-- =============================================================================
-- accounts
-- =============================================================================

alice :: Address
alice =
  case parsePublicKey "edpkvQiPK3HQgjMRe3e7tZDoRVCYvpq3VzmheymL2vy6JWcdugvKSv" of
    Right key -> mkKeyAddress key
    _ -> error "alice: unable to parse public key."

bob :: Address
bob =
  case parsePublicKey "edpkucSSDmuEXgmSPXWt1mL7wzFGM2VtyjV1WA8ymi5AE7ZF5efmWr" of
    Right key -> mkKeyAddress key
    _ -> error "bob: unable to parse public key."

simon :: Address
simon =
  case parsePublicKey "edpktyjLG3cpMMeXs8DoR2wh2N22NccSRRsXkJKGWyQjJ3y8qDQE2X" of
    Right key -> mkKeyAddress key
    _ -> error "simon: unable to parse public key."

-- =============================================================================
-- exchange contract storage
-- =============================================================================

emptyExchangeStorage :: Exchange.Storage
emptyExchangeStorage =
  Exchange.Storage
    (BigMap Map.empty)
    (#brokerAddress .! broker, #tokenAddress .! token)
    (#totalSupply .! 0, #continuationStorage .! Map.empty)

setContinuationStorage
  :: Exchange.Storage
  -> Map.Map Address Exchange.ContinuationStorage
  -> Exchange.Storage
setContinuationStorage (Exchange.Storage x y (z, _)) continuationStorage =
  Exchange.Storage x y (z, #continuationStorage .! continuationStorage)

setTotalSupply :: Exchange.Storage -> Natural -> Exchange.Storage
setTotalSupply (Exchange.Storage x y (_, z)) totalSupply =
  Exchange.Storage x y (#totalSupply .! totalSupply, z)

addLiquidityOwner :: Exchange.Storage -> Address -> Natural -> Exchange.Storage
addLiquidityOwner (Exchange.Storage (BigMap bigMap) y z) key value =
  Exchange.Storage (BigMap (Map.insert key value bigMap)) y z
