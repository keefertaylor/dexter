{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DerivingStrategies     #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE NoApplicativeDo        #-}
{-# LANGUAGE OverloadedLabels       #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE QuasiQuotes            #-}
{-# LANGUAGE RebindableSyntax       #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE TemplateHaskell        #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeOperators          #-}
{-# LANGUAGE ViewPatterns           #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Dexter.Contract.Exchange where

import Dexter.Contract.Types
import GHC.Generics    (Generic)
import Lorentz
import Michelson.Typed (BigMap, IsoValue)
import Prelude         (Either, Text, toStrict)
import Tezos.Core      (unsafeMkMutez)

data Parameter
  = AddLiquidity                AddLiquidityParams
  | RemoveLiquidity             RemoveLiquidityParams
  | TezToToken                  TezToTokenParams
  | TokenToTez                  TokenToTezParams  
  | Continuation                Balance
  deriving stock Generic
  deriving anyclass IsoValue

-- | these are set when the contract is originated and cannot be changed
type ImmutableStorage =
  ( "brokerAddress" :! Address -- ContractAddr GetBalanceBrokerContractParameter
  , "tokenAddress"  :! Address -- ContractAddr SimpleTokenContractParameter
  )

-- | data stored when the broker calls the continuation function
type ContinuationStorage =
  Either
    (Either AddLiquidityParams RemoveLiquidityParams)
    (Either TezToTokenParams TokenToTezParams)

-- this data can be updated
type MutableStorage =
  ( "totalSupply"         :! Natural
  , "continuationStorage" :! Map Address ContinuationStorage
  )

data Storage =
  Storage
    { accounts         :: BigMap Address Balance
    , immutableStorage :: ImmutableStorage
    , mutableStorage   :: MutableStorage
    }
    deriving stock Generic
    deriving anyclass IsoValue

exchangeContract :: Contract Parameter Storage
exchangeContract = do
  unpair
  caseT @Parameter
    ( #cAddLiquidity /-> addLiquidity
    , #cRemoveLiquidity /-> removeLiquidity
    , #cTezToToken /-> tezToToken
    , #cTokenToTez /-> tokenToTez
    , #cContinuation /-> do
        swap
        assertSenderIsBrokerContract
        getContinuationStorage
        -- choose function depending on what was store
        ifLeft (ifLeft addLiquidityContinuation removeLiquidityContinuation)
               (ifLeft tezToTokenContinuation tokenToTezContinuation)
    )

-- Main functions

addLiquidity :: '[AddLiquidityParams, Storage] :-> '[([Operation], Storage)]
addLiquidity = do
  -- assert now < deadline
  getField #deadline
  now
  assertLt [mt|addLiquidity: Expected the deadline to be greater than now.|]

  -- assert 0 < maxTokens
  getField #maxTokens
  push @Natural 0
  assertLt [mt|addLiquidity: Expected maxTokens to be greater than 0.|]
    
  -- assert 0 < amount
  amount
  push @Mutez (unsafeMkMutez 0)
  assertLt [mt|addLiquidity: Expected the amount sent to the contract to be greater than 0.|]

  -- stackType @[AddLiquidityParams, Storage]
  -- totalSupply > 0
  swap
  getField #mutableStorage # toField #totalSupply
  dip (push @Natural 0)
  gt
  
  if_ (do -- totalSupply is greater than 0
          swap
          stackType @[AddLiquidityParams, Storage]
          constructT @CallerToGetBalanceParams
            ( fieldCtor $ self >> address # toNamed #tokenAccount
            , fieldCtor $ dip (dup @Storage) # swap # toField #immutableStorage # toField #tokenAddress # toNamed #tokenAddress
            )
          stackType @[CallerToGetBalanceParams, AddLiquidityParams, Storage]

          -- store the AddLiquidityParams
          dip $ left >> left >> updateContinuationStorage
    
          left
          stackType @[GetBalanceBrokerContractParameter, Storage]

          dip $ do
            getField #immutableStorage
            toField #brokerAddress
            contract
            ifNone (push [mt|unable to convert brokerAddress to a contract|] # failWith) nop
            amount

          stackType @[GetBalanceBrokerContractParameter, Mutez, ContractAddr GetBalanceBrokerContractParameter, Storage]

          transferTokens
          nil; swap; cons; pair
      )
      ( do -- totalSupply is 0
          stackType @[Storage, AddLiquidityParams]
          
          -- assert amount > 1000000 Mutez (1 XTZ)
          push @Mutez (unsafeMkMutez 1000000)
          amount
          ge
          if_ nop (push [mt|addLiquidity: amount is less than 1 XTZ.|] # failWith)

          -- set sender's DEX balance to the contract's balance
          getField #accounts
          balance >> mutezToNatural >> some
          sender
          update
          setField #accounts
          stackType @[Storage, AddLiquidityParams]

          -- set the totalSupply of DEX to the contract's balance
          getField #mutableStorage
          balance >> mutezToNatural
          setField #totalSupply
          setField #mutableStorage
          stackType @[Storage, AddLiquidityParams]

          -- transfer tokens from sender to the DEX exchange contract
          constructT @TransferParams
            ( fieldCtor $ sender >> toNamed #from
            , fieldCtor $ self >> address >> toNamed #dest
            , fieldCtor $ dip dup >> swap >> toField #maxTokens >> toNamed #tokens
            )
          stackType @[TransferParams, Storage, AddLiquidityParams]
          dip (dip drop)
          left
          
          dip $ do
            (getField #immutableStorage # toField #tokenAddress # contract # ifNone (push [mt|unable to convert tokenAddress to a contract|] # failWith) nop)
            push @Mutez (unsafeMkMutez 0)            

          stackType @[SimpleTokenContractParameter, Mutez, ContractAddr SimpleTokenContractParameter, Storage]

          transferTokens
          nil; swap; cons; pair
      )

-- | The amount of tokens that the account will give to the exchange contract
getTokenAmount :: forall s. AddLiquidityParams & Storage & Natural & s :-> Natural & AddLiquidityParams & Natural & Storage & s
getTokenAmount = do
  -- tokenAmount = amount * tokenReserve / tezosReserve
  dip swap # swap -- stackType @(Natural : AddLiquidityParams : Storage : s)
  dup             -- stackType @(Natural : Natural : AddLiquidityParams : Storage : s)
  amount
  mutezToNatural
  mul
  dip getTezosReserveAsNatural
  ediv
  ifNone (push [mt|addLiquidityContinuation: divide by zero error, tezosReserve was zero in the formula (amount * tokenReserve / tezosReserve).|] # failWith)
         car
  stackType @(Natural -- tokenAmount
              : Natural -- tokenReserve
              : AddLiquidityParams : Storage : s)
  dip swap
  
  -- assert tokenAmount <= maxTokens
  dip $ getField #maxTokens
  dup
  dip $ assertLe [mt|addLiquidityContinuation: Expected tokenAmount <= maxTokens|]


-- | The amount of liquidity to be added to the exchange
getLiquidityMinted :: forall s. Storage & AddLiquidityParams & s :-> Natural & AddLiquidityParams & Storage & s
getLiquidityMinted = do  
  --                   XTZ_deposited * DEX_pool    / XTZ_pool
  -- liquidityMinted = amount        * totalSupply / tokenReserve
  amount
  mutezToNatural
  dip (getField #mutableStorage # toField #totalSupply)
  mul

  dip $ getTezosReserveAsNatural
  ediv
  ifNone (push [mt|addLiquidityContinuation: divide by zero error, tokenReserve was zero in the formula (amount * totalSupply / tokenReserve).|] # failWith)
         car
  stackType @(Natural & Storage & AddLiquidityParams & s)

  -- assert liquidityMinted >= minLiquidity
  dip $ do
    swap
    getField #minLiquidity
  dup
 
  dip $ do
    assertGe [mt|addLiquidityContinuation: Expected liquidityMinted >= minLiquidity|]
    


-- if this fails, data will be stuck in the map
addLiquidityContinuation :: '[AddLiquidityParams, Storage, Natural] :-> '[([Operation], Storage)]
addLiquidityContinuation = do
  swap >> dip swap
  stackType @[ Storage
             , Natural -- tokenReserve, the amount of tokens that belong to the exchange contract
             , AddLiquidityParams]

  -- amount:  how much was sent in the current transaction
  -- balance: how much the contract holds (this includes the amount)
  -- tokenReserve: amount of tokens held by the exchange contract
  -- tezosReserve: how much the contract held before this transction (balance - amount)

  -- tokenAmount = amount * tokenReserve / tezosReserve
  swap
  stackType @[ Natural -- tokenReserve
             , Storage
             , AddLiquidityParams]
  amount
  mutezToNatural
  stackType @[ Natural -- amount
             , Natural -- tokenReserve
             , Storage
             , AddLiquidityParams]
  mul  
  dip $ getTezosReserveAsNatural
  ediv
  ifNone (push [mt|addLiquidityContinuation: divide by zero error, tezosReserve was zero in the formula (amount * tokenReserve / tezosReserve).|] # failWith)
         car
  stackType @[ Natural -- tokenAmount
             , Storage
             , AddLiquidityParams]

  -- check that liquidityMinted >= minLiquidity
  -- DEX_pool * XTZ_deposited / XTZ_pool
  -- liquidityMinted = amount * totalSupply / (balance - amount)
  swap
  stackType @[ Storage
             , Natural -- tokenAmount
             , AddLiquidityParams]
  getField #mutableStorage # toField #totalSupply
  amount
  mutezToNatural
  mul
  dip getTezosReserveAsNatural
  ediv
  ifNone (push [mt|addLiquidityContinuation: divide by zero error, tokenReserve was zero in the formula (amount * totalSupply / tokenReserve).|] # failWith)
         car
  stackType @[ Natural -- liquidityMinted
             , Storage
             , Natural -- tokenAmount
             , AddLiquidityParams]

  -- assert maxTokens >= tokenAmount
  dip $ dip $ do
    stackType @[ Natural -- tokenAmount
               , AddLiquidityParams]
    dip $ getField #maxTokens
    dup
    stackType @[ Natural -- tokenAmount
               , Natural -- tokenAmount
               , Natural -- maxTokens
               , AddLiquidityParams]    
    dip $ assertLe [mt|addLiquidityContinuation: Expected maxTokens >= tokenAmount|]

  stackType @[ Natural -- liquidityMinted
             , Storage
             , Natural -- tokenAmount
             , AddLiquidityParams]
  
  -- assert liquidityMinted >= minLiquidity
  dip $ do
    dip swap # swap
    toField #minLiquidity  
  dup
  dip $ assertGe [mt|addLiquidityContinuation: Expected liquidityMinted >= minLiquidity|]

  stackType @[ Natural -- liquidityMinted
             , Storage
             , Natural -- tokenAmount
             ]

  -- Storage.Accounts[source].balance += liquidityMinted
  swap
  source
  getAccount
  -- stackType @[Natural, Storage, Natural, AddLiquidityParams, Natural]
  dip (swap # dup)
  add
  some
  dip $ swap # getField #accounts
  source
  update
  setField #accounts
  stackType @[ Storage
             , Natural -- liquidityMinted
             , Natural -- tokenAmount
             ]  

  -- Storage.internal.totalSupply = totalSupply + liquidityMinted
  swap
  dip (getField #mutableStorage # toField #totalSupply)
  add
  dip $ getField #mutableStorage
  setField #totalSupply
  setField #mutableStorage
  stackType @[ Storage
             , Natural -- tokenAmount
             ]  

  constructT @TransferParams
    ( fieldCtor $ source >> toNamed #from
    , fieldCtor $ self >> address >> toNamed #dest
    , fieldCtor $ dip (dup @Natural) >> swap >> toNamed #tokens
    )
  stackType @[ TransferParams, Storage, Natural ]
  dip $ dip drop

  left
  dip $ do
    (getField #immutableStorage # toField #tokenAddress # contract # ifNone (push [mt|unable to convert tokenAddress to a contract|] # failWith) nop)
    push @Mutez (unsafeMkMutez 0)

  stackType @[SimpleTokenContractParameter, Mutez, ContractAddr SimpleTokenContractParameter, Storage]
  transferTokens
  nil; swap; cons; pair

removeLiquidity :: '[RemoveLiquidityParams, Storage] :-> '[([Operation], Storage)]
removeLiquidity = do
  getField #burnAmount
  push @Natural 0
  assertLt [mt|removeLiquidity: expected burnAmount to be greater than zero.|]
    
  getField #deadline
  now
  assertLt [mt|removeLiquidity: expected deadline to be greater than the current time.|]
  
  getField #minMutez
  push @Mutez (unsafeMkMutez 0)
  assertLt [mt|removeLiquidity: expected minMutez to be greater than zero.|]

  getField #minTokens
  push @Natural 0
  assertLt [mt|removeLiquidity: expected minTokens to be greater than zero.|]

  stackType @[RemoveLiquidityParams, Storage]

  right >> left >> updateContinuationStorage

  constructT @CallerToGetBalanceParams
    ( fieldCtor $ self >> address # toNamed #tokenAccount
    , fieldCtor $ dup @Storage # toField #immutableStorage # toField #tokenAddress # toNamed #tokenAddress
    )

  stackType @[CallerToGetBalanceParams, Storage]
  left
  
  dip $ do
    getField #immutableStorage
    toField #brokerAddress
    contract
    ifNone (push [mt|unable to convert brokerAddress to a contract|] # failWith) nop
    amount

  stackType @[GetBalanceBrokerContractParameter, Mutez, ContractAddr GetBalanceBrokerContractParameter, Storage]

  transferTokens
  nil; swap; cons; pair

removeLiquidityContinuation :: '[RemoveLiquidityParams, Storage, Balance] :-> '[([Operation], Storage)]
removeLiquidityContinuation = do
  -- calculate tezos amount
  -- tezAmount = burnAmount * tezosBalance / totalLiquidity
  getField #burnAmount
  balance
  mul
  stackType @[Mutez, RemoveLiquidityParams, Storage, Balance]
  dip $ (swap >> getField #mutableStorage >> toField #totalSupply)
  ediv
  ifNone (push [mt|removeLiquidityContinuation: division by zero|] # failWith) car
  stackType @[ Mutez -- tezAmount
             , Storage, RemoveLiquidityParams, Balance]
  dip $ do
    swap
    getField #minMutez
  dup
  dip $ do
    assertGe [mt|removeLiquidityContinuation: the mutez amount to be withdrawn is lower than the minMutez that you have set.|]
    swap

  -- calculate token ammount
  -- tokenAmount = amount * tokenReserve / totalLiquidity
  dip swap >> swap
  getField #burnAmount
  dip (dip (dip swap # swap) # swap)
  mul
  dip (dip swap # swap # getField #mutableStorage # toField #totalSupply)
  ediv
  ifNone (push [mt|removeLiquidityContinuation: division by zero|] # failWith) car
  stackType @[ Natural -- tokenAmount
             , Storage
             , RemoveLiquidityParams
             , Mutez] -- tezAmount

  -- update the sender's DEX balance
  swap
  source
  getAccount
  dip (dip swap >> swap >> getField #burnAmount) 
  sub
  isNat
  ifNone (push [mt|removeLiquidityContinuation: burnAmount is more than balance.|] # failWith) nop
  stackType @[Natural, RemoveLiquidityParams, Storage, Natural, Mutez]
  some
  dip $ do
    swap
    getField #accounts
  source
  update
  setField #accounts
  stackType @[Storage, RemoveLiquidityParams, Natural, Mutez]
  
  -- update totalSupply
  -- newTotalLiquidity = totalLiquidity - burnAmount
  getField #mutableStorage >> toField #totalSupply
  dip $ do
    swap
    toField #burnAmount
  sub
  isNat
  ifNone (push [mt|removeLiquidityContinuation: burnAmount is more than balance.|] # failWith) nop
  stackType @[Natural, Storage, Natural, Mutez]
  dip (getField #mutableStorage)
  setField #totalSupply
  setField #mutableStorage
  stackType @[Storage, Natural, Mutez]

  dip swap >> swap
  dip (source >> contract @() >> ifNone (push [mt|removeLiquidityContinuation: the source contract does not match the expected type (contract Unit).|] >> failWith) nop)
  unit
  transferTokens
  stackType @[Operation, Storage, Natural]

  nil; swap; cons
  stackType @[[Operation], Storage, Natural]
  
  dip swap >> swap
  stackType @[Natural, [Operation], Storage]
  -- missing transfer from
  -- transfer tokens from DEX exchange contract to the source
  constructT @TransferParams
    ( fieldCtor $ self >> address >> toNamed #from
    , fieldCtor $ source >> toNamed #dest
    , fieldCtor $ dup >> toNamed #tokens
    )
  dip drop
  left
  dip $ do
    swap
    (getField #immutableStorage # toField #tokenAddress # contract # ifNone (push [mt|unable to convert tokenAddress to a contract|] # failWith) nop)
    push @Mutez (unsafeMkMutez 0)            

  stackType @[SimpleTokenContractParameter, Mutez, ContractAddr SimpleTokenContractParameter, Storage, [Operation]]
  transferTokens

  dip swap >> cons >> pair

tezToToken :: '[TezToTokenParams, Storage] :-> '[([Operation], Storage)]
tezToToken = do
  getField #deadline
  dip now
  assertGe [mt|tezToToken: the deadline is less than the current time.|]

  amount
  push (unsafeMkMutez 0)
  assertLt [mt|tezToToken: tezosSold is zero.|]

  getField #minTokens
  push @Natural 0
  assertLt [mt|tezToToken: minTokens is zero.|]

  -- store the parameters for the continuation
  left >> right >> updateContinuationStorage

  constructT @CallerToGetBalanceParams
    ( fieldCtor $ self >> address # toNamed #tokenAccount
    , fieldCtor $ dup @Storage # toField #immutableStorage # toField #tokenAddress # toNamed #tokenAddress
    )

  stackType @[CallerToGetBalanceParams, Storage]
  left
  dip $ do
    getField #immutableStorage
    toField #brokerAddress
    contract
    ifNone (push [mt|unable to convert broker to a contract|] # failWith) nop
    amount

  stackType @[GetBalanceBrokerContractParameter, Mutez, ContractAddr GetBalanceBrokerContractParameter, Storage]

  transferTokens
  nil; swap; cons; pair

tezToTokenContinuation :: '[TezToTokenParams, Storage, Balance] :-> '[([Operation], Storage)]
tezToTokenContinuation = do
  swap >> dip swap >> swap
  -- calculate the amount of tokens bought
  constructT @GetInputPriceParams
    ( fieldCtor $ amount >> mutezToNatural >> toNamed #inputAmount
    , fieldCtor $ amount >> balance >> sub >> mutezToNatural >> toNamed #inputReserve
    , fieldCtor $ dup >> toNamed #outputReserve
    )
  dip drop
  getInputPrice
  dip swap
  stackType @[Natural, TezToTokenParams, Storage]
  dip (toField #minTokens)

  dup
  dip $ assertGe [mt|tezToTokenContinuation: tokens bought is less than the minTokens amount you set.|]
  stackType @[Natural, Storage]

  -- transfer tokens from DEX exchange contract to the source
  constructT @TransferParams
    ( fieldCtor $ self >> address >> toNamed #from
    , fieldCtor $ source >> toNamed #dest
    , fieldCtor $ dup >> toNamed #tokens
    )

  dip drop
  left
  dip $ do
    (getField #immutableStorage # toField #tokenAddress # contract # ifNone (push [mt|unable to convert tokenAddress to a contract|] # failWith) nop)
    push @Mutez (unsafeMkMutez 0)            

  stackType @[SimpleTokenContractParameter, Mutez, ContractAddr SimpleTokenContractParameter, Storage]
  transferTokens
  nil; swap; cons; pair

type GetInputPriceParams =
  ( "inputAmount"   :! Natural
  , "inputReserve"  :! Natural
  , "outputReserve" :! Natural
  )

getInputPrice :: GetInputPriceParams & s :-> Natural & s
getInputPrice = do
  getField #inputReserve
  push @Natural 0
  assertLt [mt|inputReserve must be greater than zero|]

  getField #outputReserve
  push @Natural 0
  assertLt [mt|outputReserve must be greater than zero|]

  -- inputAmountWithFee = inputAmount * 997
  getField #inputAmount
  push @Natural 997
  mul
  dup

  -- numerator = inputAmountWithFee * outputReserve
  dip $ do
    dip (getField #outputReserve)
    mul

  -- denominator = (inputReserve * 1000) + inputAmountWithFee
  -- inputAmountWithFee & numerator & a & s
  swap
  dip $ do
    dip (toField #inputReserve # push @Natural 1000 # mul)
    add

  -- numerator & denominator & a & s
  ediv
  ifNone
    (push [mt|getInputPrice division by zero|] # failWith)
    car

tokenToTez :: '[TokenToTezParams, Storage] :-> '[([Operation], Storage)]
tokenToTez = do
  getField #deadline
  dip now
  assertGe [mt|tokenToTez: the deadline is less than the current time.|]

  getField #tokensSold
  push @Natural 0
  assertLt [mt|tokenToTez: tokensSold is zero.|]

  getField #minMutez
  push (unsafeMkMutez 0)
  assertLt [mt|tokenToTez: minMutez is zero.|]

  -- store the parameters for the continuation
  right >> right >> updateContinuationStorage

  constructT @CallerToGetBalanceParams
    ( fieldCtor $ self >> address # toNamed #tokenAccount
    , fieldCtor $ dup @Storage # toField #immutableStorage # toField #tokenAddress # toNamed #tokenAddress
    )

  stackType @[CallerToGetBalanceParams, Storage]
  left
  dip $ do
    getField #immutableStorage
    toField #brokerAddress
    contract
    ifNone (push [mt|unable to convert brokerAddress to a contract|] # failWith) nop
    amount

  stackType @[GetBalanceBrokerContractParameter, Mutez, ContractAddr GetBalanceBrokerContractParameter, Storage]

  transferTokens
  nil; swap; cons; pair

tokenToTezContinuation :: '[TokenToTezParams, Storage, Balance] :-> '[([Operation], Storage)]
tokenToTezContinuation = do
  dip swap >> swap
  stackType @[Balance, TokenToTezParams, Storage]
  swap
  -- calculate the amount of tokens bought
  constructT @GetInputPriceParams
    ( fieldCtor $ dup @TokenToTezParams >> toField #tokensSold >> toNamed #inputAmount
    , fieldCtor $ dip (dup @Balance) >> swap >> toNamed #inputReserve
    , fieldCtor $ balance >> mutezToNatural >> toNamed #outputReserve
    )

  stackType @[GetInputPriceParams, TokenToTezParams, Balance, Storage]
  dip (dip drop)
  stackType @[GetInputPriceParams, TokenToTezParams, Storage]
  
  getInputPrice
  naturalToMutez
  stackType @[Mutez, TokenToTezParams, Storage]
  
  dup
  dip $ drop

  stackType @[Mutez, TokenToTezParams, Storage]

  -- send tez to source
  source
  contract @()
  ifNone (push [mt|tokenToTezContinuation: the source does not match the expected contract type (contract unit).|] >> failWith) nop
  swap
  unit
  transferTokens

  nil; swap; cons

  stackType @[[Operation], TokenToTezParams, Storage]  

  -- send token to the exchange contract
  swap >> dip swap
  constructT @TransferParams
    ( fieldCtor $ source >> toNamed #from
    , fieldCtor $ self >> address >> toNamed #dest
    , fieldCtor $ dup >> swap >> toField #tokensSold >> toNamed #tokens
    )
  dip drop
  stackType @[TransferParams, Storage, [Operation]]
  left
  dip $ do
    (getField #immutableStorage # toField #tokenAddress # contract # ifNone (push [mt|unable to convert tokenAddress to a contract|] # failWith) nop)
    push @Mutez (unsafeMkMutez 0)            

  stackType @[SimpleTokenContractParameter, Mutez, ContractAddr SimpleTokenContractParameter, Storage, [Operation]]
  transferTokens
  dip swap; cons; pair


-- Utility functions

getContinuationStorage :: Storage & s :-> ContinuationStorage & Storage & s
getContinuationStorage = do
  getField #mutableStorage
  toField #continuationStorage
  source
  get
  ifNone (push [mt|continuation: the continuation storage could not be found for the transaction source.|] # failWith) nop
  -- remove data from storage
  dip $ do
    getField #mutableStorage
    toField #continuationStorage
    none
    source
    update
    dip $ getField #mutableStorage
    setField #continuationStorage
    setField #mutableStorage

updateContinuationStorage :: ContinuationStorage & Storage & s :-> Storage & s
updateContinuationStorage = do
  some
  dip $ do
    getField #mutableStorage
    toField  #continuationStorage
  sender
  update
  dip $ getField #mutableStorage
  setField #continuationStorage
  setField #mutableStorage

assertSenderIsBrokerContract :: Storage & s :-> Storage & s
assertSenderIsBrokerContract = do
  getField #immutableStorage
  toField #brokerAddress
  sender
  assertEq [mt|continuation: this method only allows calls from the broker contract.|]

-- | getXTZPool    
getTezosReserveAsNatural :: s :-> Natural & s
getTezosReserveAsNatural = do
  amount
  mutezToNatural
  balance
  mutezToNatural
  sub
  isNat
  ifNone (push [mt|getTezosReserveAsNatural failed, the natural value was unexpectedly less than zero.|] # failWith) nop

getAccount :: Address & Storage & s :-> Balance & Storage & s
getAccount = do
  dip (getField #accounts); get; ifNone ( push 0 ) nop

printExchangeContract :: Text
printExchangeContract = toStrict $ printLorentzContract False lcwDumb exchangeContract
