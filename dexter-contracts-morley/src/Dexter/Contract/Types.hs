{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DerivingStrategies     #-}
{-# LANGUAGE NoApplicativeDo        #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE RebindableSyntax       #-}
{-# LANGUAGE TypeOperators          #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}

module Dexter.Contract.Types where

import Lorentz         (Address, Map, Mutez, Timestamp, (#),
                        (>>), (:->), (:!), type (&), car, dip, ediv, mul,
                        ifNone, push)
import Michelson.Typed (BigMap)
import Prelude         (Either, Natural, fromInteger)
import Tezos.Core      (unsafeMkMutez)

-- =============================================================================
-- Type synonyms
-- =============================================================================

type Balance    = Natural
type Allowances = Map Address Natural
type Account    = (Balance, Allowances)
type Accounts   = BigMap Address Account

-- =============================================================================
-- Exchange contract types
-- =============================================================================

type AddLiquidityParams =
  ( "minLiquidity" :! Natural   -- minimum number of DEX sender will mint if total DEX is greater than 0
  , "maxTokens"    :! Natural   -- maximum number of tokens deposited. Deposits max amount if DEX supply is 0.
  , "deadline"     :! Timestamp -- the time after which this transaction can no longer be executed.
  )

type RemoveLiquidityParams =
  ( "burnAmount" :! Natural   -- amount of DEX burned
  , "minMutez"   :! Mutez     -- the minimum amount of XTZ (in Mutez) the sender wants to withdraw, if not met the transaction will fail.
  , "minTokens"  :! Natural   -- the minimum amount of Tokens the sender wants to withdraw, if not met the transaction will fail.
  , "deadline"   :! Timestamp -- the time after which this transaction can no longer be executed.
  )

type TezToTokenParams =
  ( "minTokens" :! Natural   -- minimum amount of tokens the sender wants to purchase, if not met the transaction will fail.
  , "deadline"  :! Timestamp -- the time after which this transaction can no longer be executed.
  )

type TokenToTezParams =
  ( "tokensSold" :! Natural   -- the number of tokens the sender wants to sell.
  , "minMutez"   :! Mutez     -- minimum amount of XTZ (in Mutez) the sender wants to purchase, if not met the transaction will fail.
  , "deadline"   :! Timestamp -- the time after which this transaction can no longer be executed.
  )

type ExchangeContractParameter =
  Either (Either AddLiquidityParams RemoveLiquidityParams)
         (Either TezToTokenParams (Either TokenToTezParams Balance))

-- =============================================================================
-- Broker contract types
-- =============================================================================

type CallerToGetBalanceParams =
  ( "tokenAccount" :! Address
  , "tokenAddress" :! Address -- ContractAddr SimpleTokenContractParameter
  )

type GetBalanceToCallerParams = Balance

type GetBalanceBrokerContractParameter = Either CallerToGetBalanceParams GetBalanceToCallerParams

-- =============================================================================
-- ERC20 token contract types
-- =============================================================================

type TransferParams =
  ( "from"   :! Address
  , "dest"   :! Address
  , "tokens" :! Natural
  )

type GetBalanceParams = Address

type SimpleTokenContractParameter = Either TransferParams GetBalanceParams

-- =============================================================================
-- Util functions
-- =============================================================================

mutezToNatural :: Mutez & s :-> Natural & s
mutezToNatural = dip (push (unsafeMkMutez 1)) # ediv # ifNone (push (0 :: Natural)) (car)

naturalToMutez :: Natural & s :-> Mutez & s
naturalToMutez = push (unsafeMkMutez 1) >> mul
