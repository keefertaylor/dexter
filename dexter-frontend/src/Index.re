let bakers: list(DexterTypes.Baker.t) = [
  {
    name: "Tezos Capital Legacy",
    address: "tz1TDSmoZXwVevLTEvKCTHWpomG76oC9S2fJ",
    iconPath: Some("tezos-capital-legacy-logo.png"),
  },
  {
    name: "TezosBC",
    address: "tz1c3Wh8gNMMsYwZd67JndQpYxdaaPUV27E7",
    iconPath: Some("tezos-bc-logo.png"),
  },
];

let exchanges: list(DexterTypes.Exchange.t) = [
  {
    name: "Tezos Gold",
    symbol: "TZG",
    tokenContractId:
      Tezos.ContractId.ofStringUnsafe("KT1U21mGrKarsajJq8Zqkyp1wxiVAmGfrdTn"),
    exchangeContractId:
      Tezos.ContractId.ofStringUnsafe("KT1Uyd4VbTiNX2qdto9SbaFAiF38Q4kcNP48"),
    bigMapId: Tezos.BigMapId.ofInt(765),
  },
  {
    name: "Tezos Tacos",
    symbol: "TZT",
    tokenContractId:
      Tezos.ContractId.ofStringUnsafe("KT1LNCXbwgtVc3z8ugEXKecPBNaDFe6Wewxs"),
    exchangeContractId:
      Tezos.ContractId.ofStringUnsafe("KT1RBBiJKpdgbbMiXMBSkDuJ4V99caRRpq4r"),
    bigMapId: Tezos.BigMapId.ofInt(777),
  },
  {
    name: "ABC",
    symbol: "ABC",
    tokenContractId:
      Tezos.ContractId.ofStringUnsafe("KT1R9N3LhTRAkkjuHMhwyzNAdzrXE7PjHiSs"),
    exchangeContractId:
      Tezos.ContractId.ofStringUnsafe("KT1GdgBRxuAX3Xe8Q763aZoFhbQ5iU2TrfmL"),
    bigMapId: Tezos.BigMapId.ofInt(778),
  },
];

ReactDOMRe.renderToElementWithId(<DexterUi bakers exchanges />, "root");
