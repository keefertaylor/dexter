type t = {
  name: string,
  symbol: string,
  tokenContractId: Tezos.ContractId.t,
  exchangeContractId: Tezos.ContractId.t,  
  bigMapId: Tezos.BigMapId.t,
};
