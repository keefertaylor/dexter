type t = {
  total: int64,
  name: string,
  symbol: string,
};

let ofExpression = (expression: Tezos.Expression.t): Belt.Result.t(t, string) => {
  switch (expression) {
  | ContractExpression(
      Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
      Some([
        _,
        ContractExpression(
          Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
          Some([
            Tezos_Expression.IntExpression(total),
            ContractExpression(
              Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
              Some([
                Tezos_Expression.StringExpression(name),
                Tezos_Expression.StringExpression(symbol),
              ]),
              _,
            ),
          ]),
          _,
        ),
      ]),
      _,
    ) =>
    Belt.Result.Ok({total, name, symbol})
  | _ =>
    Belt.Result.Error(
      Tezos.Expression.encode(expression) |> Js.Json.stringify,
    )
  };
};