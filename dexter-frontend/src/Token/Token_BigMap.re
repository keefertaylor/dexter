type t = {tokens: int64};

let ofExpression = (expression: Tezos.Expression.t): Belt.Result.t(t, string) => {
  switch (expression) {
  | ContractExpression(
      Tezos_Primitives.PrimitiveData(Tezos_PrimitiveData.Pair),
      Some([Tezos_Expression.IntExpression(tokens), _]),
      _,
    ) =>
    Belt.Result.Ok({tokens: tokens}: t)
  | _ =>
    Belt.Result.Error(
      Tezos_Expression.encode(expression) |> Js.Json.stringify,
    )
  };
};