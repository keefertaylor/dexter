let url = "http://127.0.0.1:9323";

let fetch =
    (tokenContractId: string, account: string)
    : Js.Promise.t(Belt.Result.t((Token_Store.t, Token_BigMap.t), string)) => {
  let storePromise =
    Tezos.RPC.getStorage(url, "main", "head", tokenContractId)
    |> Js.Promise.then_(result =>
         switch (result) {
         | Belt.Result.Ok(expression) =>
           Token_Store.ofExpression(expression) |> Js.Promise.resolve
         | Belt.Result.Error(_) =>
           Belt.Result.Error("storePromise failed") |> Js.Promise.resolve
         }
       );

  let query: Tezos_BigMapQuery.t = {
    key: Tezos.Expression.StringExpression(account),
    type_:
      Tezos.Expression.ContractExpression(
        Tezos.Primitives.PrimitiveType(Tezos.PrimitiveType.Address),
        None,
        None,
      ),
  };

  let bigMapPromise =
    Tezos.RPC.getBigMapValue(url, "main", "head", tokenContractId, query)
    |> Js.Promise.then_(result =>
         switch (result) {
         | Belt.Result.Ok(expression) =>
           Token_BigMap.ofExpression(expression) |> Js.Promise.resolve
         | Belt.Result.Error(_) =>
           Belt.Result.Error("bigMapPromise failed") |> Js.Promise.resolve
         }
       );

  Js.Promise.all2((storePromise, bigMapPromise))
  |> Js.Promise.then_(results =>
       switch (results) {
       | (Belt.Result.Ok(store), Belt.Result.Ok(bigMap)) =>
         Belt.Result.Ok((store, bigMap)) |> Js.Promise.resolve
       | _ => Belt.Result.Error("something went wrong") |> Js.Promise.resolve
       }
     );
};