/**
 * get address from Tezbridge
 */
let getSource: unit => Js.Promise.t(string) = [%bs.raw
  {|
    function () {
        return tezbridge.request({method: 'get_source'});
    }
    |}
];

/**
 * {operation_id: "oo42d1LB76JFUFMDMznhmuSQnneNLJkMqtSG8GRD2mwdMLvHumU", originated_contracts: Array(0)}
 */

[@bs.deriving {abstract: light}]
type transactionResponse = {
  [@bs.as "operation_id"]
  operationId: string,
  [@bs.as "originated_contracts"]
  age: array(string),
};

/**
 * only used here, unsafe types
 */
let postTransactionRaw:
  (Js.Json.t, Js.Json.t, Js.Json.t) => Js.Promise.t(transactionResponse) = [%bs.raw
  {|
   function (contract, tez, parameters) {
       return tezbridge.request({
         method: 'inject_operations',
         operations: [
           {
             kind: 'transaction',
             amount: tez,
             destination: contract,
             parameters: parameters
           }
         ]
       });
   }
   |}
];

/**
 * exposed here, safe
 */
let postTransaction =
    (
      contract: Tezos.ContractId.t,
      mutez: Tezos.Mutez.t,
      expression: Tezos.Expression.t,
    )
    : Js.Promise.t(transactionResponse) =>
  postTransactionRaw(
    Tezos.ContractId.encode(contract),
    Tezos.Mutez.encode(mutez),
    Tezos.Expression.encode(expression),
  );