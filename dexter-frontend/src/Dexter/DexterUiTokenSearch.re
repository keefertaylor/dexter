module Style = {
  open Css;

  let container = style([]);

  let searchRow =
    style([display(`flex), flexDirection(`row), paddingTop(`px(16))]);

  let input =
    style([
      selector("input:focus", [unsafe("border", "none")]),
      unsafe("border", "none"),
      paddingLeft(`px(16)),
      fontSize(`px(18)),
      color(DexterUiColor.grey),
      DexterUiFont.defaultFont,
    ]);

  let magnifyingGlassIcon =
    style([paddingLeft(`px(24)), width(`px(24)), height(`px(24))]);

  let tokenIcon =
    style([
      width(`px(32)),
      height(`px(32)),
      paddingRight(`px(16)),
      margin(`auto),
    ]);

  let tokenRow =
    style([
      display(`flex),
      flexDirection(`row),
      borderStyle(solid),
      borderColor(hex("C4D0DC")),
      borderTopWidth(`px(1)),
      borderRightWidth(`px(0)),
      borderBottomWidth(`px(0)),
      borderLeftWidth(`px(0)),
      cursor(`pointer),
    ]);

  let tokenDetails =
    style([
      display(`flex),
      flexDirection(`column),
      width(`percent(100.0)),
    ]);

  let tokenRowTop = style([]);

  let tokenRowBottom = style([]);

  let tokenSymbol =
    style([
      Css.float(`left),
      color(DexterUiColor.black),
      DexterUiFont.boldFont,
      fontSize(`px(25)),
    ]);

  let tokenName =
    style([
      Css.float(`left),
      color(DexterUiColor.grey),
      DexterUiFont.defaultFont,
      fontSize(`px(16)),
    ]);

  let tokenAmount =
    style([
      Css.float(`right),
      color(DexterUiColor.grey),
      DexterUiFont.defaultFont,
      fontSize(`px(25)),
    ]);

  let tokenAvailability =
    style([
      Css.float(`right),
      color(DexterUiColor.grey),
      DexterUiFont.defaultFont,
      fontSize(`px(16)),
    ]);
};

[@react.component]
let make = (~tokenAmounts: list(DexterUiTokenAmount.t), ~onTokenChange) =>
  <div className=Style.container>
    <div className=Style.searchRow>
      <div>
        <img className=Style.magnifyingGlassIcon src="magnifying-glass.svg" />
      </div>
      <input className=Style.input placeholder="Search token" />
    </div>
    <ul>
      (
        List.mapi(
          (i, tokenAmount: DexterUiTokenAmount.t) =>
            <li className=Style.tokenRow key=(string_of_int(i)) onClick=(_ => onTokenChange(tokenAmount))>
              <img className=Style.tokenIcon src="abc.png" />
              <div className=Style.tokenDetails>
                <div className=Style.tokenRowTop>
                  <span className=Style.tokenSymbol>
                    (ReasonReact.string(tokenAmount.symbol))
                  </span>
                  <span className=Style.tokenAmount>
                    (ReasonReact.string(string_of_int(tokenAmount.amount)))
                  </span>
                </div>
                <div className=Style.tokenRowBottom>
                  <span className=Style.tokenName>
                    (ReasonReact.string(tokenAmount.name))
                  </span>
                  <span className=Style.tokenAvailability>
                    (ReasonReact.string("Available"))
                  </span>
                </div>
              </div>
            </li>,
          tokenAmounts,
        )
        |> Array.of_list
        |> ReasonReact.array
      )
    </ul>
  </div>;
