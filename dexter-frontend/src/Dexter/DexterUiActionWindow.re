module Style = {
  open Css;

  let mainContainer =
    style([paddingLeft(`px(80)), paddingRight(`px(80))]);

  let actionContainer =
    style([
      backgroundColor(white),
      paddingTop(`px(80)),
      paddingBottom(`px(50)),
      borderRadius(`px(5)),
    ]);
};

[@react.component]
let make =
    (
      ~address: option(string),
      ~action: DexterUiAction.t,
      ~activeBaker: option(DexterTypes.Baker.t),
      ~onChange,
      ~onBakerChange,
      ~bakers: list(DexterTypes.Baker.t),
      ~exchanges: list(DexterTypes.Exchange.t),
      ~tokenAmounts: list(DexterUiTokenAmount.t),
      ~leftToken: DexterUiTokenAmount.t,
      ~rightToken:  DexterUiTokenAmount.t,
      ~onLeftTokenChange,
      ~onRightTokenChange,
      ~onSwapLeftAndRight,
    ) => {
  let body =
    switch (action) {
    | Swap => <DexterUiSwap address exchanges tokenAmounts leftToken rightToken onLeftTokenChange onRightTokenChange onSwapLeftAndRight />
    | AddLiquidity => <DexterUiAddLiquidity activeBaker address bakers tokenAmounts exchanges leftToken rightToken onBakerChange onLeftTokenChange onRightTokenChange onSwapLeftAndRight />
    | RemoveLiquidity => <DexterUiRemoveLiquidity address tokenAmounts exchanges leftToken rightToken onLeftTokenChange onRightTokenChange onSwapLeftAndRight />
    | Receive => <DexterUiReceive address />
    };
  <div className=Style.mainContainer>
    <DexterUiMainNavBar action onChange />
    <div className=Style.actionContainer> body </div>
  </div>;
};
