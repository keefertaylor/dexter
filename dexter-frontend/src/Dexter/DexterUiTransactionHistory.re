[@react.component]
let make =
    (
      ~account: option(string),
      ~recentTransactions: list(int),
      ~poolTokens: list(int),
    ) => {
  Js.Console.log(recentTransactions);
  Js.Console.log(poolTokens);
  switch (account) {
  | Some(account) =>
    <div> (ReasonReact.string("Show the account: " ++ account)) </div>
  | None =>
    <div>
      <div> (ReasonReact.string("There are no transactions yet!")) </div>
    </div>
  };
};
