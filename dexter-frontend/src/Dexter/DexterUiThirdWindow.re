module Style = {
  open Css;

  let navbar =
    style([
      display(`flex),
      justifyContent(spaceBetween),
      paddingTop(px(10)),
    ]);

  let tab =
    style([
      display(`flex),
      flex(`num(0.5)),
      cursor(`pointer),
      justifyContent(`center),
      fontSize(px(18)),
      fontFamily("Source Sans Pro, Semibold"),
      paddingTop(px(10)),
      paddingBottom(px(15)),
    ]);

  let active =
    style([
      unsafe("backgroundColor", "white"),
      color(hex("0258FF")),
      fontSize(`px(18)),
      borderTopLeftRadius(px(5)),
      borderTopRightRadius(px(5)),
    ]);

  let inactive = style([unsafe("backgroundColor", "inherit")]);

  let tabInternal = style([display(`table)]);

  let addIcon = style([width(`px(24)), height(`px(24))]);

  let tabTitle =
    style([
      display(`tableCell),
      verticalAlign(`middle),
      paddingLeft(`px(8)),
    ]);
};

let isActive = (x: DexterUiAccountData.t, y: DexterUiAccountData.t) =>
  if (x == y) {
    Style.active;
  } else {
    Style.inactive;
  };

[@react.component]
let make =
    (
      ~account: option(string),
      ~accountData: DexterUiAccountData.t,
      ~recentTransactions: list(int),
      ~poolTokens: list(int),
    ) => {
  Js.Console.log(recentTransactions);
  Js.Console.log(poolTokens);
  <div>
    <div className=Style.navbar>
      <div
        className=(
          Css.merge([
            Style.tab,
            isActive(DexterUiAccountData.RecentTransactions, accountData),
          ])
        )>
        <div className=Style.tabInternal>
          <img className=Style.addIcon />
          <span className=Style.tabTitle>
            (ReasonReact.string("Recent transactions"))
          </span>
        </div>
      </div>
      <div
        className=(
          Css.merge([
            Style.tab,
            isActive(DexterUiAccountData.PoolTokens, accountData),
          ])
        )>
        <div className=Style.tabInternal>
          <img className=Style.addIcon />
          <span className=Style.tabTitle>
            (ReasonReact.string("Your pool tokens"))
          </span>
        </div>
      </div>
    </div>
    (
      switch (accountData) {
      | RecentTransactions =>
        switch (account) {
        | Some(account) =>
          <div> (ReasonReact.string("Show the account: " ++ account)) </div>
        | None =>
          <div>
            <div>
              (ReasonReact.string("There are no transactions yet!"))
            </div>
          </div>
        }
      | PoolTokens => <div />
      }
    )
  </div>;
};
