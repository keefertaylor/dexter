type t =
  | Swap
  | AddLiquidity
  | RemoveLiquidity
  | Receive;