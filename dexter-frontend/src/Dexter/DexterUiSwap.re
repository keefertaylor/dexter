module Style = {
  open Css;

  let row = style([display(`flex)]);

  let column = style([flex(`num(50.0))]);

  let columnSide = style([flex(`num(40.0))]);
  let columnCenter = style([flex(`num(20.0))]);

  let hcenter =
    style([textAlign(`center), marginTop(px(10)), fontSize(`px(14))]);

  let black = color(hex("082344"));
  let grey = color(hex("758DA6"));
  let blue = color(hex("0258FF"));

  let action =
    style([
      grey,
      fontSize(px(30)),
      fontFamily("Source Sans Pro, Semibold"),
      letterSpacing(`px(1)),
    ]);

  let space = style([marginTop(`px(10))]);

  let balanceGroup = style([marginTop(`px(24))]);
  let balanceToken = style([display(`table)]);
  let token = style([width(`px(48)), height(`px(48))]);
  let tokenText =
    style([
      black,
      fontSize(`px(42)),
      fontFamily("Source Sans Pro, Semibold"),
      display(`tableCell),
      paddingLeft(`px(16)),
      verticalAlign(`middle),
    ]);
  let balance =
    style([
      grey,
      fontSize(px(16)),
      fontFamily("Source Sans Pro, Regular"),
    ]);

  let amount = style([fontSize(px(18)), grey]);
  let amountBox =
    style([
      unsafe("borderRadius", "10px"),
      borderColor(hex("C4D0DC")),
      borderStyle(solid),
      borderWidth(px(1)),
      padding2(~v=px(10), ~h=px(20)),
      marginTop(px(5)),
      marginBottom(px(50)),
      marginRight(px(100)),
    ]);

  let verticalBar =
    style([
      width(`px(0)),
      height(`px(96)),
      borderWidth(`px(1)),
      borderStyle(`solid),
      borderColor(DexterUiColor.lightGrey),
      marginTop(`px(38)),
      marginBottom(`px(38)),
    ]);
  let cc = style([width(`percent(100.0))]);
  let dd = style([display(`table), margin(`auto)]);
  let circle =
    style([
      backgroundColor(DexterUiColor.blue),
      backgroundRepeat(`noRepeat),
      backgroundOrigin(`paddingBox),
      borderRadius(`percent(50.0)),
      width(`px(48)),
      height(`px(48)),
      boxShadow(
        Shadow.box(~x=px(0), ~y=px(0), ~blur=px(12), hex("0A28642E")),
      ),
      position(`relative),
      marginLeft(`percent(-50.0)),
    ]);
  let circleImg =
    style([
      width(`px(21)),
      height(`px(25)),
      position(`absolute),
      margin(`auto),
      top(`px(0)),
      left(`px(0)),
      right(`px(0)),
      bottom(`px(0)),
    ]);

  let profits = style([width(`px(16)), height(`px(10))]);
  let profitsText = style([marginLeft(`px(8))]);

  let tokenAmount =
    style([
      fontSize(px(42)),
      grey,
      fontFamily("Source Sans Pro, Regular"),
    ]);

  let usdAmount =
    style([
      fontSize(px(18)),
      grey,
      fontFamily("Source Sans Pro, Semibold"),
    ]);

  let exchangeRateBox =
    style([
      borderWidth(`px(1)),
      borderStyle(`solid),
      borderColor(hex("C4D0DC")),
      borderRadius(`px(17)),
    ]);

  let exchangeRate = style([textAlign(`center), grey, fontSize(`px(16))]);

  let destinationContainer = style([paddingLeft(`px(47))]);
  let destination = style([fontSize(`px(18)), marginTop(`px(10)), grey]);
  let destinationInternal = style([display(`table)]);

  let informationBox =
    style([
      backgroundColor(hex("0258FF0D")),
      backgroundRepeat(`noRepeat),
      backgroundOrigin(`paddingBox),
      borderColor(hex("0258FF")),
      borderRadius(`px(5)),
      borderStyle(`solid),
      padding2(~v=px(20), ~h=px(20)),
    ]);

  let informationText =
    style([
      color(hex("0258FF")),
      fontFamily("Source Sans Pro, Regular"),
      fontSize(`px(16)),
    ]);

  let center = style([display(table), margin2(~v=px(75), ~h=auto)]);

  let container = style([textAlign(`center), display(`inlineBlock)]);

  let slippageMessage = style([grey, fontSize(`px(16))]);
  let slippageChange = style([blue, fontSize(`px(16))]);

  let blue = hex("0258FF");
  let lightBlue = hex("A3BDFB");

  let swapButton = (address: option(string)) =>
    style([
      switch (address) {
      | Some(_address) => backgroundColor(blue)
      | None => backgroundColor(lightBlue)
      },
      switch (address) {
      | Some(_address) => cursor(`pointer)
      | None => cursor(`default)
      },
      backgroundRepeat(`noRepeat),
      backgroundOrigin(`paddingBox),
      boxShadow(
        Shadow.box(~x=px(0), ~y=px(12), ~blur=px(26), hex("0A28642E")),
      ),
      borderRadius(`px(10)),
      borderStyle(`none),
      padding2(~v=px(20), ~h=px(20)),
    ]);

  let swapButtonText =
    style([
      fontSize(`px(18)),
      color(hex("FFFFFF")),
      fontFamily("Source Sans Pro, Semibold"),
      textAlign(`center),
    ]);

  let acceptButton = style([width(`px(24)), height(`px(24))]);
  let destinationText =
    style([
      paddingLeft(`px(8)),
      display(`tableCell),
      verticalAlign(`middle),
    ]);
};

let iInCircle = [%raw {|'\u24D8'|}];

[@react.component]
let make =
    (
      ~address: option(string),
      ~tokenAmounts: list(DexterUiTokenAmount.t),
      ~exchanges: list(DexterTypes.Exchange.t),
      ~leftToken: DexterUiTokenAmount.t,
      ~rightToken: DexterUiTokenAmount.t,
      ~onLeftTokenChange,
      ~onRightTokenChange,
      ~onSwapLeftAndRight,
    ) =>
  <div>
    <DexterUiExchange
      action=DexterUiAction.Swap
      tokenAmounts
      exchanges
      leftToken
      rightToken
      onLeftTokenChange
      onRightTokenChange
      onSwapLeftAndRight
    />
    <div className=Style.destinationContainer>
      <div className=Style.destination>
        <div className=Style.destinationInternal>
          <img
            className=Style.acceptButton
            src="accept-circular-button-grey.svg"
          />
          <span className=Style.destinationText>
            (
              ReasonReact.string(
                "Send " ++ rightToken.symbol ++ " to another address",
              )
            )
          </span>
        </div>
      </div>
    </div>
    <div className=Style.center>
      <div className=Style.container>
        <div className=Style.informationBox>
          <span className=Style.informationText>
            iInCircle
            (
              ReasonReact.string(
                " Swapping allows you to exchange XTZ from your wallet to another token.",
              )
            )
          </span>
        </div>
      </div>
    </div>
    <div className=Style.center>
      <div className=Style.container>
        <span className=Style.slippageMessage>
          (ReasonReact.string("Limit additional price slippage: 1% "))
          <sup> iInCircle </sup>
        </span>
        <span className=Style.slippageChange>
          (ReasonReact.string(" Change"))
        </span>
      </div>
    </div>
    <div className=Style.center>
      <button className=(Style.swapButton(address))>
        <span className=Style.swapButtonText>
          (ReasonReact.string("Swap XTZ to ABC"))
        </span>
      </button>
    </div>
  </div>;
