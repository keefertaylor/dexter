module Style = {
  open Css;

  let navbar =
    style([
      display(`flex),
      justifyContent(`flexEnd),
      alignContent(`flexEnd),
      paddingTop(px(10)),
    ]);

  let tutorials =
    style([display(`flex), flex(`num(1.0)), fontSize(px(18))]);

  let settings =
    style([display(`flex), flex(`num(1.0)), fontSize(px(18))]);

  let tab =
    style([
      display(`flex),
      /* flex(`num(0.5)), */
      cursor(`pointer),
      /* justifyContent(`center), */
      fontSize(px(18)),
      fontFamily("Source Sans Pro, Semibold"),
      paddingTop(px(10)),
      paddingBottom(px(15)),
    ]);

  let tabInternal = style([display(`table)]);

  let tabTutorials = style([paddingRight(`px(50))]);

  let icon = style([width(`px(24)), height(`px(24))]);

  let tabTitle =
    style([
      display(`tableCell),
      verticalAlign(`middle),
      paddingLeft(`px(8)),
    ]);
};

[@react.component]
let make = () =>
  <div className=Style.navbar>
    <div className=Style.tab>
      <div className=(Css.merge([Style.tabInternal, Style.tabTutorials]))>
        <img className=Style.icon src="round-help-button-grey.svg" />
        <span className=Style.tabTitle>
          (ReasonReact.string("Tutorials/About"))
        </span>
      </div>
    </div>
    <div className=Style.tab>
      <div className=Style.tabInternal>
        <img className=Style.icon src="settings-grey.svg" />
        <span className=Style.tabTitle>
          (ReasonReact.string("Settings"))
        </span>
      </div>
    </div>
  </div>;
