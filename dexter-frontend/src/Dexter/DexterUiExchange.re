module Style = {
  open Css;

  let row = style([display(`flex)]);

  let columnSide = style([flex(`num(40.0))]);
  let columnSidePadding = style([paddingLeft(`px(47))]);

  let columnCenter = style([flex(`num(20.0))]);

  let action =
    style([
      color(DexterUiColor.grey),
      fontSize(px(30)),
      DexterUiFont.boldFont,
      letterSpacing(`px(1)),
    ]);

  let space = style([marginTop(`px(10))]);

  let balanceGroup = style([marginTop(`px(24))]);
  let balanceToken = style([display(`table), cursor(`pointer)]);

  let token = style([width(`px(48)), height(`px(48))]);
  let tokenText =
    style([
      color(DexterUiColor.black),
      fontSize(`px(42)),
      DexterUiFont.boldFont,
      display(`tableCell),
      paddingLeft(`px(16)),
      verticalAlign(`middle),
    ]);

  let balance =
    style([
      color(DexterUiColor.grey),
      fontSize(px(16)),
      DexterUiFont.defaultFont,
    ]);

  let amount = style([fontSize(px(18)), color(DexterUiColor.grey)]);

  let amountBox =
    style([
      unsafe("borderRadius", "10px"),
      borderColor(DexterUiColor.lightGrey),
      borderStyle(solid),
      borderWidth(px(1)),
      padding2(~v=px(10), ~h=px(20)),
      marginTop(px(5)),
      marginBottom(px(50)),
      marginRight(px(100)),
    ]);

  let verticalBar =
    style([
      width(`px(0)),
      height(`px(96)),
      borderWidth(`px(1)),
      borderStyle(`solid),
      borderColor(DexterUiColor.lightGrey),
      marginTop(`px(38)),
      marginBottom(`px(38)),
    ]);

  let fullWidth = style([width(`percent(100.0))]);

  let centerSymbolContainer = style([display(`table), margin(`auto)]);

  let circle =
    style([
      backgroundColor(blue),
      backgroundRepeat(`noRepeat),
      backgroundOrigin(`paddingBox),
      borderRadius(`percent(50.0)),
      width(`px(48)),
      height(`px(48)),
      boxShadow(
        Shadow.box(
          ~x=px(0),
          ~y=px(0),
          ~blur=px(12),
          DexterUiColor.shadowColor,
        ),
      ),
      position(`relative),
      marginLeft(`percent(-50.0)),
      cursor(`pointer),
    ]);

  let circleImg =
    style([
      width(`px(21)),
      height(`px(25)),
      position(`absolute),
      margin(`auto),
      top(`px(0)),
      left(`px(0)),
      right(`px(0)),
      bottom(`px(0)),
    ]);

  let tokenAmount =
    style([
      fontSize(px(42)),
      color(DexterUiColor.grey),
      DexterUiFont.defaultFont,
      width(`percent(50.0)),
      selector("input:focus", [unsafe("border", "none")]),
      unsafe("border", "none"),      
    ]);

  let usdAmount =
    style([
      fontSize(px(18)),
      color(DexterUiColor.grey),
      DexterUiFont.boldFont,
    ]);

  let verticalAlign = style([display(tableCell), verticalAlign(middle)]);
  let downArrow =
    style([
      width(`px(14)),
      height(`px(14)),
      borderColor(hex("758DA6")),
      borderStyle(solid),
      borderTopWidth(px(0)),
      borderRightWidth(px(3)),
      borderBottomWidth(`px(3)),
      borderLeftWidth(`px(0)),
      display(inlineBlock),
      marginLeft(`px(31)),
      transform(rotate(`deg(45.0))),
    ]);

  let upArrow =
    style([
      width(`px(14)),
      height(`px(14)),
      borderColor(hex("758DA6")),
      borderStyle(solid),
      borderTopWidth(px(0)),
      borderRightWidth(px(3)),
      borderBottomWidth(`px(3)),
      borderLeftWidth(`px(0)),
      display(inlineBlock),
      marginLeft(`px(31)),
      transform(rotate(`deg(-135.0))),
    ]);

  let tokenSelector =
    style([
      position(`absolute),
      zIndex(100),
      paddingRight(`px(48)),
      backgroundColor(hex("FFFFFF")),
      boxShadow(
        Shadow.box(~x=px(0), ~y=px(10), ~blur=px(28), hex("041A441F")),
      ),
      borderRadius(`px(10)),
    ]);

  let tokenSelectorContainer = style([position(`relative)]);
};

let leftTitle = (action: DexterUiAction.t) =>
  ReasonReact.string(
    switch (action) {
    | Swap => "Swap from:"
    | AddLiquidity => "Deposit:"
    | RemoveLiquidity => "Pool tokens:"
    | Receive => ""
    },
  );

let rightTitle = (action: DexterUiAction.t) =>
  ReasonReact.string(
    switch (action) {
    | Swap => "To:"
    | AddLiquidity => "Deposit (estimate):"
    | RemoveLiquidity => "Output (estimate)"
    | Receive => ""
    },
  );

type state = {
  showLeft: bool,
  showRight: bool,
  leftAmount: int,
  rightAmount: int,  
};

type action =
  | ShowLeft(bool)
  | ShowRight(bool);

/**
 * reusuable code for Swap, Add Liquidity and Remove Liquidity
 */
[@react.component]
let make =
    (
      ~action: DexterUiAction.t,
      ~tokenAmounts: list(DexterUiTokenAmount.t),
      ~exchanges: list(DexterTypes.Exchange.t),
      ~leftToken: DexterUiTokenAmount.t,
      ~rightToken: DexterUiTokenAmount.t,
      ~onLeftTokenChange,
      ~onRightTokenChange,
      ~onSwapLeftAndRight,
    ) => {
  Js.Console.log(exchanges);
  let (state, dispatch) =
    React.useReducer(
      (state, action) =>
        switch (action) {
        | ShowLeft(showLeft) => {...state, showLeft}
        | ShowRight(showRight) => {...state, showRight}
        },
      {showLeft: false, showRight: false, leftAmount: 0, rightAmount: 0},
    );

  <>
    <div className=Style.row>
      <div className=Style.columnSide>
        <div className=Style.columnSidePadding>
          <div className=Style.action> (leftTitle(action)) </div>
          <div className=Style.balanceGroup>
            <div
              className=Style.balanceToken
              onClick=(_ => dispatch(ShowLeft(! state.showLeft)))>
              <img className=Style.token src="tezos.png" />
              <span className=Style.tokenText>
                (ReasonReact.string(leftToken.symbol))
              </span>
              <div className=Style.verticalAlign>
                (
                  if (state.showLeft) {
                    <div className=Style.upArrow />;
                  } else {
                    <div className=Style.downArrow />;
                  }
                )
              </div>
            </div>
            (
              if (state.showLeft) {
                <div className=Style.tokenSelectorContainer>
                  <div className=Style.tokenSelector>
                    <DexterUiTokenSearch
                      tokenAmounts
                      onTokenChange=(
                        token => {
                          dispatch(ShowLeft(false));
                          onLeftTokenChange(token);
                        }
                      )
                    />
                  </div>
                </div>;
              } else {
                ReasonReact.null;
              }
            )
            <div className=Style.balance>
              (ReasonReact.string("You have: " ++ string_of_int(leftToken.amount)))
            </div>
          </div>
          <div className=Style.space>
            <div className=Style.amount>
              (ReasonReact.string("Amount:"))
            </div>
            <div className=Style.amountBox>
              <input className=Style.tokenAmount value=(string_of_int(state.leftAmount)) onChange=(_ => ()) />
              <div className=Style.usdAmount>
                (ReasonReact.string("$0.00"))
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className=Style.columnCenter>
        <div className=Style.fullWidth>
          <div className=Style.centerSymbolContainer>
            <div className=Style.verticalBar />
            <div className=Style.circle onClick=(_ => onSwapLeftAndRight())>
              <img className=Style.circleImg src="sort-white.svg" />
            </div>
            <div className=Style.verticalBar />
          </div>
        </div>
      </div>
      <div className=Style.columnSide>
        <div className=Style.action> (rightTitle(action)) </div>
        <div className=Style.balanceGroup>
          <div
            className=Style.balanceToken
            onClick=(_ => dispatch(ShowRight(! state.showRight)))>
            <img className=Style.token src="abc.png" />
            <span className=Style.tokenText>
              (ReasonReact.string(rightToken.symbol))
            </span>
            <div className=Style.verticalAlign>
              (
                if (state.showRight) {
                  <div className=Style.upArrow />;
                } else {
                  <div className=Style.downArrow />;
                }
              )
            </div>
          </div>
          (
            if (state.showRight) {
              <div className=Style.tokenSelectorContainer>
                <div className=Style.tokenSelector>
                  <DexterUiTokenSearch
                    tokenAmounts
                    onTokenChange=(
                      token => {
                        dispatch(ShowRight(false));
                        onRightTokenChange(token);
                      }
                    )
                  />
                </div>
              </div>;
            } else {
              ReasonReact.null;
            }
          )
          <div className=Style.balance>
            (ReasonReact.string("You have: " ++ string_of_int(rightToken.amount)))
          </div>
        </div>
        <div className=Style.space>
          <div className=Style.amount> (ReasonReact.string("Amount:")) </div>
          <div className=Style.amountBox>
            <input className=Style.tokenAmount value=(string_of_int(state.rightAmount)) onChange=(_ => ()) />
            <div className=Style.usdAmount>
              (ReasonReact.string("$0.00"))
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className=Style.row>
      <div className=Style.columnSide />
      <div className=Style.columnCenter> <DexterUiExchangeRate /> </div>
      <div className=Style.columnSide />
    </div>
  </>;
};
