module Style = {
  open Css;


  let exchangeRateBox =
    style([
      borderWidth(`px(1)),
      borderStyle(`solid),
      borderColor(hex("C4D0DC")),
      borderRadius(`px(17)),
    ]);

  let exchangeRate = style([textAlign(`center), color(DexterUiColor.grey), fontSize(`px(16))]);

  let profits = style([width(`px(16)), height(`px(10))]);

  let profitsText = style([marginLeft(`px(8))]);
};

[@react.component]
let make = () =>
  <div className=Style.exchangeRateBox>
    <div className=Style.exchangeRate>
      <img className=Style.profits src="profits.svg" />
      <span className=Style.profitsText>
        (ReasonReact.string("1 XTZ = 2 ABC"))
      </span>
    </div>
  </div>;
