module Style = {
  open Css;

  let center = style([display(table), margin2(~v=px(75), ~h=auto)]);

  let message =
    style([
      textAlign(`center),
      display(`inlineBlock),
      DexterUiFont.defaultFont,
      fontSize(`px(16)),
      color(DexterUiColor.grey),
      marginBottom(`px(34)),
    ]);

  let firstMessage = style([marginBottom(`px(24))]);

  let money = style([DexterUiFont.boldFont]);

  let button = (address: option(string)) =>
    style([
      switch (address) {
      | Some(_address) => backgroundColor(DexterUiColor.blue)
      | None => backgroundColor(DexterUiColor.lightBlue)
      },
      switch (address) {
      | Some(_address) => cursor(`pointer)
      | None => cursor(`default)
      },
      backgroundRepeat(`noRepeat),
      backgroundOrigin(`paddingBox),
      boxShadow(
        Shadow.box(
          ~x=px(0),
          ~y=px(12),
          ~blur=px(26),
          DexterUiColor.shadowColor,
        ),
      ),
      borderRadius(`px(10)),
      borderStyle(`none),
      padding2(~v=px(20), ~h=px(20)),
    ]);

  let buttonText =
    style([
      fontSize(`px(18)),
      color(DexterUiColor.white),
      DexterUiFont.boldFont,
      textAlign(`center),
    ]);
};

[@react.component]
let make =
    (
      ~activeBaker: option(DexterTypes.Baker.t),
      ~address: option(string),
      ~bakers: list(DexterTypes.Baker.t),
      ~exchanges: list(DexterTypes.Exchange.t),
      ~onBakerChange,
      ~tokenAmounts: list(DexterUiTokenAmount.t),
      ~leftToken: DexterUiTokenAmount.t,
      ~rightToken: DexterUiTokenAmount.t,
      ~onLeftTokenChange,
      ~onRightTokenChange,
      ~onSwapLeftAndRight,
    ) =>
  <div>
    <DexterUiExchange
      action=DexterUiAction.AddLiquidity
      tokenAmounts
      exchanges
      leftToken
      rightToken
      onLeftTokenChange
      onRightTokenChange
      onSwapLeftAndRight
    />
    <DexterUiBakerSelector activeBaker bakers onBakerChange />
    <div className=Style.center>
      <div className=Style.message>
        <div className=Style.firstMessage>
          (ReasonReact.string("You are adding "))
          <span className=Style.money> (ReasonReact.string("10 XTZ")) </span>
          (ReasonReact.string(" and "))
          <span className=Style.money> (ReasonReact.string("~20 ABC")) </span>
          (ReasonReact.string(" = "))
          <span className=Style.money>
            (ReasonReact.string("1,000 ABC"))
          </span>
          (ReasonReact.string(" pool tokens."))
        </div>
        <div>
          (ReasonReact.string("Est. annual reward "))
          <span className=Style.money>
            (ReasonReact.string("66.60 XTZ"))
          </span>
          (ReasonReact.string(" ($60)"))
        </div>
      </div>
    </div>
    <div className=Style.center>
      <button className=(Style.button(address))>
        <span className=Style.buttonText>
          (ReasonReact.string("Add liquidity"))
        </span>
      </button>
    </div>
  </div>;
