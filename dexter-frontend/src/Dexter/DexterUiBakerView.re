module Style = {
  open Css;

  let container = (active: bool) =>
    style([
      borderColor(
        if (active) {DexterUiColor.blue} else {DexterUiColor.lightGrey},
      ),
      borderStyle(solid),
      borderWidth(px(1)),
      borderRadius(`px(10)),
    ]);

  let flexbox =
    style([
      display(`flex),
      marginLeft(`px(24)),
      marginRight(`px(24)),
      marginTop(`px(16)),
      marginBottom(`px(16)),
    ]);

  let icon = style([height(`px(32)), width(`px(32))]);

  let name = style([fontSize(`px(14)), color(DexterUiColor.black)]);

  let address = style([fontSize(`px(14)), color(DexterUiColor.grey)]);
};

/**
 * display
 * baker icon
 * baker name
 * baker public address
 */
[@react.component]
let make = (~baker: DexterTypes.Baker.t, ~active: bool) =>
  <div className=(Style.container(active))>
    <div className=Style.flexbox>
      <div>
        (
          switch (baker.iconPath) {
          | Some(iconPath) => <img className=Style.icon src=iconPath />
          | None => <img className=Style.icon />
          }
        )
      </div>
      <div>
        <div> (ReasonReact.string(baker.name)) </div>
        <div> (ReasonReact.string(baker.address)) </div>
      </div>
    </div>
  </div>;
