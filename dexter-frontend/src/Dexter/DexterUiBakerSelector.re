module Style = {
  open Css;

  let row = style([display(`flex)]);

  let columnSide =
    style([
      flex(`num(40.0)),
      paddingLeft(`px(48)),
      paddingRight(`px(86)),
    ]);

  let columnEmpty = style([flex(`num(60.0))]);

  let title =
    style([
      marginBottom(`px(9)),
      color(DexterUiColor.grey),
      fontSize(`px(18)),
      DexterUiFont.boldFont,
    ]);

  let baker = style([marginBottom(`px(24)), cursor(`pointer)]);
};

let nbsp = [%raw {|'\u00a0'|}];

type state = {activeBaker: option(DexterTypes.Baker.t)};

type action =
  | UpdateActiveBaker(DexterTypes.Baker.t);

let setActiveBaker =
    (baker: DexterTypes.Baker.t, activeBaker: option(DexterTypes.Baker.t)) =>
  switch (activeBaker) {
  | Some(activeBaker) =>
    if (baker == activeBaker) {
      None;
    } else {
      Some(baker);
    }
  | None => Some(baker)
  };

let compareBaker = (x: DexterTypes.Baker.t, y: option(DexterTypes.Baker.t)) =>
  switch (y) {
  | None => false
  | Some(y') => x == y'
  };

[@react.component]
let make =
    (
      ~activeBaker: option(DexterTypes.Baker.t),
      ~bakers: list(DexterTypes.Baker.t),
      ~onBakerChange,
    ) => {
  let (state, dispatch) =
    React.useReducer(
      (state, action) =>
        switch (action) {
        | UpdateActiveBaker(baker) => {
            activeBaker: setActiveBaker(baker, state.activeBaker),
          }
        },
      {activeBaker: activeBaker}: state,
    );

  <div className=Style.row>
    <div className=Style.columnSide>
      <div className=Style.title>
        (ReasonReact.string("Choose your baker"))
      </div>
      (
        List.map(
          (baker: DexterTypes.Baker.t) =>
            <div
              className=Style.baker
              onClick=(
                _event => {
                  dispatch(UpdateActiveBaker(baker));
                  onBakerChange(setActiveBaker(baker, state.activeBaker));
                }
              )>
              <DexterUiBakerView
                baker
                active=(compareBaker(baker, state.activeBaker))
              />
            </div>,
          bakers,
        )
        |> Array.of_list
        |> ReasonReact.array
      )
    </div>
    <div className=Style.columnEmpty> nbsp </div>
  </div>;
};
