type t = {
  symbol: string,
  name: string,
  amount: int,
};
