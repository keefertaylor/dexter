open Css;

let black       = hex("082344");
let grey        = hex("758DA6");
let lightGrey   = hex("C4D0DC");
let blue        = hex("0258FF");
let lightBlue   = hex("A3BDFB");
let shadowColor = hex("0A28642E");
let white       = hex("FFFFFF");
