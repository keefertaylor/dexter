module Style = {
  open Css;

  let navbar =
    style([
      display(`flex),
      justifyContent(spaceBetween),
      paddingTop(px(10)),
    ]);

  let title =
    style([
      display(`flex),
      flex(`num(0.5)),
      fontSize(px(26)),
      fontFamily("Source Sans Pro, Semibold"),
      color(hex("495B6D")),
      marginLeft(`percent(2.0)),
    ]);

  let tab =
    style([
      display(`flex),
      flex(`num(0.5)),
      cursor(`pointer),
      justifyContent(`center),
      fontSize(px(18)),
      fontFamily("Source Sans Pro, Semibold"),
      paddingTop(px(10)),
      paddingBottom(px(15)),
    ]);

  let active =
    style([
      unsafe("backgroundColor", "white"),
      color(hex("0258FF")),
      fontSize(`px(18)),
      borderTopLeftRadius(px(5)),
      borderTopRightRadius(px(5)),
    ]);

  let inactive = style([unsafe("backgroundColor", "inherit")]);

  let tabInternal = style([display(`table)]);

  let swapIcon = style([width(`px(24)), height(`px(16))]);
  let addIcon = style([width(`px(24)), height(`px(24))]);

  let tabTitle =
    style([
      display(`tableCell),
      verticalAlign(`middle),
      paddingLeft(`px(8)),
    ]);
};

let isActive = (x: DexterUiAction.t, y: DexterUiAction.t) =>
  if (x == y) {
    Style.active;
  } else {
    Style.inactive;
  };

let getIconSrc = (action: DexterUiAction.t, currentAction: DexterUiAction.t) => {
  let actionString =
    switch (action) {
    | Swap => "sort"
    | AddLiquidity => "add"
    | RemoveLiquidity => "minus"
    | Receive => "money"
    };
  if (action == currentAction) {
    actionString ++ "-blue.svg";
  } else {
    actionString ++ "-grey.svg";
  };
};

[@react.component]
let make = (~action: DexterUiAction.t, ~onChange) =>
  <div className=Style.navbar>
    <div className=Style.title> (ReasonReact.string("Dexter")) </div>
    <div
      className=(
        Css.merge([Style.tab, isActive(DexterUiAction.Swap, action)])
      )
      onClick=(_event => onChange(DexterUiAction.Swap))>
      <div className=Style.tabInternal>
        <img
          className=Style.swapIcon
          src=(getIconSrc(DexterUiAction.Swap, action))
        />
        <span className=Style.tabTitle> (ReasonReact.string("Swap")) </span>
      </div>
    </div>
    <div
      className=(
        Css.merge([Style.tab, isActive(DexterUiAction.AddLiquidity, action)])
      )
      onClick=(_event => onChange(DexterUiAction.AddLiquidity))>
      <div className=Style.tabInternal>
        <img
          className=Style.addIcon
          src=(getIconSrc(DexterUiAction.AddLiquidity, action))
        />
        <span className=Style.tabTitle>
          (ReasonReact.string("Add Liquidity"))
        </span>
      </div>
    </div>
    <div
      className=(
        Css.merge([
          Style.tab,
          isActive(DexterUiAction.RemoveLiquidity, action),
        ])
      )
      onClick=(_event => onChange(DexterUiAction.RemoveLiquidity))>
      <div className=Style.tabInternal>
        <img
          className=Style.addIcon
          src=(getIconSrc(DexterUiAction.RemoveLiquidity, action))
        />
        <span className=Style.tabTitle>
          (ReasonReact.string("Remove Liquidity"))
        </span>
      </div>
    </div>
    <div
      className=(
        Css.merge([Style.tab, isActive(DexterUiAction.Receive, action)])
      )
      onClick=(_event => onChange(DexterUiAction.Receive))>
      <div className=Style.tabInternal>
        <img
          className=Style.addIcon
          src=(getIconSrc(DexterUiAction.Receive, action))
        />
        <span className=Style.tabTitle>
          (ReasonReact.string("Receive"))
        </span>
      </div>
    </div>
  </div>;
