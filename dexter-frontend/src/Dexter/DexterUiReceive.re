module Style = {
  open Css;

  let center = style([display(table), margin2(~v=px(75), ~h=auto)]);

  let title =
    style([
      fontSize(`px(25)),
      DexterUiFont.boldFont,
      color(DexterUiColor.black),
    ]);

  let addressTitle =
    style([
      fontSize(`px(16)),
      DexterUiFont.boldFont,
      color(DexterUiColor.black),
    ]);

  let address =
    style([
      fontSize(`px(16)),
      DexterUiFont.defaultFont,
      color(DexterUiColor.black),
    ]);

  let button =
    style([
      backgroundColor(DexterUiColor.blue),
      cursor(`pointer),
      backgroundRepeat(`noRepeat),
      backgroundOrigin(`paddingBox),
      boxShadow(
        Shadow.box(
          ~x=px(0),
          ~y=px(12),
          ~blur=px(26),
          DexterUiColor.shadowColor,
        ),
      ),
      borderRadius(`px(10)),
      borderStyle(`none),
      padding2(~v=px(20), ~h=px(20)),
    ]);

  let buttonText =
    style([
      fontSize(`px(18)),
      color(DexterUiColor.white),
      DexterUiFont.boldFont,
      textAlign(`center),
    ]);
};

[@react.component]
let make = (~address: option(string)) =>
  <div className=Style.center>
    (
      switch (address) {
      | None =>
        <>
          <div className=Style.title>
            (ReasonReact.string("Please connect your wallet first!"))
          </div>
          <div className=Style.center>
            <div className=Style.button>
              <span className=Style.buttonText>
                (ReasonReact.string("Connect wallet"))
              </span>
            </div>
          </div>
        </>
      | Some(address) =>
        <>
          <div className=Style.title>
            (ReasonReact.string("Scan QR code to receive"))
          </div>
          <div className=Style.addressTitle>
            (ReasonReact.string("Your wallet address:"))
          </div>
          <div className=Style.address> (ReasonReact.string(address)) </div>
          <div> (ReasonReact.string("Request amount")) </div>
          <input />
          <div className=Style.center>
            <div className=Style.button>
              <span className=Style.buttonText>
                (ReasonReact.string("Generate new QR code"))
              </span>
            </div>
          </div>
        </>
      }
    )
  </div>;
