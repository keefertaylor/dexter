module Style = {
  open Css;
  
  let container = style([
    paddingLeft(`px(80)),
    paddingRight(`px(80)),
    /* height(`px(176)), */
    /* width(`px(596)), */
  ]);
};

[@react.component]
let make = (~address: option(string), ~exchanges: list(DexterTypes.Exchange.t)) => {
  <div className=Style.container>
    <DexterUiSecondaryNavBar />
    <DexterUiConnectWallet address exchanges />
  </div>
};
