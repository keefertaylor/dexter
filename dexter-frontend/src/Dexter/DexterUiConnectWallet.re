module Style = {
  open Css;

  let container =
    style([
      width(`percent(100.0)),
      borderRadius(`px(10)),
      backgroundColor(hex("FFFFFF")),
      backgroundRepeat(`noRepeat),
      backgroundOrigin(`paddingBox),
      boxShadow(
        Shadow.box(~x=px(0), ~y=px(10), ~blur=px(28), hex("0A28640D")),
      ),
    ]);

  let status =
    style([
      paddingTop(`px(25)),
      color(hex("082344")),
      fontSize(`px(25)),
      fontFamily("Source Sans Pro, Semibold"),
      textAlign(`center),
    ]);

  let message =
    style([
      color(hex("758DA6")),
      fontSize(`px(16)),
      fontFamily("Source Sans Pro, Semibold"),
      textAlign(`center),
    ]);

  let buttonContainer =
    style([
      marginTop(`px(18)),
      display(`flex),
      justifyContent(`center),
    ]);

  let button =
    style([
      cursor(`pointer),
      backgroundColor(hex("0258FF")),
      backgroundRepeat(`noRepeat),
      backgroundOrigin(`paddingBox),
      borderRadius(`px(10)),
      height(`px(48)),
      marginBottom(`px(32)),
      width(`px(200)),
    ]);

  let buttonText =
    style([
      color(hex("FFFFFF")),
      textAlign(`center),
      fontSize(`px(18)),
      fontFamily("Source Sans Pro, Semibold"),
    ]);

  let addressContainer =
    style([
      paddingTop(`px(16)),
      paddingLeft(`px(26)),
      marginLeft(`px(80)),
      marginRight(`px(80)),
      height(`px(128)),
      width(`px(596)),
      borderRadius(`px(10)),
      backgroundColor(hex("FFFFFF")),
      backgroundRepeat(`noRepeat),
      backgroundOrigin(`paddingBox),
      boxShadow(
        Shadow.box(~x=px(0), ~y=px(10), ~blur=px(28), hex("0A28640D")),
      ),
    ]);
  let addressStatus = style([display(`flex)]);

  let ellipse =
    style([
      alignItems(`center),
      height(`px(8)),
      width(`px(8)),
      backgroundColor(hex("38C636")),
      backgroundRepeat(`noRepeat),
      backgroundOrigin(`paddingBox),
      borderRadius(`percent(50.0)),
    ]);

  let addressOrigin =
    style([
      alignItems(`center),
      color(hex("C4D0DC")),
      fontFamily("Source Sans Pro, Regular"),
      fontSize(`px(16)),
      paddingLeft(`px(6)),
    ]);

  let managerName = style([DexterUiFont.boldFont, fontSize(`px(18))]);

  let address = style([DexterUiFont.defaultFont, fontSize(`px(16))]);
};

/* State declaration */
type state = {
  address: option(string),
  zed: string,
};

/* Action declaration */
type action =
  | UpdateAddress(option(string));

[@react.component]
let make = (~address, ~exchanges: list(DexterTypes.Exchange.t)) => {
  let (state: state, dispatch) =
    React.useReducer(
      (state: state, action) =>
        switch (action) {
        | UpdateAddress(address) => {...state, address}
        },
      {address, zed: ""},
    );
  switch (state.address) {
  | None =>
    <div className=Style.container>
      <div className=Style.status>
        (ReasonReact.string("No wallet connected!"))
      </div>
      <div className=Style.message>
        (
          ReasonReact.string(
            "Please connect your wallet using the available options.",
          )
        )
      </div>
      <div className=Style.buttonContainer>
        <button
          className=Style.button
          onClick=(
            _event =>
              TezBridge.getSource()
            |> Js.Promise.then_(result => {
                /* get the users address from TezBridge */
                dispatch(UpdateAddress(Some(result)));
                let promises = List.map((exchange: DexterTypes.Exchange.t) => {
                  Token.fetch(Tezos.ContractId.toString(exchange.exchangeContractId), result)
                }, exchanges);
                Js.Promise.all(promises |> Array.of_list) |> Js.Promise.then_(_ => { Js.Console.log("good job"); Js.Promise.resolve() }) |> ignore;
                   Js.Promise.resolve();
              })
              
              |> ignore
          )>
          <span className=Style.buttonText>
            (ReasonReact.string("Connect wallet"))
          </span>
        </button>
      </div>
    </div>
  | Some(address) =>
    <div className=Style.addressContainer>
      <div className=Style.addressStatus>
        <div className=Style.ellipse />
        <div className=Style.addressOrigin>
          (ReasonReact.string("TezBridge address"))
        </div>
      </div>
      <div className=Style.managerName>
        (ReasonReact.string("Manager Name"))
      </div>
      <div className=Style.address> (ReasonReact.string(address)) </div>
    </div>
  };
};
