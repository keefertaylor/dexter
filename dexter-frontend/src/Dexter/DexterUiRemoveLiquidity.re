module Style = {
  open Css;

  let center = style([display(table), margin2(~v=px(75), ~h=auto)]);

  let message =
    style([
      textAlign(`center),
      display(`inlineBlock),
      DexterUiFont.defaultFont,
      fontSize(`px(16)),
      color(DexterUiColor.grey),
      marginBottom(`px(34)),
    ]);

  let firstMessage = style([marginBottom(`px(24))]);

  let money = style([DexterUiFont.boldFont]);

  let button = (address: option(string)) =>
    style([
      switch (address) {
      | Some(_address) => backgroundColor(DexterUiColor.blue)
      | None => backgroundColor(DexterUiColor.lightBlue)
      },
      switch (address) {
      | Some(_address) => cursor(`pointer)
      | None => cursor(`default)
      },
      backgroundRepeat(`noRepeat),
      backgroundOrigin(`paddingBox),
      boxShadow(
        Shadow.box(
          ~x=px(0),
          ~y=px(12),
          ~blur=px(26),
          DexterUiColor.shadowColor,
        ),
      ),
      borderRadius(`px(10)),
      borderStyle(`none),
      padding2(~v=px(20), ~h=px(20)),
    ]);

  let buttonText =
    style([
      fontSize(`px(18)),
      color(DexterUiColor.white),
      DexterUiFont.boldFont,
      textAlign(`center),
    ]);
};

[@react.component]
let make =
    (
      ~address: option(string),
      ~tokenAmounts: list(DexterUiTokenAmount.t),
      ~exchanges: list(DexterTypes.Exchange.t),
      ~leftToken: DexterUiTokenAmount.t,
      ~rightToken: DexterUiTokenAmount.t,
      ~onLeftTokenChange,
      ~onRightTokenChange,
      ~onSwapLeftAndRight,
    ) =>
  <div>
    <DexterUiExchange
      action=DexterUiAction.RemoveLiquidity
      tokenAmounts
      exchanges
      leftToken
      rightToken
      onLeftTokenChange
      onRightTokenChange
      onSwapLeftAndRight
    />
    <div className=Style.center>
      <div className=Style.message>
        <div className=Style.firstMessage>
          (ReasonReact.string("You are removing "))
          <span className=Style.money>
            (ReasonReact.string("1,500 ABC"))
          </span>
          (ReasonReact.string(" ($30) pool tokens "))
        </div>
        <div>
          (ReasonReact.string("worth "))
          <span className=Style.money> (ReasonReact.string("~15 XTZ")) </span>
          (ReasonReact.string(" ($15) and "))
          <span className=Style.money> (ReasonReact.string("~30 ABC")) </span>
          (ReasonReact.string(" ($15) at current exchange rate"))
        </div>
      </div>
    </div>
    <div className=Style.center>
      <button className=(Style.button(address))>
        <span className=Style.buttonText>
          (ReasonReact.string("Remove liquidity"))
        </span>
      </button>
    </div>
  </div>;
