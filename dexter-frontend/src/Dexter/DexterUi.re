module Style = {
  open Css;

  let main = style([backgroundColor(hex("F7F9FB"))]);
  let space = style([marginBottom(px(50))]);

  let row = style([display(`flex)]);
  let leftColumn = style([flexBasis(`percent(65.0))]);
  let rightColumn = style([flexBasis(`percent(35.0))]);
};

let nbsp = [%raw {|'\u00a0'|}];

type state = {
  action: DexterUiAction.t,
  address: option(string),
  activeBaker: option(DexterTypes.Baker.t),
  leftToken: DexterUiTokenAmount.t,
  rightToken: DexterUiTokenAmount.t,
};

type action =
  | UpdateAction(DexterUiAction.t)
  | UpdateActiveBaker(option(DexterTypes.Baker.t))
  | UpdateLeftToken(DexterUiTokenAmount.t)
  | UpdateRightToken(DexterUiTokenAmount.t)
  | SwapLeftAndRight;

/** arbitrary values */
let tokenAmounts: list(DexterUiTokenAmount.t) = [
  {amount: 300, name: "Tez", symbol: "XTZ"},
  {amount: 12, name: "ABC", symbol: "ABC"},
  {amount: 105, name: "Tezos Tacos", symbol: "TZT"},
  {amount: 2000, name: "Tezos Gold", symbol: "TZG"},
];

/** pool tokens for each, exchange rate to usd*/

[@react.component]
let make =
    (
      ~bakers: list(DexterTypes.Baker.t),
      ~exchanges: list(DexterTypes.Exchange.t),
    ) => {
  let (state, dispatch) =
    React.useReducer(
      (state, action) =>
        switch (action) {
        | UpdateAction(action) => {...state, action}
        | UpdateActiveBaker(activeBaker) => {...state, activeBaker}
        | UpdateLeftToken(leftToken) => {...state, leftToken}
        | UpdateRightToken(rightToken) => {...state, rightToken}
        | SwapLeftAndRight => {
            ...state,
            leftToken: state.rightToken,
            rightToken: state.leftToken,
          }
        },
      {
        action: DexterUiAction.Swap,
        address: None,
        activeBaker: None,
        leftToken: List.hd(tokenAmounts),
        rightToken: List.hd(List.tl(tokenAmounts)),
      }: state,
    );

  <div className=Style.main>
    <div className=Style.row>
      <div className=Style.leftColumn>
        <DexterUiActionWindow
          address=state.address
          action=state.action
          activeBaker=state.activeBaker
          bakers
          exchanges      
          tokenAmounts
          leftToken=state.leftToken
          rightToken=state.rightToken
          onChange=(action => dispatch(UpdateAction(action)))
          onBakerChange=(baker => dispatch(UpdateActiveBaker(baker)))
          onLeftTokenChange=(token => dispatch(UpdateLeftToken(token)))
          onRightTokenChange=(token => dispatch(UpdateRightToken(token)))
          onSwapLeftAndRight=(_ => dispatch(SwapLeftAndRight))
        />
      </div>
      <div className=Style.rightColumn>
        <DexterUiSecondaryWindow address=state.address exchanges />
      </div>
    </div>
    <div className=Style.space> nbsp </div>
  </div>;
};
