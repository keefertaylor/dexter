module Style = {
  open Css;
  let container =
    style([
      flexDirection(`row),
    ])

  let circle =
    style([
      width(`px(48)),
      height(`px(48)),
      backgroundColor(DexterUiColor.blue),
      backgroundRepeat(`noRepeat),
      backgroundOrigin(`paddingBox),
      borderRadius(`percent(50.0)),
    ]);
};

[@react.component]
let make = (~account: option(string), ~poolTokens: list(int)) => {
  Js.Console.log(poolTokens);
  switch (account) {
  | Some(_account) =>
    <div className=Style.container>
      <div className=Style.circle> (ReasonReact.string("1")) </div>
      <table>
        <tr>
          <td colSpan=2>
            <span> (ReasonReact.string("1,500 ABC")) </span>
            <span> (ReasonReact.string("(0.01%)")) </span>
          </td>
          <td> (ReasonReact.string("15 XTZ")) </td>
          <td> (ReasonReact.string("30 ABC")) </td>
        </tr>
        <tr>
          <td colSpan=2> (ReasonReact.string("Pool tokens")) </td>
          <td> (ReasonReact.string("$15")) </td>
          <td> (ReasonReact.string("$15")) </td>
        </tr>
        <tr> <td colSpan=4> (ReasonReact.string("Rewards")) </td> </tr>
        <tr>
          <td> (ReasonReact.string("8.8727 XTZ")) </td>
          <td> (ReasonReact.string("$7.74")) </td>
          <td> (ReasonReact.string("26.6181 XTZ")) </td>
          <td> (ReasonReact.string("$22.32")) </td>
        </tr>
        <tr>
          <td> (ReasonReact.string("Monthly earning")) </td>
          <td />
          <td> (ReasonReact.string("Total earned")) </td>
          <td />
        </tr>
      </table>
    </div>
  | None =>
    <div>
      <div> (ReasonReact.string("There are no transactions yet!")) </div>
    </div>
  };
};
