type t =
  | Address(string);

let encode = (t: t) : Js.Json.t =>
  switch (t) {
  | Address(c) => Json.Encode.string(c)
  };

let decode = (json: Js.Json.t) : Belt.Result.t(t, string) =>
  switch (Json.Decode.string(json)) {
  | v => Belt.Result.Ok(Address(v))
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("Address.decode failed: " ++ error)
  };

let ofString = (candidate: string) : Belt.Result.t(t, string) =>
  if (Js.String2.startsWith(candidate, "tz1")) {
    Belt.Result.Ok(Address(candidate));
  } else {
    Belt.Result.Error(
      "Address.mk: unexpected candidate string: " ++ candidate,
    );
  };

let toString = (t: t) : string =>
  switch (t) {
  | Address(str) => str
  };

let jsPack: (string) => string = [%bs.raw
  {|
   function (input) {
     // ed25519_public_key_hash
     const bs58check = require('bs58check');
     const elliptic  = require('elliptic');
     const prefix    = new Uint8Array([6, 161, 159]);
     const bytes     = '0000' + elliptic.utils.toHex(bs58check.decode(input).slice(prefix.length));
     const len = bytes.length / 2;
     const result = [];
     result.push('050a');
     result.push(len.toString(16).padStart(8, '0'));
     result.push(bytes);
     return result.join('');
   }
|}
];

let pack = (t: t) : string =>
  switch (t) {
  | Address(str) =>
      jsPack(str);
  };    

let jsToScriptExpr: (string) => string = [%bs.raw
  {|
   function (input) {
     const blake    = require('blakejs');
     const elliptic = require('elliptic');
     const bs58check = require('bs58check');

     const prefix2 = elliptic.utils.toHex(new Uint8Array([13, 44, 64, 27]));
     const prefix = new Uint8Array([13, 44, 64, 27]);

     var a = [];
     for (var i = 0, len = input.length; i < len; i+=2) {
       a.push(parseInt(input.substr(i,2),16));
     }
  
     const hex2buf =  new Uint8Array(a);

     const blakeHash = blake.blake2b(hex2buf, null, 32);
     
     const payloadAr = typeof blakeHash === 'string' ? Uint8Array.from(Buffer.from(blakeHash, 'hex')) : blakeHash;

     const n = new Uint8Array(prefix.length + payloadAr.length);
     n.set(prefix);
     n.set(payloadAr, prefix.length);

     return bs58check.encode(Buffer.from(n.buffer));     
   }
|}                                      
];

let toScriptExpr = (t: t) : string =>
  switch (t) {
  | Address(str) =>
      jsToScriptExpr(jsPack(str));
  };    
  

type s = t;

module Comparable =
  Belt.Id.MakeComparable({
    type t = s;
    let cmp = (c0, c1) =>
      switch (c0, c1) {
      | (Address(str0), Address(str1)) =>
        Pervasives.compare(str0, str1)
      };
  });
