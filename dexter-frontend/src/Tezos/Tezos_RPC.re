/** get latest state from the exchange contract */;
/** get latest state from the token contract */;
/** get user from tezbridge */;

/**
 * type GetContractStorage
  = "chains"
  :> Capture "chain-id" ChainId
  :> "blocks"
  :> Capture "block-id" BlockId
  :> "context"
  :> "contracts"
  :> Capture "contract-id" ContractId
  :> "storage"
  :> Get '[JSON] Expression

 */;

type tezosNodeRoute =
  | Balance(string, string, string)
  | BigMap(string, string, string)
  | Storage(string, string, string);

let toRoute = (baseUrl: string, route: tezosNodeRoute) => {
  baseUrl
  ++ (
    switch (route) {
    | Balance(chainId, blockId, contractId) =>
      "/chains/"
      ++ chainId
      ++ "/blocks/"
      ++ blockId
      ++ "/context/contracts/"
      ++ contractId
      ++ "/balance"
    | BigMap(chainId, blockId, contractId) =>
      "/chains/"
      ++ chainId
      ++ "/blocks/"
      ++ blockId
      ++ "/context/contracts/"
      ++ contractId
      ++ "/big_map_get"
    | Storage(chainId, blockId, contractId) =>
      "/chains/"
      ++ chainId
      ++ "/blocks/"
      ++ blockId
      ++ "/context/contracts/"
      ++ contractId
      ++ "/storage"
    }
  );
};

let getRequest = (url, headers) =>
  Js.Promise.(
    Fetch.fetchWithInit(
      url,
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers,
        ~credentials=Include,
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(Belt.Result.Ok(json)))
    |> catch(e => resolve(Belt.Result.Error(e)))
  );

let postRequest = (url, headers, body) =>
  Js.Promise.(
    Fetch.fetchWithInit(
      url,
      Fetch.RequestInit.make(
        ~method_=Post,
        ~headers,
        ~credentials=Include,
        ~body,
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(Belt.Result.Ok(json)))
    |> catch(e => resolve(Belt.Result.Error(e)))
  );

let getBalance =
    (baseUrl, chainId, blockId, contractId)
    : Js.Promise.t(Belt.Result.t(Tezos_Mutez.t, string)) => {
  let url = toRoute(baseUrl, Balance(chainId, blockId, contractId));
  Js.Promise.(
    Fetch.fetchWithInit(
      url,
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(Tezos_Mutez.decode(json)))
    |> catch(e => {
         Js.Console.log(e);
         resolve(Belt.Result.Error(""));
       })
  );
};

let getBigMapValue =
    (baseUrl, chainId, blockId, contractId, query: Tezos_BigMapQuery.t)
    : Js.Promise.t(Belt.Result.t(Tezos_Expression.t, string)) => {
  let url = toRoute(baseUrl, BigMap(chainId, blockId, contractId));
  let body =
    query |> Tezos_BigMapQuery.encode |> Json.stringify |> Fetch.BodyInit.make;
  Js.Promise.(
    Fetch.fetchWithInit(
      url,
      Fetch.RequestInit.make(
        ~method_=Post,
        ~mode=CORS,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        ~body,
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(Tezos_Expression.decode(json)))
    |> catch(e => {
         Js.Console.log(e);
         resolve(Belt.Result.Error(""));
       })
  );
};

let getStorage =
    (baseUrl, chainId, blockId, contractId)
    : Js.Promise.t(Belt.Result.t(Tezos_Expression.t, string)) => {
  let url = toRoute(baseUrl, Storage(chainId, blockId, contractId));
  Js.Promise.(
    Fetch.fetchWithInit(
      url,
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
          |]),
        ~credentials=Omit,
        (),
      ),
    )
    |> then_(Fetch.Response.json)
    |> then_(json => resolve(Tezos_Expression.decode(json)))
    |> catch(e => {
         Js.Console.log(e);
         resolve(Belt.Result.Error(""));
       })
  );
};