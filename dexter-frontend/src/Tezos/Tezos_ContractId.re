type t =
  | ContractId(string);

let encode = (t: t) : Js.Json.t =>
  switch (t) {
  | ContractId(c) => Json.Encode.string(c)
  };

let decode = (json: Js.Json.t) : Belt.Result.t(t, string) =>
  switch (Json.Decode.string(json)) {
  | v => Belt.Result.Ok(ContractId(v))
  | exception (Json.Decode.DecodeError(error)) =>
    Belt.Result.Error("ContractId.decode failed: " ++ error)
  };

let ofString = (candidate: string) : Belt.Result.t(t, string) =>
  if (Js.String2.startsWith(candidate, "KT1")) {
    Belt.Result.Ok(ContractId(candidate));
  } else {
    Belt.Result.Error(
      "ContractId.mk: unexpected candidate string: " ++ candidate,
    );
  };

let ofStringUnsafe = (candidate: string) =>
  ContractId(candidate);

let toString = (t: t) : string =>
  switch (t) {
  | ContractId(str) => str
  };

type s = t;

module Comparable =
  Belt.Id.MakeComparable({
    type t = s;
    let cmp = (c0, c1) =>
      switch (c0, c1) {
      | (ContractId(str0), ContractId(str1)) =>
        Pervasives.compare(str0, str1)
      };
  });
