/* module type Serializable = {
     let encode: t => Js.Json.t;
     let decode: Js.Json.t => Belt.Result.t(t, string);
   }; */

module Timestamp = Tezos_Timestamp;

module Address = Tezos_Address;

module BigMapId = Tezos_BigMapId;

module Code = Tezos_Code;

module ContractId = Tezos_ContractId;

module Expression = Tezos_Expression;

module Mutez = Tezos_Mutez;

module PrimitiveInstruction = Tezos_PrimitiveInstruction;

module PrimitiveData = Tezos_PrimitiveData;

module PrimitiveType = Tezos_PrimitiveType;

module Primitives = Tezos_Primitives;

module RPC = Tezos_RPC;

module Token = Tezos_Token;
