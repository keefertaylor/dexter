Jest.test("", _ =>
  Jest.Expect.expect(Tezos.Mutez.ofTezString("0.000001"))
  |> Jest.Expect.toEqual(Tezos.Mutez.ofInt64(1L))
);

Jest.test("", _ =>
  Jest.Expect.expect(Tezos.Mutez.ofTezString("0"))
  |> Jest.Expect.toEqual(Tezos.Mutez.ofInt64(0L))
);

Jest.test("", _ =>
  Jest.Expect.expect(Tezos.Mutez.ofTezString("0."))
  |> Jest.Expect.toEqual(Tezos.Mutez.ofInt64(0L))
);

Jest.test("", _ =>
  Jest.Expect.expect(Tezos.Mutez.ofTezString("0.0"))
  |> Jest.Expect.toEqual(Tezos.Mutez.ofInt64(0L))
);

Jest.test("", _ =>
  Jest.Expect.expect(Tezos.Mutez.ofTezString("1.1"))
  |> Jest.Expect.toEqual(Tezos.Mutez.ofInt64(1100000L))
);

Jest.test("", _ =>
  Jest.Expect.expect(Tezos.Mutez.ofTezString("1.12"))
  |> Jest.Expect.toEqual(Tezos.Mutez.ofInt64(1120000L))
);

Jest.test("", _ =>
  Jest.Expect.expect(Tezos.Mutez.ofTezString("1.123456"))
  |> Jest.Expect.toEqual(Tezos.Mutez.ofInt64(1123456L))
);

Jest.test("", _ =>
  Jest.Expect.expect(Tezos.Mutez.ofTezString("12"))
  |> Jest.Expect.toEqual(Tezos.Mutez.ofInt64(12000000L))
);

Jest.test("", _ =>
  Jest.Expect.expect(Tezos.Mutez.ofTezString("23"))
  |> Jest.Expect.toEqual(Tezos.Mutez.ofInt64(23000000L))
);

Jest.test("", _ =>
  Jest.Expect.expect(Tezos.Mutez.ofTezString("123.145"))
  |> Jest.Expect.toEqual(Tezos.Mutez.ofInt64(123145000L))
);

Jest.test("too many digits", _ =>
  Jest.Expect.expect(
    Belt.Result.isError(Tezos.Mutez.ofTezString("0.0000001")),
  )
  |> Jest.Expect.toEqual(true)
);

Jest.test("too many digits", _ =>
  Jest.Expect.expect(
    Belt.Result.isError(Tezos.Mutez.ofTezString("0.0000000")),
  )
  |> Jest.Expect.toEqual(true)
);