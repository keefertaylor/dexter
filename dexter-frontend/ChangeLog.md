### 2019-09-11 -- v0.1.1
* Update language to be more consistent: buy and sell.
* Add Checkers and Wrapped LTC tokens and exchanges.
* Move Dexter how to to the top.

### 2019-09-11 -- v0.1.0
* Release first alpha version of Dexter on the alphanet.
