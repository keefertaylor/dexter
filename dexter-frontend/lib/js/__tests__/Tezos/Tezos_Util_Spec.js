'use strict';

var Jest = require("@glennsl/bs-jest/lib/js/src/jest.js");
var Tezos_Util = require("../../src/Tezos/Tezos_Util.js");

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(6, Jest.Expect.expect(Tezos_Util.getPositionOfSmallestNonZero(0.000001)));
      }));

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(1, Jest.Expect.expect(Tezos_Util.getPositionOfSmallestNonZero(0.1)));
      }));

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(1, Jest.Expect.expect(Tezos_Util.getPositionOfSmallestNonZero(0.100000)));
      }));

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(0, Jest.Expect.expect(Tezos_Util.getPositionOfSmallestNonZero(1.0)));
      }));

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(2, Jest.Expect.expect(Tezos_Util.getPositionOfSmallestNonZero(1.030)));
      }));

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(3, Jest.Expect.expect(Tezos_Util.getPositionOfLastDecimalDigit("1.030")));
      }));

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(1, Jest.Expect.expect(Tezos_Util.getPositionOfLastDecimalDigit("1.0")));
      }));

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(0, Jest.Expect.expect(Tezos_Util.getPositionOfLastDecimalDigit("1.")));
      }));

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(4, Jest.Expect.expect(Tezos_Util.getPositionOfLastDecimalDigit("1.0301")));
      }));

/*  Not a pure module */
