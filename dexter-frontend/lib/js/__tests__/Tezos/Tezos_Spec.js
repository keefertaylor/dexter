'use strict';

var JsonTest = require("../JsonTest/JsonTest.js");
var Tezos_Code = require("../../src/Tezos/Tezos_Code.js");
var Tezos_Timestamp = require("../../src/Tezos/Tezos_Timestamp.js");
var Tezos_Expression = require("../../src/Tezos/Tezos_Expression.js");
var Tezos_PrimitiveInstruction = require("../../src/Tezos/Tezos_PrimitiveInstruction.js");

var fp = "__tests__/golden/";

JsonTest.jsonRoundtripSpecFile(Tezos_Timestamp.decode, Tezos_Timestamp.encode, "__tests__/golden/timestamp.json");

JsonTest.jsonRoundtripSpecFile(Tezos_PrimitiveInstruction.decode, Tezos_PrimitiveInstruction.encode, "__tests__/golden/primitive_instruction.json");

JsonTest.jsonRoundtripSpecFile(Tezos_Code.decode, Tezos_Code.encode, "__tests__/golden/code.json");

JsonTest.jsonRoundtripSpecFile(Tezos_Expression.decode, Tezos_Expression.encode, "__tests__/golden/expression.json");

exports.fp = fp;
/*  Not a pure module */
