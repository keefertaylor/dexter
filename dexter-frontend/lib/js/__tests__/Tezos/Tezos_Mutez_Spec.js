'use strict';

var Jest = require("@glennsl/bs-jest/lib/js/src/jest.js");
var Belt_Result = require("bs-platform/lib/js/belt_Result.js");
var Tezos_Mutez = require("../../src/Tezos/Tezos_Mutez.js");

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(Tezos_Mutez.ofInt64(/* int64 */[
                        /* hi */0,
                        /* lo */1
                      ]), Jest.Expect.expect(Tezos_Mutez.ofTezString("0.000001")));
      }));

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(Tezos_Mutez.ofInt64(/* int64 */[
                        /* hi */0,
                        /* lo */0
                      ]), Jest.Expect.expect(Tezos_Mutez.ofTezString("0")));
      }));

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(Tezos_Mutez.ofInt64(/* int64 */[
                        /* hi */0,
                        /* lo */0
                      ]), Jest.Expect.expect(Tezos_Mutez.ofTezString("0.")));
      }));

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(Tezos_Mutez.ofInt64(/* int64 */[
                        /* hi */0,
                        /* lo */0
                      ]), Jest.Expect.expect(Tezos_Mutez.ofTezString("0.0")));
      }));

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(Tezos_Mutez.ofInt64(/* int64 */[
                        /* hi */0,
                        /* lo */1100000
                      ]), Jest.Expect.expect(Tezos_Mutez.ofTezString("1.1")));
      }));

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(Tezos_Mutez.ofInt64(/* int64 */[
                        /* hi */0,
                        /* lo */1120000
                      ]), Jest.Expect.expect(Tezos_Mutez.ofTezString("1.12")));
      }));

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(Tezos_Mutez.ofInt64(/* int64 */[
                        /* hi */0,
                        /* lo */1123456
                      ]), Jest.Expect.expect(Tezos_Mutez.ofTezString("1.123456")));
      }));

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(Tezos_Mutez.ofInt64(/* int64 */[
                        /* hi */0,
                        /* lo */12000000
                      ]), Jest.Expect.expect(Tezos_Mutez.ofTezString("12")));
      }));

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(Tezos_Mutez.ofInt64(/* int64 */[
                        /* hi */0,
                        /* lo */23000000
                      ]), Jest.Expect.expect(Tezos_Mutez.ofTezString("23")));
      }));

Jest.test("", (function (param) {
        return Jest.Expect.toEqual(Tezos_Mutez.ofInt64(/* int64 */[
                        /* hi */0,
                        /* lo */123145000
                      ]), Jest.Expect.expect(Tezos_Mutez.ofTezString("123.145")));
      }));

Jest.test("too many digits", (function (param) {
        return Jest.Expect.toEqual(true, Jest.Expect.expect(Belt_Result.isError(Tezos_Mutez.ofTezString("0.0000001"))));
      }));

Jest.test("too many digits", (function (param) {
        return Jest.Expect.toEqual(true, Jest.Expect.expect(Belt_Result.isError(Tezos_Mutez.ofTezString("0.0000000"))));
      }));

/*  Not a pure module */
