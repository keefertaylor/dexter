'use strict';

var React = require("react");
var DexterUi = require("./Dexter/DexterUi.js");
var ReactDOMRe = require("reason-react/lib/js/src/ReactDOMRe.js");
var Tezos_BigMapId = require("./Tezos/Tezos_BigMapId.js");
var Tezos_ContractId = require("./Tezos/Tezos_ContractId.js");

var bakers = /* :: */[
  /* record */[
    /* name */"Tezos Capital Legacy",
    /* address */"tz1TDSmoZXwVevLTEvKCTHWpomG76oC9S2fJ",
    /* iconPath */"tezos-capital-legacy-logo.png"
  ],
  /* :: */[
    /* record */[
      /* name */"TezosBC",
      /* address */"tz1c3Wh8gNMMsYwZd67JndQpYxdaaPUV27E7",
      /* iconPath */"tezos-bc-logo.png"
    ],
    /* [] */0
  ]
];

var exchanges_000 = /* record */[
  /* name */"Tezos Gold",
  /* symbol */"TZG",
  /* tokenContractId */Tezos_ContractId.ofStringUnsafe("KT1U21mGrKarsajJq8Zqkyp1wxiVAmGfrdTn"),
  /* exchangeContractId */Tezos_ContractId.ofStringUnsafe("KT1Uyd4VbTiNX2qdto9SbaFAiF38Q4kcNP48"),
  /* bigMapId */Tezos_BigMapId.ofInt(765)
];

var exchanges_001 = /* :: */[
  /* record */[
    /* name */"Tezos Tacos",
    /* symbol */"TZT",
    /* tokenContractId */Tezos_ContractId.ofStringUnsafe("KT1LNCXbwgtVc3z8ugEXKecPBNaDFe6Wewxs"),
    /* exchangeContractId */Tezos_ContractId.ofStringUnsafe("KT1RBBiJKpdgbbMiXMBSkDuJ4V99caRRpq4r"),
    /* bigMapId */Tezos_BigMapId.ofInt(777)
  ],
  /* :: */[
    /* record */[
      /* name */"ABC",
      /* symbol */"ABC",
      /* tokenContractId */Tezos_ContractId.ofStringUnsafe("KT1R9N3LhTRAkkjuHMhwyzNAdzrXE7PjHiSs"),
      /* exchangeContractId */Tezos_ContractId.ofStringUnsafe("KT1GdgBRxuAX3Xe8Q763aZoFhbQ5iU2TrfmL"),
      /* bigMapId */Tezos_BigMapId.ofInt(778)
    ],
    /* [] */0
  ]
];

var exchanges = /* :: */[
  exchanges_000,
  exchanges_001
];

ReactDOMRe.renderToElementWithId(React.createElement(DexterUi.make, {
          bakers: bakers,
          exchanges: exchanges
        }), "root");

exports.bakers = bakers;
exports.exchanges = exchanges;
/* exchanges Not a pure module */
