'use strict';

var Curry = require("bs-platform/lib/js/curry.js");
var Tezos_Mutez = require("../Tezos/Tezos_Mutez.js");
var Tezos_ContractId = require("../Tezos/Tezos_ContractId.js");
var Tezos_Expression = require("../Tezos/Tezos_Expression.js");

var getSource = (
    function () {
        return tezbridge.request({method: 'get_source'});
    }
    );

var postTransactionRaw = (
   function (contract, tez, parameters) {
       return tezbridge.request({
         method: 'inject_operations',
         operations: [
           {
             kind: 'transaction',
             amount: tez,
             destination: contract,
             parameters: parameters
           }
         ]
       });
   }
   );

function postTransaction(contract, mutez, expression) {
  return Curry._3(postTransactionRaw, Tezos_ContractId.encode(contract), Tezos_Mutez.encode(mutez), Tezos_Expression.encode(expression));
}

exports.getSource = getSource;
exports.postTransactionRaw = postTransactionRaw;
exports.postTransaction = postTransaction;
/* getSource Not a pure module */
