'use strict';

var Block = require("bs-platform/lib/js/block.js");
var Tezos_Expression = require("../Tezos/Tezos_Expression.js");

function ofExpression(expression) {
  if (expression.tag === /* ContractExpression */4) {
    var match = expression[0];
    switch (match.tag | 0) {
      case /* PrimitiveData */1 :
          if (match[0] >= 8) {
            var match$1 = expression[1];
            if (match$1 !== undefined) {
              var match$2 = match$1;
              if (match$2) {
                var match$3 = match$2[0];
                if (match$3.tag) {
                  return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                } else {
                  var match$4 = match$2[1];
                  if (match$4 && !match$4[1]) {
                    return /* Ok */Block.__(0, [/* record */[/* tokens */match$3[0]]]);
                  } else {
                    return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                  }
                }
              } else {
                return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
              }
            } else {
              return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
            }
          } else {
            return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
          }
      case /* PrimitiveInstruction */0 :
      case /* PrimitiveType */2 :
          return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
      
    }
  } else {
    return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
  }
}

exports.ofExpression = ofExpression;
/* No side effect */
