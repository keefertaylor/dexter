'use strict';

var Block = require("bs-platform/lib/js/block.js");
var Tezos_RPC = require("../Tezos/Tezos_RPC.js");
var Token_Store = require("./Token_Store.js");
var Token_BigMap = require("./Token_BigMap.js");

var url = "http://127.0.0.1:9323";

function $$fetch(tokenContractId, account) {
  var storePromise = Tezos_RPC.getStorage(url, "main", "head", tokenContractId).then((function (result) {
          if (result.tag) {
            return Promise.resolve(/* Error */Block.__(1, ["storePromise failed"]));
          } else {
            return Promise.resolve(Token_Store.ofExpression(result[0]));
          }
        }));
  var query_000 = /* key : StringExpression */Block.__(1, [account]);
  var query_001 = /* type_ : ContractExpression */Block.__(4, [
      /* PrimitiveType */Block.__(2, [/* Address */5]),
      undefined,
      undefined
    ]);
  var query = /* record */[
    query_000,
    query_001
  ];
  var bigMapPromise = Tezos_RPC.getBigMapValue(url, "main", "head", tokenContractId, query).then((function (result) {
          if (result.tag) {
            return Promise.resolve(/* Error */Block.__(1, ["bigMapPromise failed"]));
          } else {
            return Promise.resolve(Token_BigMap.ofExpression(result[0]));
          }
        }));
  return Promise.all(/* tuple */[
                storePromise,
                bigMapPromise
              ]).then((function (results) {
                var match = results[0];
                if (match.tag) {
                  return Promise.resolve(/* Error */Block.__(1, ["something went wrong"]));
                } else {
                  var match$1 = results[1];
                  if (match$1.tag) {
                    return Promise.resolve(/* Error */Block.__(1, ["something went wrong"]));
                  } else {
                    return Promise.resolve(/* Ok */Block.__(0, [/* tuple */[
                                    match[0],
                                    match$1[0]
                                  ]]));
                  }
                }
              }));
}

exports.url = url;
exports.$$fetch = $$fetch;
/* No side effect */
