'use strict';

var Block = require("bs-platform/lib/js/block.js");
var Tezos_Expression = require("../Tezos/Tezos_Expression.js");

function ofExpression(expression) {
  if (expression.tag === /* ContractExpression */4) {
    var match = expression[0];
    switch (match.tag | 0) {
      case /* PrimitiveData */1 :
          if (match[0] >= 8) {
            var match$1 = expression[1];
            if (match$1 !== undefined) {
              var match$2 = match$1;
              if (match$2) {
                var match$3 = match$2[1];
                if (match$3) {
                  var match$4 = match$3[0];
                  if (match$4.tag === /* ContractExpression */4) {
                    var match$5 = match$4[0];
                    switch (match$5.tag | 0) {
                      case /* PrimitiveData */1 :
                          if (match$5[0] >= 8) {
                            var match$6 = match$4[1];
                            if (match$6 !== undefined) {
                              var match$7 = match$6;
                              if (match$7) {
                                var match$8 = match$7[0];
                                if (match$8.tag) {
                                  return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                                } else {
                                  var match$9 = match$7[1];
                                  if (match$9) {
                                    var match$10 = match$9[0];
                                    if (match$10.tag === /* ContractExpression */4) {
                                      var match$11 = match$10[0];
                                      switch (match$11.tag | 0) {
                                        case /* PrimitiveData */1 :
                                            if (match$11[0] >= 8) {
                                              var match$12 = match$10[1];
                                              if (match$12 !== undefined) {
                                                var match$13 = match$12;
                                                if (match$13) {
                                                  var match$14 = match$13[0];
                                                  if (match$14.tag === /* StringExpression */1) {
                                                    var match$15 = match$13[1];
                                                    if (match$15) {
                                                      var match$16 = match$15[0];
                                                      if (match$16.tag === /* StringExpression */1 && !(match$15[1] || match$9[1] || match$3[1])) {
                                                        return /* Ok */Block.__(0, [/* record */[
                                                                    /* total */match$8[0],
                                                                    /* name */match$14[0],
                                                                    /* symbol */match$16[0]
                                                                  ]]);
                                                      } else {
                                                        return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                                                      }
                                                    } else {
                                                      return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                                                    }
                                                  } else {
                                                    return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                                                  }
                                                } else {
                                                  return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                                                }
                                              } else {
                                                return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                                              }
                                            } else {
                                              return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                                            }
                                        case /* PrimitiveInstruction */0 :
                                        case /* PrimitiveType */2 :
                                            return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                                        
                                      }
                                    } else {
                                      return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                                    }
                                  } else {
                                    return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                                  }
                                }
                              } else {
                                return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                              }
                            } else {
                              return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                            }
                          } else {
                            return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                          }
                      case /* PrimitiveInstruction */0 :
                      case /* PrimitiveType */2 :
                          return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                      
                    }
                  } else {
                    return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                  }
                } else {
                  return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
                }
              } else {
                return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
              }
            } else {
              return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
            }
          } else {
            return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
          }
      case /* PrimitiveInstruction */0 :
      case /* PrimitiveType */2 :
          return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
      
    }
  } else {
    return /* Error */Block.__(1, [JSON.stringify(Tezos_Expression.encode(expression))]);
  }
}

exports.ofExpression = ofExpression;
/* No side effect */
