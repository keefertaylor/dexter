'use strict';

var Curry = require("bs-platform/lib/js/curry.js");
var React = require("react");
var Caml_option = require("bs-platform/lib/js/caml_option.js");
var Tezos_Mutez = require("../Tezos/Tezos_Mutez.js");
var Tezos_Token = require("../Tezos/Tezos_Token.js");

function eventToValue($$event) {
  return $$event.target.value;
}

function ofString(x) {
  var match = Tezos_Token.ofString(x);
  if (match.tag) {
    return ;
  } else {
    return match[0];
  }
}

var NumericInputToken = {
  ofString: ofString,
  toString: Tezos_Token.toString
};

function ofString$1(x) {
  var match = Tezos_Mutez.ofTezString(x);
  if (match.tag) {
    return ;
  } else {
    return match[0];
  }
}

var NumericInputMutez = {
  ofString: ofString$1,
  toString: Tezos_Mutez.toString
};

function MakeNumericInput(NumericInputConfig) {
  var CommonUi_NumericInput$MakeNumericInput = function (Props) {
    var onChange = Props.onChange;
    var match = Props.onEmpty;
    var onEmpty = match !== undefined ? match : (function (prim) {
          return /* () */0;
        });
    var match$1 = Props.onError;
    var onError = match$1 !== undefined ? match$1 : (function (prim) {
          return /* () */0;
        });
    var numValue = Props.numValue;
    var initialValue = Props.initialValue;
    var match$2 = Props.style;
    var style = match$2 !== undefined ? Caml_option.valFromOption(match$2) : { };
    var match$3 = Props.disabled;
    var disabled = match$3 !== undefined ? match$3 : false;
    var match$4 = Props.className;
    var className = match$4 !== undefined ? match$4 : "";
    var match$5 = initialValue !== undefined ? /* tuple */[
        initialValue,
        true
      ] : (
        numValue !== undefined ? /* tuple */[
            Curry._1(NumericInputConfig.toString, Caml_option.valFromOption(numValue)),
            true
          ] : /* tuple */[
            "",
            false
          ]
      );
    var match$6 = React.useReducer((function (_state, value) {
            if (value === "") {
              Curry._1(onEmpty, /* () */0);
              return /* record */[
                      /* value */value,
                      /* valid */true
                    ];
            } else {
              var val;
              try {
                val = Curry._1(NumericInputConfig.ofString, value);
              }
              catch (exn){
                Curry._1(onError, /* () */0);
                return /* record */[
                        /* value */value,
                        /* valid */false
                      ];
              }
              if (val !== undefined) {
                Curry._1(onChange, Caml_option.valFromOption(val));
                return /* record */[
                        /* value */value,
                        /* valid */true
                      ];
              } else {
                Curry._1(onError, /* () */0);
                return /* record */[
                        /* value */value,
                        /* valid */false
                      ];
              }
            }
          }), /* record */[
          /* value */match$5[0],
          /* valid */match$5[1]
        ]);
    var dispatch = match$6[1];
    var state = match$6[0];
    var match$7 = state[/* valid */1];
    return React.createElement("span", {
                className: match$7 ? "" : "has-error"
              }, React.createElement("input", {
                    className: className,
                    style: style,
                    disabled: disabled,
                    type: "text",
                    value: state[/* value */0],
                    onChange: (function (ev) {
                        return Curry._1(dispatch, ev.target.value);
                      })
                  }));
  };
  return {
          make: CommonUi_NumericInput$MakeNumericInput
        };
}

function CommonUi_NumericInput$MakeNumericInput(Props) {
  var onChange = Props.onChange;
  var match = Props.onEmpty;
  var onEmpty = match !== undefined ? match : (function (prim) {
        return /* () */0;
      });
  var match$1 = Props.onError;
  var onError = match$1 !== undefined ? match$1 : (function (prim) {
        return /* () */0;
      });
  var numValue = Props.numValue;
  var initialValue = Props.initialValue;
  var match$2 = Props.style;
  var style = match$2 !== undefined ? Caml_option.valFromOption(match$2) : { };
  var match$3 = Props.disabled;
  var disabled = match$3 !== undefined ? match$3 : false;
  var match$4 = Props.className;
  var className = match$4 !== undefined ? match$4 : "";
  var match$5 = initialValue !== undefined ? /* tuple */[
      initialValue,
      true
    ] : (
      numValue !== undefined ? /* tuple */[
          Tezos_Token.toString(Caml_option.valFromOption(numValue)),
          true
        ] : /* tuple */[
          "",
          false
        ]
    );
  var match$6 = React.useReducer((function (_state, value) {
          if (value === "") {
            Curry._1(onEmpty, /* () */0);
            return /* record */[
                    /* value */value,
                    /* valid */true
                  ];
          } else {
            var val;
            try {
              val = ofString(value);
            }
            catch (exn){
              Curry._1(onError, /* () */0);
              return /* record */[
                      /* value */value,
                      /* valid */false
                    ];
            }
            if (val !== undefined) {
              Curry._1(onChange, Caml_option.valFromOption(val));
              return /* record */[
                      /* value */value,
                      /* valid */true
                    ];
            } else {
              Curry._1(onError, /* () */0);
              return /* record */[
                      /* value */value,
                      /* valid */false
                    ];
            }
          }
        }), /* record */[
        /* value */match$5[0],
        /* valid */match$5[1]
      ]);
  var dispatch = match$6[1];
  var state = match$6[0];
  var match$7 = state[/* valid */1];
  return React.createElement("span", {
              className: match$7 ? "" : "has-error"
            }, React.createElement("input", {
                  className: className,
                  style: style,
                  disabled: disabled,
                  type: "text",
                  value: state[/* value */0],
                  onChange: (function (ev) {
                      return Curry._1(dispatch, ev.target.value);
                    })
                }));
}

var TokenInput = {
  make: CommonUi_NumericInput$MakeNumericInput
};

function CommonUi_NumericInput$MakeNumericInput$1(Props) {
  var onChange = Props.onChange;
  var match = Props.onEmpty;
  var onEmpty = match !== undefined ? match : (function (prim) {
        return /* () */0;
      });
  var match$1 = Props.onError;
  var onError = match$1 !== undefined ? match$1 : (function (prim) {
        return /* () */0;
      });
  var numValue = Props.numValue;
  var initialValue = Props.initialValue;
  var match$2 = Props.style;
  var style = match$2 !== undefined ? Caml_option.valFromOption(match$2) : { };
  var match$3 = Props.disabled;
  var disabled = match$3 !== undefined ? match$3 : false;
  var match$4 = Props.className;
  var className = match$4 !== undefined ? match$4 : "";
  var match$5 = initialValue !== undefined ? /* tuple */[
      initialValue,
      true
    ] : (
      numValue !== undefined ? /* tuple */[
          Tezos_Mutez.toString(Caml_option.valFromOption(numValue)),
          true
        ] : /* tuple */[
          "",
          false
        ]
    );
  var match$6 = React.useReducer((function (_state, value) {
          if (value === "") {
            Curry._1(onEmpty, /* () */0);
            return /* record */[
                    /* value */value,
                    /* valid */true
                  ];
          } else {
            var val;
            try {
              val = ofString$1(value);
            }
            catch (exn){
              Curry._1(onError, /* () */0);
              return /* record */[
                      /* value */value,
                      /* valid */false
                    ];
            }
            if (val !== undefined) {
              Curry._1(onChange, Caml_option.valFromOption(val));
              return /* record */[
                      /* value */value,
                      /* valid */true
                    ];
            } else {
              Curry._1(onError, /* () */0);
              return /* record */[
                      /* value */value,
                      /* valid */false
                    ];
            }
          }
        }), /* record */[
        /* value */match$5[0],
        /* valid */match$5[1]
      ]);
  var dispatch = match$6[1];
  var state = match$6[0];
  var match$7 = state[/* valid */1];
  return React.createElement("span", {
              className: match$7 ? "" : "has-error"
            }, React.createElement("input", {
                  className: className,
                  style: style,
                  disabled: disabled,
                  type: "text",
                  value: state[/* value */0],
                  onChange: (function (ev) {
                      return Curry._1(dispatch, ev.target.value);
                    })
                }));
}

var MutezInput = {
  make: CommonUi_NumericInput$MakeNumericInput$1
};

exports.eventToValue = eventToValue;
exports.NumericInputToken = NumericInputToken;
exports.NumericInputMutez = NumericInputMutez;
exports.MakeNumericInput = MakeNumericInput;
exports.TokenInput = TokenInput;
exports.MutezInput = MutezInput;
/* react Not a pure module */
