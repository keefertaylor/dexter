'use strict';

var $$Array = require("bs-platform/lib/js/array.js");
var Curry = require("bs-platform/lib/js/curry.js");
var React = require("react");
var Caml_obj = require("bs-platform/lib/js/caml_obj.js");
var Caml_array = require("bs-platform/lib/js/caml_array.js");
var Belt_Option = require("bs-platform/lib/js/belt_Option.js");
var Caml_format = require("bs-platform/lib/js/caml_format.js");

function indexOf(arr, el) {
  var indexOf$1 = function (_n) {
    while(true) {
      var n = _n;
      if (Caml_obj.caml_equal(Caml_array.caml_array_get(arr, n), el)) {
        return n;
      } else {
        _n = n + 1 | 0;
        continue ;
      }
    };
  };
  var n;
  try {
    n = indexOf$1(0);
  }
  catch (exn){
    return ;
  }
  return n;
}

function eventToValue($$event) {
  return $$event.target.value;
}

function MakeDropDown(DropDownConfig) {
  var CommonUi_DropDown$MakeDropDown = function (Props) {
    var onChange = Props.onChange;
    var selectedItem = Props.selectedItem;
    var elements = Props.elements;
    var match = Props.disabled;
    var disabled = match !== undefined ? match : false;
    var match$1 = Props.className;
    var className = match$1 !== undefined ? match$1 : "form-control";
    var handleChange = function (evt) {
      var pos = Caml_format.caml_int_of_string(evt.target.value);
      var value;
      try {
        value = Caml_array.caml_array_get(elements, pos);
      }
      catch (exn){
        return /* () */0;
      }
      return Curry._1(onChange, value);
    };
    return React.createElement("select", {
                className: className,
                disabled: disabled,
                value: Belt_Option.mapWithDefault(indexOf(elements, selectedItem), "-1", (function (prim) {
                        return String(prim);
                      })),
                onChange: handleChange
              }, $$Array.mapi((function (ix, v) {
                      var s = Curry._1(DropDownConfig.toString, v);
                      return React.createElement("option", {
                                  key: s,
                                  value: String(ix)
                                }, s);
                    }), elements));
  };
  return {
          make: CommonUi_DropDown$MakeDropDown
        };
}

exports.indexOf = indexOf;
exports.eventToValue = eventToValue;
exports.MakeDropDown = MakeDropDown;
/* react Not a pure module */
