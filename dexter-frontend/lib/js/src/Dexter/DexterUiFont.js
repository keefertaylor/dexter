'use strict';

var Css = require("bs-css/lib/js/src/Css.js");

var defaultFont = Css.fontFamily("Source Sans Pro, Regular");

var boldFont = Css.fontFamily("Source Sans Pro, Semibold");

exports.defaultFont = defaultFont;
exports.boldFont = boldFont;
/* defaultFont Not a pure module */
