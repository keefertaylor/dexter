'use strict';

var Css = require("bs-css/lib/js/src/Css.js");
var React = require("react");
var DexterUiColor = require("./DexterUiColor.js");

var container = Css.style(/* :: */[
      Css.flexDirection(/* row */5693978),
      /* [] */0
    ]);

var circle = Css.style(/* :: */[
      Css.width(/* `px */[
            25096,
            48
          ]),
      /* :: */[
        Css.height(/* `px */[
              25096,
              48
            ]),
        /* :: */[
          Css.backgroundColor(DexterUiColor.blue),
          /* :: */[
            Css.backgroundRepeat(/* noRepeat */-695430532),
            /* :: */[
              Css.backgroundOrigin(/* paddingBox */972575930),
              /* :: */[
                Css.borderRadius(/* `percent */[
                      -119887163,
                      50.0
                    ]),
                /* [] */0
              ]
            ]
          ]
        ]
      ]
    ]);

var Style = {
  container: container,
  circle: circle
};

function DexterUiPoolTokens(Props) {
  var account = Props.account;
  var poolTokens = Props.poolTokens;
  console.log(poolTokens);
  if (account !== undefined) {
    return React.createElement("div", {
                className: container
              }, React.createElement("div", {
                    className: circle
                  }, "1"), React.createElement("table", undefined, React.createElement("tr", undefined, React.createElement("td", {
                            colSpan: 2
                          }, React.createElement("span", undefined, "1,500 ABC"), React.createElement("span", undefined, "(0.01%)")), React.createElement("td", undefined, "15 XTZ"), React.createElement("td", undefined, "30 ABC")), React.createElement("tr", undefined, React.createElement("td", {
                            colSpan: 2
                          }, "Pool tokens"), React.createElement("td", undefined, "$15"), React.createElement("td", undefined, "$15")), React.createElement("tr", undefined, React.createElement("td", {
                            colSpan: 4
                          }, "Rewards")), React.createElement("tr", undefined, React.createElement("td", undefined, "8.8727 XTZ"), React.createElement("td", undefined, "$7.74"), React.createElement("td", undefined, "26.6181 XTZ"), React.createElement("td", undefined, "$22.32")), React.createElement("tr", undefined, React.createElement("td", undefined, "Monthly earning"), React.createElement("td", undefined), React.createElement("td", undefined, "Total earned"), React.createElement("td", undefined))));
  } else {
    return React.createElement("div", undefined, React.createElement("div", undefined, "There are no transactions yet!"));
  }
}

var make = DexterUiPoolTokens;

exports.Style = Style;
exports.make = make;
/* container Not a pure module */
