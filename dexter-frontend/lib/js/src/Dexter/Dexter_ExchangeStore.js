'use strict';


function ofExpression(expression) {
  if (expression.tag === /* ContractExpression */4) {
    var match = expression[0];
    switch (match.tag | 0) {
      case /* PrimitiveData */1 :
          if (match[0] >= 8) {
            var match$1 = expression[1];
            if (match$1 !== undefined) {
              var match$2 = match$1;
              if (match$2) {
                var match$3 = match$2[1];
                if (match$3) {
                  var match$4 = match$3[0];
                  if (match$4.tag === /* ContractExpression */4) {
                    var match$5 = match$4[0];
                    switch (match$5.tag | 0) {
                      case /* PrimitiveData */1 :
                          if (match$5[0] >= 8) {
                            var match$6 = match$4[1];
                            if (match$6 !== undefined) {
                              var match$7 = match$6;
                              if (match$7) {
                                var match$8 = match$7[0];
                                if (match$8.tag) {
                                  return ;
                                } else {
                                  var match$9 = match$7[1];
                                  if (match$9 && !(match$9[1] || match$3[1])) {
                                    return match$8[0];
                                  } else {
                                    return ;
                                  }
                                }
                              } else {
                                return ;
                              }
                            } else {
                              return ;
                            }
                          } else {
                            return ;
                          }
                      case /* PrimitiveInstruction */0 :
                      case /* PrimitiveType */2 :
                          return ;
                      
                    }
                  } else {
                    return ;
                  }
                } else {
                  return ;
                }
              } else {
                return ;
              }
            } else {
              return ;
            }
          } else {
            return ;
          }
      case /* PrimitiveInstruction */0 :
      case /* PrimitiveType */2 :
          return ;
      
    }
  }
  
}

exports.ofExpression = ofExpression;
/* No side effect */
