'use strict';

var Css = require("bs-css/lib/js/src/Css.js");
var List = require("bs-platform/lib/js/list.js");
var $$Array = require("bs-platform/lib/js/array.js");
var Curry = require("bs-platform/lib/js/curry.js");
var React = require("react");
var Caml_obj = require("bs-platform/lib/js/caml_obj.js");
var DexterUiFont = require("./DexterUiFont.js");
var DexterUiColor = require("./DexterUiColor.js");
var DexterUiBakerView = require("./DexterUiBakerView.js");

var row = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* [] */0
    ]);

var columnSide = Css.style(/* :: */[
      Css.flex(/* `num */[
            5496390,
            40.0
          ]),
      /* :: */[
        Css.paddingLeft(/* `px */[
              25096,
              48
            ]),
        /* :: */[
          Css.paddingRight(/* `px */[
                25096,
                86
              ]),
          /* [] */0
        ]
      ]
    ]);

var columnEmpty = Css.style(/* :: */[
      Css.flex(/* `num */[
            5496390,
            60.0
          ]),
      /* [] */0
    ]);

var title = Css.style(/* :: */[
      Css.marginBottom(/* `px */[
            25096,
            9
          ]),
      /* :: */[
        Css.color(DexterUiColor.grey),
        /* :: */[
          Css.fontSize(/* `px */[
                25096,
                18
              ]),
          /* :: */[
            DexterUiFont.boldFont,
            /* [] */0
          ]
        ]
      ]
    ]);

var baker = Css.style(/* :: */[
      Css.marginBottom(/* `px */[
            25096,
            24
          ]),
      /* :: */[
        Css.cursor(/* pointer */-786317123),
        /* [] */0
      ]
    ]);

var Style = {
  row: row,
  columnSide: columnSide,
  columnEmpty: columnEmpty,
  title: title,
  baker: baker
};

var nbsp = ('\u00a0');

function setActiveBaker(baker, activeBaker) {
  if (activeBaker !== undefined && Caml_obj.caml_equal(baker, activeBaker)) {
    return ;
  } else {
    return baker;
  }
}

function compareBaker(x, y) {
  if (y !== undefined) {
    return Caml_obj.caml_equal(x, y);
  } else {
    return false;
  }
}

function DexterUiBakerSelector(Props) {
  var activeBaker = Props.activeBaker;
  var bakers = Props.bakers;
  var onBakerChange = Props.onBakerChange;
  var match = React.useReducer((function (state, action) {
          return /* record */[/* activeBaker */setActiveBaker(action[0], state[/* activeBaker */0])];
        }), /* record */[/* activeBaker */activeBaker]);
  var dispatch = match[1];
  var state = match[0];
  return React.createElement("div", {
              className: row
            }, React.createElement("div", {
                  className: columnSide
                }, React.createElement("div", {
                      className: title
                    }, "Choose your baker"), $$Array.of_list(List.map((function (baker$1) {
                            return React.createElement("div", {
                                        className: baker,
                                        onClick: (function (_event) {
                                            Curry._1(dispatch, /* UpdateActiveBaker */[baker$1]);
                                            return Curry._1(onBakerChange, setActiveBaker(baker$1, state[/* activeBaker */0]));
                                          })
                                      }, React.createElement(DexterUiBakerView.make, {
                                            baker: baker$1,
                                            active: compareBaker(baker$1, state[/* activeBaker */0])
                                          }));
                          }), bakers))), React.createElement("div", {
                  className: columnEmpty
                }, nbsp));
}

var make = DexterUiBakerSelector;

exports.Style = Style;
exports.nbsp = nbsp;
exports.setActiveBaker = setActiveBaker;
exports.compareBaker = compareBaker;
exports.make = make;
/* row Not a pure module */
