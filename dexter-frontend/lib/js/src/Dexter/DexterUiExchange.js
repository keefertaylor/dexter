'use strict';

var Css = require("bs-css/lib/js/src/Css.js");
var Block = require("bs-platform/lib/js/block.js");
var Curry = require("bs-platform/lib/js/curry.js");
var React = require("react");
var DexterUiFont = require("./DexterUiFont.js");
var DexterUiColor = require("./DexterUiColor.js");
var DexterUiTokenSearch = require("./DexterUiTokenSearch.js");
var DexterUiExchangeRate = require("./DexterUiExchangeRate.js");

var row = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* [] */0
    ]);

var columnSide = Css.style(/* :: */[
      Css.flex(/* `num */[
            5496390,
            40.0
          ]),
      /* [] */0
    ]);

var columnSidePadding = Css.style(/* :: */[
      Css.paddingLeft(/* `px */[
            25096,
            47
          ]),
      /* [] */0
    ]);

var columnCenter = Css.style(/* :: */[
      Css.flex(/* `num */[
            5496390,
            20.0
          ]),
      /* [] */0
    ]);

var action = Css.style(/* :: */[
      Css.color(DexterUiColor.grey),
      /* :: */[
        Css.fontSize(Css.px(30)),
        /* :: */[
          DexterUiFont.boldFont,
          /* :: */[
            Css.letterSpacing(/* `px */[
                  25096,
                  1
                ]),
            /* [] */0
          ]
        ]
      ]
    ]);

var space = Css.style(/* :: */[
      Css.marginTop(/* `px */[
            25096,
            10
          ]),
      /* [] */0
    ]);

var balanceGroup = Css.style(/* :: */[
      Css.marginTop(/* `px */[
            25096,
            24
          ]),
      /* [] */0
    ]);

var balanceToken = Css.style(/* :: */[
      Css.display(/* table */182695950),
      /* :: */[
        Css.cursor(/* pointer */-786317123),
        /* [] */0
      ]
    ]);

var token = Css.style(/* :: */[
      Css.width(/* `px */[
            25096,
            48
          ]),
      /* :: */[
        Css.height(/* `px */[
              25096,
              48
            ]),
        /* [] */0
      ]
    ]);

var tokenText = Css.style(/* :: */[
      Css.color(DexterUiColor.black),
      /* :: */[
        Css.fontSize(/* `px */[
              25096,
              42
            ]),
        /* :: */[
          DexterUiFont.boldFont,
          /* :: */[
            Css.display(/* tableCell */793912528),
            /* :: */[
              Css.paddingLeft(/* `px */[
                    25096,
                    16
                  ]),
              /* :: */[
                Css.verticalAlign(/* middle */-866200747),
                /* [] */0
              ]
            ]
          ]
        ]
      ]
    ]);

var balance = Css.style(/* :: */[
      Css.color(DexterUiColor.grey),
      /* :: */[
        Css.fontSize(Css.px(16)),
        /* :: */[
          DexterUiFont.defaultFont,
          /* [] */0
        ]
      ]
    ]);

var amount = Css.style(/* :: */[
      Css.fontSize(Css.px(18)),
      /* :: */[
        Css.color(DexterUiColor.grey),
        /* [] */0
      ]
    ]);

var amountBox = Css.style(/* :: */[
      Css.unsafe("borderRadius", "10px"),
      /* :: */[
        Css.borderColor(DexterUiColor.lightGrey),
        /* :: */[
          Css.borderStyle(Css.solid),
          /* :: */[
            Css.borderWidth(Css.px(1)),
            /* :: */[
              Css.padding2(Css.px(10), Css.px(20)),
              /* :: */[
                Css.marginTop(Css.px(5)),
                /* :: */[
                  Css.marginBottom(Css.px(50)),
                  /* :: */[
                    Css.marginRight(Css.px(100)),
                    /* [] */0
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var verticalBar = Css.style(/* :: */[
      Css.width(/* `px */[
            25096,
            0
          ]),
      /* :: */[
        Css.height(/* `px */[
              25096,
              96
            ]),
        /* :: */[
          Css.borderWidth(/* `px */[
                25096,
                1
              ]),
          /* :: */[
            Css.borderStyle(/* solid */12956715),
            /* :: */[
              Css.borderColor(DexterUiColor.lightGrey),
              /* :: */[
                Css.marginTop(/* `px */[
                      25096,
                      38
                    ]),
                /* :: */[
                  Css.marginBottom(/* `px */[
                        25096,
                        38
                      ]),
                  /* [] */0
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var fullWidth = Css.style(/* :: */[
      Css.width(/* `percent */[
            -119887163,
            100.0
          ]),
      /* [] */0
    ]);

var centerSymbolContainer = Css.style(/* :: */[
      Css.display(/* table */182695950),
      /* :: */[
        Css.margin(/* auto */-1065951377),
        /* [] */0
      ]
    ]);

var circle = Css.style(/* :: */[
      Css.backgroundColor(Css.blue),
      /* :: */[
        Css.backgroundRepeat(/* noRepeat */-695430532),
        /* :: */[
          Css.backgroundOrigin(/* paddingBox */972575930),
          /* :: */[
            Css.borderRadius(/* `percent */[
                  -119887163,
                  50.0
                ]),
            /* :: */[
              Css.width(/* `px */[
                    25096,
                    48
                  ]),
              /* :: */[
                Css.height(/* `px */[
                      25096,
                      48
                    ]),
                /* :: */[
                  Css.boxShadow(Css.Shadow.box(Css.px(0), Css.px(0), Css.px(12), undefined, undefined, DexterUiColor.shadowColor)),
                  /* :: */[
                    Css.position(/* relative */903134412),
                    /* :: */[
                      Css.marginLeft(/* `percent */[
                            -119887163,
                            -50.0
                          ]),
                      /* :: */[
                        Css.cursor(/* pointer */-786317123),
                        /* [] */0
                      ]
                    ]
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var circleImg = Css.style(/* :: */[
      Css.width(/* `px */[
            25096,
            21
          ]),
      /* :: */[
        Css.height(/* `px */[
              25096,
              25
            ]),
        /* :: */[
          Css.position(/* absolute */-1013592457),
          /* :: */[
            Css.margin(/* auto */-1065951377),
            /* :: */[
              Css.top(/* `px */[
                    25096,
                    0
                  ]),
              /* :: */[
                Css.left(/* `px */[
                      25096,
                      0
                    ]),
                /* :: */[
                  Css.right(/* `px */[
                        25096,
                        0
                      ]),
                  /* :: */[
                    Css.bottom(/* `px */[
                          25096,
                          0
                        ]),
                    /* [] */0
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var tokenAmount = Css.style(/* :: */[
      Css.fontSize(Css.px(42)),
      /* :: */[
        Css.color(DexterUiColor.grey),
        /* :: */[
          DexterUiFont.defaultFont,
          /* :: */[
            Css.width(/* `percent */[
                  -119887163,
                  50.0
                ]),
            /* :: */[
              Css.selector("input:focus", /* :: */[
                    Css.unsafe("border", "none"),
                    /* [] */0
                  ]),
              /* :: */[
                Css.unsafe("border", "none"),
                /* [] */0
              ]
            ]
          ]
        ]
      ]
    ]);

var usdAmount = Css.style(/* :: */[
      Css.fontSize(Css.px(18)),
      /* :: */[
        Css.color(DexterUiColor.grey),
        /* :: */[
          DexterUiFont.boldFont,
          /* [] */0
        ]
      ]
    ]);

var verticalAlign = Css.style(/* :: */[
      Css.display(Css.tableCell),
      /* :: */[
        Css.verticalAlign(Css.middle),
        /* [] */0
      ]
    ]);

var downArrow = Css.style(/* :: */[
      Css.width(/* `px */[
            25096,
            14
          ]),
      /* :: */[
        Css.height(/* `px */[
              25096,
              14
            ]),
        /* :: */[
          Css.borderColor(Css.hex("758DA6")),
          /* :: */[
            Css.borderStyle(Css.solid),
            /* :: */[
              Css.borderTopWidth(Css.px(0)),
              /* :: */[
                Css.borderRightWidth(Css.px(3)),
                /* :: */[
                  Css.borderBottomWidth(/* `px */[
                        25096,
                        3
                      ]),
                  /* :: */[
                    Css.borderLeftWidth(/* `px */[
                          25096,
                          0
                        ]),
                    /* :: */[
                      Css.display(Css.inlineBlock),
                      /* :: */[
                        Css.marginLeft(/* `px */[
                              25096,
                              31
                            ]),
                        /* :: */[
                          Css.transform(Css.rotate(/* `deg */[
                                    4995526,
                                    45.0
                                  ])),
                          /* [] */0
                        ]
                      ]
                    ]
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var upArrow = Css.style(/* :: */[
      Css.width(/* `px */[
            25096,
            14
          ]),
      /* :: */[
        Css.height(/* `px */[
              25096,
              14
            ]),
        /* :: */[
          Css.borderColor(Css.hex("758DA6")),
          /* :: */[
            Css.borderStyle(Css.solid),
            /* :: */[
              Css.borderTopWidth(Css.px(0)),
              /* :: */[
                Css.borderRightWidth(Css.px(3)),
                /* :: */[
                  Css.borderBottomWidth(/* `px */[
                        25096,
                        3
                      ]),
                  /* :: */[
                    Css.borderLeftWidth(/* `px */[
                          25096,
                          0
                        ]),
                    /* :: */[
                      Css.display(Css.inlineBlock),
                      /* :: */[
                        Css.marginLeft(/* `px */[
                              25096,
                              31
                            ]),
                        /* :: */[
                          Css.transform(Css.rotate(/* `deg */[
                                    4995526,
                                    -135.0
                                  ])),
                          /* [] */0
                        ]
                      ]
                    ]
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var tokenSelector = Css.style(/* :: */[
      Css.position(/* absolute */-1013592457),
      /* :: */[
        Css.zIndex(100),
        /* :: */[
          Css.paddingRight(/* `px */[
                25096,
                48
              ]),
          /* :: */[
            Css.backgroundColor(Css.hex("FFFFFF")),
            /* :: */[
              Css.boxShadow(Css.Shadow.box(Css.px(0), Css.px(10), Css.px(28), undefined, undefined, Css.hex("041A441F"))),
              /* :: */[
                Css.borderRadius(/* `px */[
                      25096,
                      10
                    ]),
                /* [] */0
              ]
            ]
          ]
        ]
      ]
    ]);

var tokenSelectorContainer = Css.style(/* :: */[
      Css.position(/* relative */903134412),
      /* [] */0
    ]);

var Style = {
  row: row,
  columnSide: columnSide,
  columnSidePadding: columnSidePadding,
  columnCenter: columnCenter,
  action: action,
  space: space,
  balanceGroup: balanceGroup,
  balanceToken: balanceToken,
  token: token,
  tokenText: tokenText,
  balance: balance,
  amount: amount,
  amountBox: amountBox,
  verticalBar: verticalBar,
  fullWidth: fullWidth,
  centerSymbolContainer: centerSymbolContainer,
  circle: circle,
  circleImg: circleImg,
  tokenAmount: tokenAmount,
  usdAmount: usdAmount,
  verticalAlign: verticalAlign,
  downArrow: downArrow,
  upArrow: upArrow,
  tokenSelector: tokenSelector,
  tokenSelectorContainer: tokenSelectorContainer
};

function leftTitle(action) {
  switch (action) {
    case /* Swap */0 :
        return "Swap from:";
    case /* AddLiquidity */1 :
        return "Deposit:";
    case /* RemoveLiquidity */2 :
        return "Pool tokens:";
    case /* Receive */3 :
        return "";
    
  }
}

function rightTitle(action) {
  switch (action) {
    case /* Swap */0 :
        return "To:";
    case /* AddLiquidity */1 :
        return "Deposit (estimate):";
    case /* RemoveLiquidity */2 :
        return "Output (estimate)";
    case /* Receive */3 :
        return "";
    
  }
}

function DexterUiExchange(Props) {
  var action$1 = Props.action;
  var tokenAmounts = Props.tokenAmounts;
  var exchanges = Props.exchanges;
  var leftToken = Props.leftToken;
  var rightToken = Props.rightToken;
  var onLeftTokenChange = Props.onLeftTokenChange;
  var onRightTokenChange = Props.onRightTokenChange;
  var onSwapLeftAndRight = Props.onSwapLeftAndRight;
  console.log(exchanges);
  var match = React.useReducer((function (state, action) {
          if (action.tag) {
            return /* record */[
                    /* showLeft */state[/* showLeft */0],
                    /* showRight */action[0],
                    /* leftAmount */state[/* leftAmount */2],
                    /* rightAmount */state[/* rightAmount */3]
                  ];
          } else {
            return /* record */[
                    /* showLeft */action[0],
                    /* showRight */state[/* showRight */1],
                    /* leftAmount */state[/* leftAmount */2],
                    /* rightAmount */state[/* rightAmount */3]
                  ];
          }
        }), /* record */[
        /* showLeft */false,
        /* showRight */false,
        /* leftAmount */0,
        /* rightAmount */0
      ]);
  var dispatch = match[1];
  var state = match[0];
  return React.createElement(React.Fragment, undefined, React.createElement("div", {
                  className: row
                }, React.createElement("div", {
                      className: columnSide
                    }, React.createElement("div", {
                          className: columnSidePadding
                        }, React.createElement("div", {
                              className: action
                            }, leftTitle(action$1)), React.createElement("div", {
                              className: balanceGroup
                            }, React.createElement("div", {
                                  className: balanceToken,
                                  onClick: (function (param) {
                                      return Curry._1(dispatch, /* ShowLeft */Block.__(0, [!state[/* showLeft */0]]));
                                    })
                                }, React.createElement("img", {
                                      className: token,
                                      src: "tezos.png"
                                    }), React.createElement("span", {
                                      className: tokenText
                                    }, leftToken[/* symbol */0]), React.createElement("div", {
                                      className: verticalAlign
                                    }, state[/* showLeft */0] ? React.createElement("div", {
                                            className: upArrow
                                          }) : React.createElement("div", {
                                            className: downArrow
                                          }))), state[/* showLeft */0] ? React.createElement("div", {
                                    className: tokenSelectorContainer
                                  }, React.createElement("div", {
                                        className: tokenSelector
                                      }, React.createElement(DexterUiTokenSearch.make, {
                                            tokenAmounts: tokenAmounts,
                                            onTokenChange: (function (token) {
                                                Curry._1(dispatch, /* ShowLeft */Block.__(0, [false]));
                                                return Curry._1(onLeftTokenChange, token);
                                              })
                                          }))) : null, React.createElement("div", {
                                  className: balance
                                }, "You have: " + String(leftToken[/* amount */2]))), React.createElement("div", {
                              className: space
                            }, React.createElement("div", {
                                  className: amount
                                }, "Amount:"), React.createElement("div", {
                                  className: amountBox
                                }, React.createElement("input", {
                                      className: tokenAmount,
                                      value: String(state[/* leftAmount */2]),
                                      onChange: (function (param) {
                                          return /* () */0;
                                        })
                                    }), React.createElement("div", {
                                      className: usdAmount
                                    }, "$0.00"))))), React.createElement("div", {
                      className: columnCenter
                    }, React.createElement("div", {
                          className: fullWidth
                        }, React.createElement("div", {
                              className: centerSymbolContainer
                            }, React.createElement("div", {
                                  className: verticalBar
                                }), React.createElement("div", {
                                  className: circle,
                                  onClick: (function (param) {
                                      return Curry._1(onSwapLeftAndRight, /* () */0);
                                    })
                                }, React.createElement("img", {
                                      className: circleImg,
                                      src: "sort-white.svg"
                                    })), React.createElement("div", {
                                  className: verticalBar
                                })))), React.createElement("div", {
                      className: columnSide
                    }, React.createElement("div", {
                          className: action
                        }, rightTitle(action$1)), React.createElement("div", {
                          className: balanceGroup
                        }, React.createElement("div", {
                              className: balanceToken,
                              onClick: (function (param) {
                                  return Curry._1(dispatch, /* ShowRight */Block.__(1, [!state[/* showRight */1]]));
                                })
                            }, React.createElement("img", {
                                  className: token,
                                  src: "abc.png"
                                }), React.createElement("span", {
                                  className: tokenText
                                }, rightToken[/* symbol */0]), React.createElement("div", {
                                  className: verticalAlign
                                }, state[/* showRight */1] ? React.createElement("div", {
                                        className: upArrow
                                      }) : React.createElement("div", {
                                        className: downArrow
                                      }))), state[/* showRight */1] ? React.createElement("div", {
                                className: tokenSelectorContainer
                              }, React.createElement("div", {
                                    className: tokenSelector
                                  }, React.createElement(DexterUiTokenSearch.make, {
                                        tokenAmounts: tokenAmounts,
                                        onTokenChange: (function (token) {
                                            Curry._1(dispatch, /* ShowRight */Block.__(1, [false]));
                                            return Curry._1(onRightTokenChange, token);
                                          })
                                      }))) : null, React.createElement("div", {
                              className: balance
                            }, "You have: " + String(rightToken[/* amount */2]))), React.createElement("div", {
                          className: space
                        }, React.createElement("div", {
                              className: amount
                            }, "Amount:"), React.createElement("div", {
                              className: amountBox
                            }, React.createElement("input", {
                                  className: tokenAmount,
                                  value: String(state[/* rightAmount */3]),
                                  onChange: (function (param) {
                                      return /* () */0;
                                    })
                                }), React.createElement("div", {
                                  className: usdAmount
                                }, "$0.00"))))), React.createElement("div", {
                  className: row
                }, React.createElement("div", {
                      className: columnSide
                    }), React.createElement("div", {
                      className: columnCenter
                    }, React.createElement(DexterUiExchangeRate.make, { })), React.createElement("div", {
                      className: columnSide
                    })));
}

var make = DexterUiExchange;

exports.Style = Style;
exports.leftTitle = leftTitle;
exports.rightTitle = rightTitle;
exports.make = make;
/* row Not a pure module */
