'use strict';

var Css = require("bs-css/lib/js/src/Css.js");

var black = Css.hex("082344");

var grey = Css.hex("758DA6");

var lightGrey = Css.hex("C4D0DC");

var blue = Css.hex("0258FF");

var lightBlue = Css.hex("A3BDFB");

var shadowColor = Css.hex("0A28642E");

var white = Css.hex("FFFFFF");

exports.black = black;
exports.grey = grey;
exports.lightGrey = lightGrey;
exports.blue = blue;
exports.lightBlue = lightBlue;
exports.shadowColor = shadowColor;
exports.white = white;
/* black Not a pure module */
