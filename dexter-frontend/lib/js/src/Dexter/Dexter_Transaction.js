'use strict';


function toString(t) {
  switch (t) {
    case /* MutezToTokens */0 :
        return "Tez (XTZ) to Tokens";
    case /* TokensToMutez */1 :
        return "Tokens to Tez (XTZ)";
    case /* AddLiquidity */2 :
        return "Add Liquidity";
    case /* RemoveLiquidity */3 :
        return "Remove Liquidity";
    
  }
}

exports.toString = toString;
/* No side effect */
