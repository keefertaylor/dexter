'use strict';

var Css = require("bs-css/lib/js/src/Css.js");
var React = require("react");

var navbar = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* :: */[
        Css.justifyContent(/* flexEnd */924268066),
        /* :: */[
          Css.alignContent(/* flexEnd */924268066),
          /* :: */[
            Css.paddingTop(Css.px(10)),
            /* [] */0
          ]
        ]
      ]
    ]);

var tutorials = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* :: */[
        Css.flex(/* `num */[
              5496390,
              1.0
            ]),
        /* :: */[
          Css.fontSize(Css.px(18)),
          /* [] */0
        ]
      ]
    ]);

var settings = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* :: */[
        Css.flex(/* `num */[
              5496390,
              1.0
            ]),
        /* :: */[
          Css.fontSize(Css.px(18)),
          /* [] */0
        ]
      ]
    ]);

var tab = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* :: */[
        Css.cursor(/* pointer */-786317123),
        /* :: */[
          Css.fontSize(Css.px(18)),
          /* :: */[
            Css.fontFamily("Source Sans Pro, Semibold"),
            /* :: */[
              Css.paddingTop(Css.px(10)),
              /* :: */[
                Css.paddingBottom(Css.px(15)),
                /* [] */0
              ]
            ]
          ]
        ]
      ]
    ]);

var tabInternal = Css.style(/* :: */[
      Css.display(/* table */182695950),
      /* [] */0
    ]);

var tabTutorials = Css.style(/* :: */[
      Css.paddingRight(/* `px */[
            25096,
            50
          ]),
      /* [] */0
    ]);

var icon = Css.style(/* :: */[
      Css.width(/* `px */[
            25096,
            24
          ]),
      /* :: */[
        Css.height(/* `px */[
              25096,
              24
            ]),
        /* [] */0
      ]
    ]);

var tabTitle = Css.style(/* :: */[
      Css.display(/* tableCell */793912528),
      /* :: */[
        Css.verticalAlign(/* middle */-866200747),
        /* :: */[
          Css.paddingLeft(/* `px */[
                25096,
                8
              ]),
          /* [] */0
        ]
      ]
    ]);

var Style = {
  navbar: navbar,
  tutorials: tutorials,
  settings: settings,
  tab: tab,
  tabInternal: tabInternal,
  tabTutorials: tabTutorials,
  icon: icon,
  tabTitle: tabTitle
};

function DexterUiSecondaryNavBar(Props) {
  return React.createElement("div", {
              className: navbar
            }, React.createElement("div", {
                  className: tab
                }, React.createElement("div", {
                      className: Css.merge(/* :: */[
                            tabInternal,
                            /* :: */[
                              tabTutorials,
                              /* [] */0
                            ]
                          ])
                    }, React.createElement("img", {
                          className: icon,
                          src: "round-help-button-grey.svg"
                        }), React.createElement("span", {
                          className: tabTitle
                        }, "Tutorials/About"))), React.createElement("div", {
                  className: tab
                }, React.createElement("div", {
                      className: tabInternal
                    }, React.createElement("img", {
                          className: icon,
                          src: "settings-grey.svg"
                        }), React.createElement("span", {
                          className: tabTitle
                        }, "Settings"))));
}

var make = DexterUiSecondaryNavBar;

exports.Style = Style;
exports.make = make;
/* navbar Not a pure module */
