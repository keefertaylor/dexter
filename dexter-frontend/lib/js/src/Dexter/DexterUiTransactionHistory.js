'use strict';

var React = require("react");

function DexterUiTransactionHistory(Props) {
  var account = Props.account;
  var recentTransactions = Props.recentTransactions;
  var poolTokens = Props.poolTokens;
  console.log(recentTransactions);
  console.log(poolTokens);
  if (account !== undefined) {
    return React.createElement("div", undefined, "Show the account: " + account);
  } else {
    return React.createElement("div", undefined, React.createElement("div", undefined, "There are no transactions yet!"));
  }
}

var make = DexterUiTransactionHistory;

exports.make = make;
/* react Not a pure module */
