'use strict';

var Css = require("bs-css/lib/js/src/Css.js");
var Curry = require("bs-platform/lib/js/curry.js");
var React = require("react");

var navbar = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* :: */[
        Css.justifyContent(Css.spaceBetween),
        /* :: */[
          Css.paddingTop(Css.px(10)),
          /* [] */0
        ]
      ]
    ]);

var title = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* :: */[
        Css.flex(/* `num */[
              5496390,
              0.5
            ]),
        /* :: */[
          Css.fontSize(Css.px(26)),
          /* :: */[
            Css.fontFamily("Source Sans Pro, Semibold"),
            /* :: */[
              Css.color(Css.hex("495B6D")),
              /* :: */[
                Css.marginLeft(/* `percent */[
                      -119887163,
                      2.0
                    ]),
                /* [] */0
              ]
            ]
          ]
        ]
      ]
    ]);

var tab = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* :: */[
        Css.flex(/* `num */[
              5496390,
              0.5
            ]),
        /* :: */[
          Css.cursor(/* pointer */-786317123),
          /* :: */[
            Css.justifyContent(/* center */98248149),
            /* :: */[
              Css.fontSize(Css.px(18)),
              /* :: */[
                Css.fontFamily("Source Sans Pro, Semibold"),
                /* :: */[
                  Css.paddingTop(Css.px(10)),
                  /* :: */[
                    Css.paddingBottom(Css.px(15)),
                    /* [] */0
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var active = Css.style(/* :: */[
      Css.unsafe("backgroundColor", "white"),
      /* :: */[
        Css.color(Css.hex("0258FF")),
        /* :: */[
          Css.fontSize(/* `px */[
                25096,
                18
              ]),
          /* :: */[
            Css.borderTopLeftRadius(Css.px(5)),
            /* :: */[
              Css.borderTopRightRadius(Css.px(5)),
              /* [] */0
            ]
          ]
        ]
      ]
    ]);

var inactive = Css.style(/* :: */[
      Css.unsafe("backgroundColor", "inherit"),
      /* [] */0
    ]);

var tabInternal = Css.style(/* :: */[
      Css.display(/* table */182695950),
      /* [] */0
    ]);

var swapIcon = Css.style(/* :: */[
      Css.width(/* `px */[
            25096,
            24
          ]),
      /* :: */[
        Css.height(/* `px */[
              25096,
              16
            ]),
        /* [] */0
      ]
    ]);

var addIcon = Css.style(/* :: */[
      Css.width(/* `px */[
            25096,
            24
          ]),
      /* :: */[
        Css.height(/* `px */[
              25096,
              24
            ]),
        /* [] */0
      ]
    ]);

var tabTitle = Css.style(/* :: */[
      Css.display(/* tableCell */793912528),
      /* :: */[
        Css.verticalAlign(/* middle */-866200747),
        /* :: */[
          Css.paddingLeft(/* `px */[
                25096,
                8
              ]),
          /* [] */0
        ]
      ]
    ]);

var Style = {
  navbar: navbar,
  title: title,
  tab: tab,
  active: active,
  inactive: inactive,
  tabInternal: tabInternal,
  swapIcon: swapIcon,
  addIcon: addIcon,
  tabTitle: tabTitle
};

function isActive(x, y) {
  if (x === y) {
    return active;
  } else {
    return inactive;
  }
}

function getIconSrc(action, currentAction) {
  var actionString;
  switch (action) {
    case /* Swap */0 :
        actionString = "sort";
        break;
    case /* AddLiquidity */1 :
        actionString = "add";
        break;
    case /* RemoveLiquidity */2 :
        actionString = "minus";
        break;
    case /* Receive */3 :
        actionString = "money";
        break;
    
  }
  if (action === currentAction) {
    return actionString + "-blue.svg";
  } else {
    return actionString + "-grey.svg";
  }
}

function DexterUiMainNavBar(Props) {
  var action = Props.action;
  var onChange = Props.onChange;
  return React.createElement("div", {
              className: navbar
            }, React.createElement("div", {
                  className: title
                }, "Dexter"), React.createElement("div", {
                  className: Css.merge(/* :: */[
                        tab,
                        /* :: */[
                          isActive(/* Swap */0, action),
                          /* [] */0
                        ]
                      ]),
                  onClick: (function (_event) {
                      return Curry._1(onChange, /* Swap */0);
                    })
                }, React.createElement("div", {
                      className: tabInternal
                    }, React.createElement("img", {
                          className: swapIcon,
                          src: getIconSrc(/* Swap */0, action)
                        }), React.createElement("span", {
                          className: tabTitle
                        }, "Swap"))), React.createElement("div", {
                  className: Css.merge(/* :: */[
                        tab,
                        /* :: */[
                          isActive(/* AddLiquidity */1, action),
                          /* [] */0
                        ]
                      ]),
                  onClick: (function (_event) {
                      return Curry._1(onChange, /* AddLiquidity */1);
                    })
                }, React.createElement("div", {
                      className: tabInternal
                    }, React.createElement("img", {
                          className: addIcon,
                          src: getIconSrc(/* AddLiquidity */1, action)
                        }), React.createElement("span", {
                          className: tabTitle
                        }, "Add Liquidity"))), React.createElement("div", {
                  className: Css.merge(/* :: */[
                        tab,
                        /* :: */[
                          isActive(/* RemoveLiquidity */2, action),
                          /* [] */0
                        ]
                      ]),
                  onClick: (function (_event) {
                      return Curry._1(onChange, /* RemoveLiquidity */2);
                    })
                }, React.createElement("div", {
                      className: tabInternal
                    }, React.createElement("img", {
                          className: addIcon,
                          src: getIconSrc(/* RemoveLiquidity */2, action)
                        }), React.createElement("span", {
                          className: tabTitle
                        }, "Remove Liquidity"))), React.createElement("div", {
                  className: Css.merge(/* :: */[
                        tab,
                        /* :: */[
                          isActive(/* Receive */3, action),
                          /* [] */0
                        ]
                      ]),
                  onClick: (function (_event) {
                      return Curry._1(onChange, /* Receive */3);
                    })
                }, React.createElement("div", {
                      className: tabInternal
                    }, React.createElement("img", {
                          className: addIcon,
                          src: getIconSrc(/* Receive */3, action)
                        }), React.createElement("span", {
                          className: tabTitle
                        }, "Receive"))));
}

var make = DexterUiMainNavBar;

exports.Style = Style;
exports.isActive = isActive;
exports.getIconSrc = getIconSrc;
exports.make = make;
/* navbar Not a pure module */
