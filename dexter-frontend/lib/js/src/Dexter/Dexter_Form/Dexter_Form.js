'use strict';

var React = require("react");
var Dexter_Form_AddLiquidity = require("./Dexter_Form_AddLiquidity.js");
var Dexter_Form_MutezToTokens = require("./Dexter_Form_MutezToTokens.js");
var Dexter_Form_TokensToMutez = require("./Dexter_Form_TokensToMutez.js");
var Dexter_Form_RemoveLiquidity = require("./Dexter_Form_RemoveLiquidity.js");

function Dexter_Form(Props) {
  var address = Props.address;
  var dexterContract = Props.dexterContract;
  var dexterTransaction = Props.dexterTransaction;
  var onChange = Props.onChange;
  switch (dexterTransaction) {
    case /* MutezToTokens */0 :
        return React.createElement(Dexter_Form_MutezToTokens.make, {
                    address: address,
                    dexterContract: dexterContract,
                    onChange: onChange
                  });
    case /* TokensToMutez */1 :
        return React.createElement(Dexter_Form_TokensToMutez.make, {
                    address: address,
                    dexterContract: dexterContract,
                    onChange: onChange
                  });
    case /* AddLiquidity */2 :
        return React.createElement(Dexter_Form_AddLiquidity.make, {
                    address: address,
                    dexterContract: dexterContract,
                    onChange: onChange
                  });
    case /* RemoveLiquidity */3 :
        return React.createElement(Dexter_Form_RemoveLiquidity.make, {
                    address: address,
                    dexterContract: dexterContract,
                    onChange: onChange
                  });
    
  }
}

var make = Dexter_Form;

exports.make = make;
/* react Not a pure module */
