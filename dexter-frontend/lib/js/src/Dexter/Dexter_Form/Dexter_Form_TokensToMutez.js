'use strict';

var Curry = require("bs-platform/lib/js/curry.js");
var React = require("react");
var Belt_Option = require("bs-platform/lib/js/belt_Option.js");
var Dexter_Valid = require("../Dexter_Valid.js");
var DexterContract = require("../../DexterContract/DexterContract.js");
var Tezos_Timestamp = require("../../Tezos/Tezos_Timestamp.js");
var CommonUi_NumericInput = require("../../CommonUi/CommonUi_NumericInput.js");

function Dexter_Form_TokensToMutez(Props) {
  var address = Props.address;
  var dexterContract = Props.dexterContract;
  var onChange = Props.onChange;
  var match = React.useReducer((function (_state, action) {
          return action[0];
        }), /* record */[
        /* tokensSold : Empty */0,
        /* minMutezRequired : Empty */0
      ]);
  var dispatch = match[1];
  var state = match[0];
  return React.createElement("form", undefined, React.createElement("p", undefined, React.createElement("label", undefined, "Amount of Tokens You Want to Sell"), React.createElement("br", undefined), React.createElement(CommonUi_NumericInput.TokenInput.make, {
                      onChange: (function (token) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* tokensSold : Ok */[token],
                                        /* minMutezRequired */state[/* minMutezRequired */1]
                                      ]]);
                        }),
                      onEmpty: (function (param) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* tokensSold : Empty */0,
                                        /* minMutezRequired */state[/* minMutezRequired */1]
                                      ]]);
                        }),
                      onError: (function (param) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* tokensSold : Error */1,
                                        /* minMutezRequired */state[/* minMutezRequired */1]
                                      ]]);
                        }),
                      numValue: Dexter_Valid.toOption(state[/* tokensSold */0]),
                      initialValue: undefined,
                      style: Dexter_Valid.toStyle(state[/* tokensSold */0])
                    })), React.createElement("p", undefined, React.createElement("label", undefined, "Minimum Amount of Tez You Want to Buy"), React.createElement("br", undefined), React.createElement(CommonUi_NumericInput.MutezInput.make, {
                      onChange: (function (mutez) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* tokensSold */state[/* tokensSold */0],
                                        /* minMutezRequired : Ok */[mutez]
                                      ]]);
                        }),
                      onError: (function (param) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* tokensSold */state[/* tokensSold */0],
                                        /* minMutezRequired : Empty */0
                                      ]]);
                        }),
                      numValue: Dexter_Valid.toOption(state[/* minMutezRequired */1]),
                      initialValue: undefined
                    })), React.createElement("p", undefined, React.createElement("button", {
                      disabled: Belt_Option.isNone(address) || Dexter_Valid.notOk(state[/* tokensSold */0]) || Dexter_Valid.notOk(state[/* minMutezRequired */1]),
                      type: "button",
                      onClick: (function (_event) {
                          var match = state[/* tokensSold */0];
                          var match$1 = state[/* minMutezRequired */1];
                          if (typeof match === "number" || typeof match$1 === "number") {
                            return /* () */0;
                          } else {
                            DexterContract.tokensToMutez(dexterContract, match[0], match$1[0], Tezos_Timestamp.ofString("2020-06-29T18:00:21Z")).then((function (res) {
                                    Curry._2(onChange, /* TokensToMutez */1, res);
                                    console.log("tokensToMutez: ");
                                    console.log(res);
                                    return Promise.resolve(/* () */0);
                                  }));
                            return /* () */0;
                          }
                        })
                    }, "Buy Tez with Tokens")));
}

var make = Dexter_Form_TokensToMutez;

exports.make = make;
/* react Not a pure module */
