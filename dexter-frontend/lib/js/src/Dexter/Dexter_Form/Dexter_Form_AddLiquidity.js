'use strict';

var Curry = require("bs-platform/lib/js/curry.js");
var React = require("react");
var Belt_Option = require("bs-platform/lib/js/belt_Option.js");
var Dexter_Valid = require("../Dexter_Valid.js");
var DexterContract = require("../../DexterContract/DexterContract.js");
var Tezos_Timestamp = require("../../Tezos/Tezos_Timestamp.js");
var CommonUi_NumericInput = require("../../CommonUi/CommonUi_NumericInput.js");

function Dexter_Form_AddLiquidity(Props) {
  var address = Props.address;
  var dexterContract = Props.dexterContract;
  var onChange = Props.onChange;
  var match = React.useReducer((function (_state, action) {
          return action[0];
        }), /* record */[
        /* mutez : Empty */0,
        /* minLiquidityMinted : Empty */0,
        /* maxTokensDeposited : Empty */0
      ]);
  var dispatch = match[1];
  var state = match[0];
  return React.createElement("form", undefined, React.createElement("p", undefined, React.createElement("label", undefined, "Amount of Tez (XTZ) You Want to Deposit"), React.createElement("br", undefined), React.createElement(CommonUi_NumericInput.MutezInput.make, {
                      onChange: (function (mutez) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* mutez : Ok */[mutez],
                                        /* minLiquidityMinted */state[/* minLiquidityMinted */1],
                                        /* maxTokensDeposited */state[/* maxTokensDeposited */2]
                                      ]]);
                        }),
                      onEmpty: (function (param) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* mutez : Empty */0,
                                        /* minLiquidityMinted */state[/* minLiquidityMinted */1],
                                        /* maxTokensDeposited */state[/* maxTokensDeposited */2]
                                      ]]);
                        }),
                      onError: (function (param) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* mutez : Error */1,
                                        /* minLiquidityMinted */state[/* minLiquidityMinted */1],
                                        /* maxTokensDeposited */state[/* maxTokensDeposited */2]
                                      ]]);
                        }),
                      numValue: Dexter_Valid.toOption(state[/* mutez */0]),
                      initialValue: undefined,
                      style: Dexter_Valid.toStyle(state[/* mutez */0])
                    })), React.createElement("p", undefined, React.createElement("label", undefined, "Minimum Amount of Liquidity You Want to Mint"), React.createElement("br", undefined), React.createElement(CommonUi_NumericInput.TokenInput.make, {
                      onChange: (function (token) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* mutez */state[/* mutez */0],
                                        /* minLiquidityMinted : Ok */[token],
                                        /* maxTokensDeposited */state[/* maxTokensDeposited */2]
                                      ]]);
                        }),
                      onEmpty: (function (param) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* mutez */state[/* mutez */0],
                                        /* minLiquidityMinted : Empty */0,
                                        /* maxTokensDeposited */state[/* maxTokensDeposited */2]
                                      ]]);
                        }),
                      onError: (function (param) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* mutez */state[/* mutez */0],
                                        /* minLiquidityMinted : Error */1,
                                        /* maxTokensDeposited */state[/* maxTokensDeposited */2]
                                      ]]);
                        }),
                      numValue: Dexter_Valid.toOption(state[/* minLiquidityMinted */1]),
                      initialValue: undefined,
                      style: Dexter_Valid.toStyle(state[/* minLiquidityMinted */1])
                    })), React.createElement("p", undefined, React.createElement("label", undefined, "Maximum Amount of Tokens You Want to Deposit"), React.createElement("br", undefined), React.createElement(CommonUi_NumericInput.TokenInput.make, {
                      onChange: (function (token) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* mutez */state[/* mutez */0],
                                        /* minLiquidityMinted */state[/* minLiquidityMinted */1],
                                        /* maxTokensDeposited : Ok */[token]
                                      ]]);
                        }),
                      onEmpty: (function (param) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* mutez */state[/* mutez */0],
                                        /* minLiquidityMinted */state[/* minLiquidityMinted */1],
                                        /* maxTokensDeposited : Empty */0
                                      ]]);
                        }),
                      onError: (function (param) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* mutez */state[/* mutez */0],
                                        /* minLiquidityMinted */state[/* minLiquidityMinted */1],
                                        /* maxTokensDeposited : Error */1
                                      ]]);
                        }),
                      numValue: Dexter_Valid.toOption(state[/* maxTokensDeposited */2]),
                      initialValue: undefined,
                      style: Dexter_Valid.toStyle(state[/* maxTokensDeposited */2])
                    })), React.createElement("p", undefined, React.createElement("button", {
                      disabled: Belt_Option.isNone(address) || Dexter_Valid.notOk(state[/* mutez */0]) || Dexter_Valid.notOk(state[/* minLiquidityMinted */1]) || Dexter_Valid.notOk(state[/* maxTokensDeposited */2]),
                      type: "button",
                      onClick: (function (_event) {
                          var match = state[/* mutez */0];
                          var match$1 = state[/* minLiquidityMinted */1];
                          var match$2 = state[/* maxTokensDeposited */2];
                          if (typeof match === "number" || typeof match$1 === "number" || typeof match$2 === "number") {
                            return /* () */0;
                          } else {
                            DexterContract.addLiquidity(dexterContract, match[0], match$1[0], match$2[0], Tezos_Timestamp.ofString("2020-06-29T18:00:21Z")).then((function (res) {
                                    Curry._2(onChange, /* AddLiquidity */2, res);
                                    console.log("addLiquidity: ");
                                    console.log(res);
                                    return Promise.resolve(/* () */0);
                                  }));
                            return /* () */0;
                          }
                        })
                    }, "Add Liquidity")));
}

var make = Dexter_Form_AddLiquidity;

exports.make = make;
/* react Not a pure module */
