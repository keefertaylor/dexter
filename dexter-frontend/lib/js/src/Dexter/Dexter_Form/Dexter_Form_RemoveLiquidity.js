'use strict';

var Curry = require("bs-platform/lib/js/curry.js");
var React = require("react");
var Belt_Option = require("bs-platform/lib/js/belt_Option.js");
var Dexter_Valid = require("../Dexter_Valid.js");
var DexterContract = require("../../DexterContract/DexterContract.js");
var Tezos_Timestamp = require("../../Tezos/Tezos_Timestamp.js");
var CommonUi_NumericInput = require("../../CommonUi/CommonUi_NumericInput.js");

function Dexter_Form_RemoveLiquidity(Props) {
  var address = Props.address;
  var dexterContract = Props.dexterContract;
  var onChange = Props.onChange;
  var match = React.useReducer((function (_state, action) {
          return action[0];
        }), /* record */[
        /* liquidityBurned : Empty */0,
        /* minMutezWithdrawn : Empty */0,
        /* minTokensWithdrawn : Empty */0
      ]);
  var dispatch = match[1];
  var state = match[0];
  return React.createElement("form", undefined, React.createElement("p", undefined, React.createElement("label", undefined, "Amount of Liquidity Tokens You Want to Burn"), React.createElement("br", undefined), React.createElement(CommonUi_NumericInput.TokenInput.make, {
                      onChange: (function (token) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* liquidityBurned : Ok */[token],
                                        /* minMutezWithdrawn */state[/* minMutezWithdrawn */1],
                                        /* minTokensWithdrawn */state[/* minTokensWithdrawn */2]
                                      ]]);
                        }),
                      onEmpty: (function (param) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* liquidityBurned : Empty */0,
                                        /* minMutezWithdrawn */state[/* minMutezWithdrawn */1],
                                        /* minTokensWithdrawn */state[/* minTokensWithdrawn */2]
                                      ]]);
                        }),
                      onError: (function (param) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* liquidityBurned : Error */1,
                                        /* minMutezWithdrawn */state[/* minMutezWithdrawn */1],
                                        /* minTokensWithdrawn */state[/* minTokensWithdrawn */2]
                                      ]]);
                        }),
                      numValue: Dexter_Valid.toOption(state[/* liquidityBurned */0]),
                      initialValue: undefined,
                      style: Dexter_Valid.toStyle(state[/* liquidityBurned */0])
                    })), React.createElement("p", undefined, React.createElement("label", undefined, "Minimum Amount of Liquidity You Want to Withdraw"), React.createElement("br", undefined), React.createElement(CommonUi_NumericInput.MutezInput.make, {
                      onChange: (function (mutez) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* liquidityBurned */state[/* liquidityBurned */0],
                                        /* minMutezWithdrawn : Ok */[mutez],
                                        /* minTokensWithdrawn */state[/* minTokensWithdrawn */2]
                                      ]]);
                        }),
                      onEmpty: (function (param) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* liquidityBurned */state[/* liquidityBurned */0],
                                        /* minMutezWithdrawn : Empty */0,
                                        /* minTokensWithdrawn */state[/* minTokensWithdrawn */2]
                                      ]]);
                        }),
                      onError: (function (param) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* liquidityBurned */state[/* liquidityBurned */0],
                                        /* minMutezWithdrawn : Error */1,
                                        /* minTokensWithdrawn */state[/* minTokensWithdrawn */2]
                                      ]]);
                        }),
                      numValue: Dexter_Valid.toOption(state[/* minMutezWithdrawn */1]),
                      initialValue: undefined,
                      style: Dexter_Valid.toStyle(state[/* minMutezWithdrawn */1])
                    })), React.createElement("p", undefined, React.createElement("label", undefined, "Minimum Amount of Tokens You Want to Withdraw"), React.createElement("br", undefined), React.createElement(CommonUi_NumericInput.TokenInput.make, {
                      onChange: (function (token) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* liquidityBurned */state[/* liquidityBurned */0],
                                        /* minMutezWithdrawn */state[/* minMutezWithdrawn */1],
                                        /* minTokensWithdrawn : Ok */[token]
                                      ]]);
                        }),
                      onEmpty: (function (param) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* liquidityBurned */state[/* liquidityBurned */0],
                                        /* minMutezWithdrawn */state[/* minMutezWithdrawn */1],
                                        /* minTokensWithdrawn : Empty */0
                                      ]]);
                        }),
                      onError: (function (param) {
                          return Curry._1(dispatch, /* Update */[/* record */[
                                        /* liquidityBurned */state[/* liquidityBurned */0],
                                        /* minMutezWithdrawn */state[/* minMutezWithdrawn */1],
                                        /* minTokensWithdrawn : Error */1
                                      ]]);
                        }),
                      numValue: Dexter_Valid.toOption(state[/* minTokensWithdrawn */2]),
                      initialValue: undefined,
                      style: Dexter_Valid.toStyle(state[/* minTokensWithdrawn */2])
                    })), React.createElement("p", undefined, React.createElement("button", {
                      disabled: Belt_Option.isNone(address) || Dexter_Valid.notOk(state[/* liquidityBurned */0]) || Dexter_Valid.notOk(state[/* minMutezWithdrawn */1]) || Dexter_Valid.notOk(state[/* minTokensWithdrawn */2]),
                      type: "button",
                      onClick: (function (_event) {
                          var match = state[/* liquidityBurned */0];
                          var match$1 = state[/* minMutezWithdrawn */1];
                          var match$2 = state[/* minTokensWithdrawn */2];
                          if (typeof match === "number" || typeof match$1 === "number" || typeof match$2 === "number") {
                            return /* () */0;
                          } else {
                            DexterContract.removeLiquidity(dexterContract, match[0], match$1[0], match$2[0], Tezos_Timestamp.ofString("2020-06-29T18:00:21Z")).then((function (res) {
                                    Curry._2(onChange, /* RemoveLiquidity */3, res);
                                    console.log("removeLiquidity: ");
                                    console.log(res);
                                    return Promise.resolve(/* () */0);
                                  }));
                            return /* () */0;
                          }
                        })
                    }, "Remove Liquidity")));
}

var make = Dexter_Form_RemoveLiquidity;

exports.make = make;
/* react Not a pure module */
