'use strict';

var List = require("bs-platform/lib/js/list.js");
var $$Array = require("bs-platform/lib/js/array.js");
var Caml_option = require("bs-platform/lib/js/caml_option.js");
var Belt_MapString = require("bs-platform/lib/js/belt_MapString.js");

function ofList(cs) {
  var match = List.fold_left((function (param, c) {
          var match = c[1];
          var rToken = match[1];
          var rExchange = match[0];
          var xs = param[1];
          var errors = param[0];
          if (!rExchange.tag && !rToken.tag) {
            return /* tuple */[
                    errors,
                    List.concat(/* :: */[
                          xs,
                          /* :: */[
                            /* :: */[
                              /* tuple */[
                                c[0],
                                /* tuple */[
                                  rExchange[0],
                                  rToken[0]
                                ]
                              ],
                              /* [] */0
                            ],
                            /* [] */0
                          ]
                        ])
                  ];
          }
          var exchangeError;
          exchangeError = rExchange.tag ? /* :: */[
              rExchange[0],
              /* [] */0
            ] : /* [] */0;
          var tokenError;
          tokenError = rToken.tag ? /* :: */[
              rToken[0],
              /* [] */0
            ] : /* [] */0;
          return /* tuple */[
                  List.concat(/* :: */[
                        errors,
                        /* :: */[
                          exchangeError,
                          /* :: */[
                            tokenError,
                            /* [] */0
                          ]
                        ]
                      ]),
                  xs
                ];
        }), /* tuple */[
        /* [] */0,
        /* [] */0
      ], cs);
  var errors = match[0];
  if (List.length(errors) > 0) {
    console.log(errors);
    return ;
  } else {
    return Caml_option.some(Belt_MapString.fromArray($$Array.of_list(match[1])));
  }
}

exports.ofList = ofList;
/* No side effect */
