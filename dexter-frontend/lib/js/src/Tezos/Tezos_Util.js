'use strict';

var List = require("bs-platform/lib/js/list.js");
var Caml_array = require("bs-platform/lib/js/caml_array.js");
var Caml_format = require("bs-platform/lib/js/caml_format.js");
var Caml_string = require("bs-platform/lib/js/caml_string.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.bs.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");
var Caml_builtin_exceptions = require("bs-platform/lib/js/caml_builtin_exceptions.js");

function unwrapResult(r) {
  if (r.tag) {
    throw [
          Json_decode.DecodeError,
          r[0]
        ];
  } else {
    return r[0];
  }
}

function floatOfString(x) {
  try {
    return Caml_format.caml_float_of_string(x);
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn[0] === Caml_builtin_exceptions.failure) {
      return ;
    } else {
      throw exn;
    }
  }
}

function int64OfString(x) {
  try {
    return Caml_format.caml_int64_of_string(x);
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn[0] === Caml_builtin_exceptions.failure) {
      return ;
    } else {
      throw exn;
    }
  }
}

function explode(s) {
  var _i = s.length - 1 | 0;
  var _l = /* [] */0;
  while(true) {
    var l = _l;
    var i = _i;
    if (i < 0) {
      return l;
    } else {
      _l = /* :: */[
        Caml_string.get(s, i),
        l
      ];
      _i = i - 1 | 0;
      continue ;
    }
  };
}

function getPositionOfLastDecimalDigit(floatString) {
  var match = floatOfString(floatString);
  if (match !== undefined) {
    var split = floatString.split(".");
    if (split.length === 0) {
      return ;
    } else if (split.length === 1) {
      return 0;
    } else if (split.length === 2) {
      return Caml_array.caml_array_get(split, 1).length;
    } else {
      return ;
    }
  }
  
}

function getPositionOfSmallestNonZero($$float) {
  var floatString = $$float.toString();
  var split = floatString.split(".");
  if (split.length === 1 || split.length !== 2) {
    return 0;
  } else {
    var str = Caml_array.caml_array_get(split, 1);
    var chars = explode(str);
    var length = List.length(chars);
    var charsRev = List.rev(chars);
    return List.fold_left((function (param, $$char) {
                    var finished = param[1];
                    var l = param[0];
                    if (finished) {
                      return /* tuple */[
                              l,
                              finished
                            ];
                    } else if ($$char === /* "0" */48) {
                      return /* tuple */[
                              l - 1 | 0,
                              false
                            ];
                    } else {
                      return /* tuple */[
                              l,
                              true
                            ];
                    }
                  }), /* tuple */[
                  length,
                  false
                ], charsRev)[0];
  }
}

exports.unwrapResult = unwrapResult;
exports.floatOfString = floatOfString;
exports.int64OfString = int64OfString;
exports.explode = explode;
exports.getPositionOfLastDecimalDigit = getPositionOfLastDecimalDigit;
exports.getPositionOfSmallestNonZero = getPositionOfSmallestNonZero;
/* No side effect */
