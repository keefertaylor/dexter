'use strict';

var List = require("bs-platform/lib/js/list.js");
var Block = require("bs-platform/lib/js/block.js");
var Int64 = require("bs-platform/lib/js/int64.js");
var Tezos_Util = require("./Tezos_Util.js");
var Caml_format = require("bs-platform/lib/js/caml_format.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.bs.js");
var Json_encode = require("@glennsl/bs-json/lib/js/src/Json_encode.bs.js");
var Tezos_Primitives = require("./Tezos_Primitives.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");
var Caml_builtin_exceptions = require("bs-platform/lib/js/caml_builtin_exceptions.js");

function encode(expression) {
  switch (expression.tag | 0) {
    case /* IntExpression */0 :
        return Json_encode.object_(/* :: */[
                    /* tuple */[
                      "int",
                      Int64.to_string(expression[0])
                    ],
                    /* [] */0
                  ]);
    case /* StringExpression */1 :
        return Json_encode.object_(/* :: */[
                    /* tuple */[
                      "string",
                      expression[0]
                    ],
                    /* [] */0
                  ]);
    case /* BytesExpression */2 :
        return Json_encode.object_(/* :: */[
                    /* tuple */[
                      "bytes",
                      expression[0]
                    ],
                    /* [] */0
                  ]);
    case /* CodeExpression */3 :
        return Json_encode.list(encode, expression[0]);
    case /* ContractExpression */4 :
        var annots = expression[2];
        var args = expression[1];
        var argsEncoded = args !== undefined ? /* :: */[
            /* tuple */[
              "args",
              Json_encode.list(encode, args)
            ],
            /* [] */0
          ] : /* [] */0;
        var annotsEncoded = annots !== undefined ? /* :: */[
            /* tuple */[
              "annots",
              Json_encode.list((function (prim) {
                      return prim;
                    }), annots)
            ],
            /* [] */0
          ] : /* [] */0;
        return Json_encode.object_(List.concat(/* :: */[
                        /* :: */[
                          /* tuple */[
                            "prim",
                            Tezos_Primitives.encode(expression[0])
                          ],
                          /* [] */0
                        ],
                        /* :: */[
                          argsEncoded,
                          /* :: */[
                            annotsEncoded,
                            /* [] */0
                          ]
                        ]
                      ]));
    
  }
}

function intOfStringOpt(x) {
  try {
    return Caml_format.caml_int_of_string(x);
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn[0] === Caml_builtin_exceptions.failure) {
      return ;
    } else {
      throw exn;
    }
  }
}

function int64OfStringOpt(x) {
  try {
    return Caml_format.caml_int64_of_string(x);
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn[0] === Caml_builtin_exceptions.failure) {
      return ;
    } else {
      throw exn;
    }
  }
}

function decode(json) {
  var v;
  try {
    v = Json_decode.list((function (a) {
            return Tezos_Util.unwrapResult(decode(a));
          }), json);
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn[0] === Json_decode.DecodeError) {
      var exit = 0;
      var v$1;
      try {
        v$1 = Json_decode.field("int", Json_decode.string, json);
        exit = 2;
      }
      catch (raw_exn$1){
        var exn$1 = Caml_js_exceptions.internalToOCamlException(raw_exn$1);
        if (exn$1[0] === Json_decode.DecodeError) {
          var exit$1 = 0;
          var v$2;
          try {
            v$2 = Json_decode.field("string", Json_decode.string, json);
            exit$1 = 3;
          }
          catch (raw_exn$2){
            var exn$2 = Caml_js_exceptions.internalToOCamlException(raw_exn$2);
            if (exn$2[0] === Json_decode.DecodeError) {
              var exit$2 = 0;
              var v$3;
              try {
                v$3 = Json_decode.field("bytes", Json_decode.string, json);
                exit$2 = 4;
              }
              catch (raw_exn$3){
                var exn$3 = Caml_js_exceptions.internalToOCamlException(raw_exn$3);
                if (exn$3[0] === Json_decode.DecodeError) {
                  var exit$3 = 0;
                  var val;
                  var val$1;
                  var val$2;
                  try {
                    val = Json_decode.field("prim", (function (a) {
                            return Tezos_Util.unwrapResult(Tezos_Primitives.decode(a));
                          }), json);
                    val$1 = Json_decode.optional((function (param) {
                            return Json_decode.field("args", (function (param) {
                                          return Json_decode.list((function (a) {
                                                        return Tezos_Util.unwrapResult(decode(a));
                                                      }), param);
                                        }), param);
                          }), json);
                    val$2 = Json_decode.optional((function (param) {
                            return Json_decode.field("annots", (function (param) {
                                          return Json_decode.list(Json_decode.string, param);
                                        }), param);
                          }), json);
                    exit$3 = 5;
                  }
                  catch (raw_exn$4){
                    var exn$4 = Caml_js_exceptions.internalToOCamlException(raw_exn$4);
                    if (exn$4[0] === Json_decode.DecodeError) {
                      return /* Error */Block.__(1, [exn$4[1]]);
                    } else {
                      throw exn$4;
                    }
                  }
                  if (exit$3 === 5) {
                    return /* Ok */Block.__(0, [/* ContractExpression */Block.__(4, [
                                  val,
                                  val$1,
                                  val$2
                                ])]);
                  }
                  
                } else {
                  throw exn$3;
                }
              }
              if (exit$2 === 4) {
                return /* Ok */Block.__(0, [/* BytesExpression */Block.__(2, [v$3])]);
              }
              
            } else {
              throw exn$2;
            }
          }
          if (exit$1 === 3) {
            return /* Ok */Block.__(0, [/* StringExpression */Block.__(1, [v$2])]);
          }
          
        } else {
          throw exn$1;
        }
      }
      if (exit === 2) {
        var match = int64OfStringOpt(v$1);
        if (match !== undefined) {
          return /* Ok */Block.__(0, [/* IntExpression */Block.__(0, [match])]);
        } else {
          return /* Error */Block.__(1, ["Expected string encoded int."]);
        }
      }
      
    } else {
      throw exn;
    }
  }
  return /* Ok */Block.__(0, [/* CodeExpression */Block.__(3, [v])]);
}

exports.encode = encode;
exports.intOfStringOpt = intOfStringOpt;
exports.int64OfStringOpt = int64OfStringOpt;
exports.decode = decode;
/* No side effect */
