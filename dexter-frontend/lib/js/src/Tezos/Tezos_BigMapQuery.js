'use strict';

var Block = require("bs-platform/lib/js/block.js");
var Tezos_Util = require("./Tezos_Util.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.bs.js");
var Json_encode = require("@glennsl/bs-json/lib/js/src/Json_encode.bs.js");
var Tezos_Expression = require("./Tezos_Expression.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function encode(query) {
  return Json_encode.object_(/* :: */[
              /* tuple */[
                "key",
                Tezos_Expression.encode(query[/* key */0])
              ],
              /* :: */[
                /* tuple */[
                  "type",
                  Tezos_Expression.encode(query[/* type_ */1])
                ],
                /* [] */0
              ]
            ]);
}

function decode(json) {
  var v;
  try {
    v = /* record */[
      /* key */Tezos_Util.unwrapResult(Json_decode.field("key", Tezos_Expression.decode, json)),
      /* type_ */Tezos_Util.unwrapResult(Json_decode.field("type", Tezos_Expression.decode, json))
    ];
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn[0] === Json_decode.DecodeError) {
      return /* Error */Block.__(1, [exn[1]]);
    } else {
      throw exn;
    }
  }
  return /* Ok */Block.__(0, [v]);
}

exports.encode = encode;
exports.decode = decode;
/* No side effect */
