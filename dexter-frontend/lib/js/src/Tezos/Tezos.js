'use strict';


var Timestamp = 0;

var Address = 0;

var BigMapId = 0;

var Code = 0;

var ContractId = 0;

var Expression = 0;

var Mutez = 0;

var PrimitiveInstruction = 0;

var PrimitiveData = 0;

var PrimitiveType = 0;

var Primitives = 0;

var RPC = 0;

var Token = 0;

exports.Timestamp = Timestamp;
exports.Address = Address;
exports.BigMapId = BigMapId;
exports.Code = Code;
exports.ContractId = ContractId;
exports.Expression = Expression;
exports.Mutez = Mutez;
exports.PrimitiveInstruction = PrimitiveInstruction;
exports.PrimitiveData = PrimitiveData;
exports.PrimitiveType = PrimitiveType;
exports.Primitives = Primitives;
exports.RPC = RPC;
exports.Token = Token;
/* No side effect */
