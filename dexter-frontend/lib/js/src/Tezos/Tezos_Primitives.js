'use strict';

var Block = require("bs-platform/lib/js/block.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.bs.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");
var Tezos_PrimitiveData = require("./Tezos_PrimitiveData.js");
var Tezos_PrimitiveType = require("./Tezos_PrimitiveType.js");
var Tezos_PrimitiveInstruction = require("./Tezos_PrimitiveInstruction.js");

function encode(primitives) {
  switch (primitives.tag | 0) {
    case /* PrimitiveInstruction */0 :
        return Tezos_PrimitiveInstruction.encode(primitives[0]);
    case /* PrimitiveData */1 :
        return Tezos_PrimitiveData.encode(primitives[0]);
    case /* PrimitiveType */2 :
        return Tezos_PrimitiveType.encode(primitives[0]);
    
  }
}

function decode(json) {
  var exit = 0;
  var val;
  try {
    val = Tezos_PrimitiveInstruction.decode(json);
    exit = 1;
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn[0] === Json_decode.DecodeError) {
      return /* Error */Block.__(1, ["Primitives.decode: " + exn[1]]);
    } else {
      throw exn;
    }
  }
  if (exit === 1) {
    if (val.tag) {
      var exit$1 = 0;
      var val$1;
      try {
        val$1 = Tezos_PrimitiveData.decode(json);
        exit$1 = 2;
      }
      catch (raw_exn$1){
        var exn$1 = Caml_js_exceptions.internalToOCamlException(raw_exn$1);
        if (exn$1[0] === Json_decode.DecodeError) {
          return /* Error */Block.__(1, ["Primitives.decode: " + exn$1[1]]);
        } else {
          throw exn$1;
        }
      }
      if (exit$1 === 2) {
        if (val$1.tag) {
          var val$2;
          try {
            val$2 = Tezos_PrimitiveType.decode(json);
          }
          catch (raw_exn$2){
            var exn$2 = Caml_js_exceptions.internalToOCamlException(raw_exn$2);
            if (exn$2[0] === Json_decode.DecodeError) {
              return /* Error */Block.__(1, ["Primitives.decode: " + exn$2[1]]);
            } else {
              throw exn$2;
            }
          }
          if (val$2.tag) {
            return /* Error */Block.__(1, ["Primitives.decode: " + val$2[0]]);
          } else {
            return /* Ok */Block.__(0, [/* PrimitiveType */Block.__(2, [val$2[0]])]);
          }
        } else {
          return /* Ok */Block.__(0, [/* PrimitiveData */Block.__(1, [val$1[0]])]);
        }
      }
      
    } else {
      return /* Ok */Block.__(0, [/* PrimitiveInstruction */Block.__(0, [val[0]])]);
    }
  }
  
}

exports.encode = encode;
exports.decode = decode;
/* No side effect */
