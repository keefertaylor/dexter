'use strict';

var Belt_Id = require("bs-platform/lib/js/belt_Id.js");
var Caml_primitive = require("bs-platform/lib/js/caml_primitive.js");

function ofInt(i) {
  return /* BigMapId */[i];
}

function cmp(c0, c1) {
  return Caml_primitive.caml_int_compare(c0[0], c1[0]);
}

var Comparable = Belt_Id.MakeComparable({
      cmp: cmp
    });

exports.ofInt = ofInt;
exports.Comparable = Comparable;
/* Comparable Not a pure module */
