'use strict';

var Block = require("bs-platform/lib/js/block.js");
var Curry = require("bs-platform/lib/js/curry.js");
var Belt_Id = require("bs-platform/lib/js/belt_Id.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.bs.js");
var Caml_primitive = require("bs-platform/lib/js/caml_primitive.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function encode(t) {
  return t[0];
}

function decode(json) {
  var v;
  try {
    v = Json_decode.string(json);
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn[0] === Json_decode.DecodeError) {
      return /* Error */Block.__(1, ["Address.decode failed: " + exn[1]]);
    } else {
      throw exn;
    }
  }
  return /* Ok */Block.__(0, [/* Address */[v]]);
}

function ofString(candidate) {
  if (candidate.startsWith("tz1")) {
    return /* Ok */Block.__(0, [/* Address */[candidate]]);
  } else {
    return /* Error */Block.__(1, ["Address.mk: unexpected candidate string: " + candidate]);
  }
}

function toString(t) {
  return t[0];
}

var jsPack = (
   function (input) {
     // ed25519_public_key_hash
     const bs58check = require('bs58check');
     const elliptic  = require('elliptic');
     const prefix    = new Uint8Array([6, 161, 159]);
     const bytes     = '0000' + elliptic.utils.toHex(bs58check.decode(input).slice(prefix.length));
     const len = bytes.length / 2;
     const result = [];
     result.push('050a');
     result.push(len.toString(16).padStart(8, '0'));
     result.push(bytes);
     return result.join('');
   }
);

function pack(t) {
  return Curry._1(jsPack, t[0]);
}

var jsToScriptExpr = (
   function (input) {
     const blake    = require('blakejs');
     const elliptic = require('elliptic');
     const bs58check = require('bs58check');

     const prefix2 = elliptic.utils.toHex(new Uint8Array([13, 44, 64, 27]));
     const prefix = new Uint8Array([13, 44, 64, 27]);

     var a = [];
     for (var i = 0, len = input.length; i < len; i+=2) {
       a.push(parseInt(input.substr(i,2),16));
     }
  
     const hex2buf =  new Uint8Array(a);

     const blakeHash = blake.blake2b(hex2buf, null, 32);
     
     const payloadAr = typeof blakeHash === 'string' ? Uint8Array.from(Buffer.from(blakeHash, 'hex')) : blakeHash;

     const n = new Uint8Array(prefix.length + payloadAr.length);
     n.set(prefix);
     n.set(payloadAr, prefix.length);

     return bs58check.encode(Buffer.from(n.buffer));     
   }
);

function toScriptExpr(t) {
  return Curry._1(jsToScriptExpr, Curry._1(jsPack, t[0]));
}

function cmp(c0, c1) {
  return Caml_primitive.caml_string_compare(c0[0], c1[0]);
}

var Comparable = Belt_Id.MakeComparable({
      cmp: cmp
    });

exports.encode = encode;
exports.decode = decode;
exports.ofString = ofString;
exports.toString = toString;
exports.jsPack = jsPack;
exports.pack = pack;
exports.jsToScriptExpr = jsToScriptExpr;
exports.toScriptExpr = toScriptExpr;
exports.Comparable = Comparable;
/* jsPack Not a pure module */
