'use strict';

var Block = require("bs-platform/lib/js/block.js");
var Json_decode = require("@glennsl/bs-json/lib/js/src/Json_decode.bs.js");
var Caml_js_exceptions = require("bs-platform/lib/js/caml_js_exceptions.js");

function encode(primitiveType) {
  switch (primitiveType) {
    case /* Timestamp */0 :
        return "timestamp";
    case /* Signature */1 :
        return "signature";
    case /* Set */2 :
        return "set";
    case /* Pair */3 :
        return "pair";
    case /* Bytes */4 :
        return "bytes";
    case /* Address */5 :
        return "address";
    case /* Or */6 :
        return "or";
    case /* List */7 :
        return "list";
    case /* Storage */8 :
        return "storage";
    case /* KeyHash */9 :
        return "key_hash";
    case /* Unit */10 :
        return "unit";
    case /* Option */11 :
        return "option";
    case /* BigMap */12 :
        return "big_map";
    case /* String */13 :
        return "string";
    case /* Mutez */14 :
        return "mutez";
    case /* Bool */15 :
        return "bool";
    case /* Operation */16 :
        return "operation";
    case /* Contract */17 :
        return "contract";
    case /* Map */18 :
        return "map";
    case /* Nat */19 :
        return "nat";
    case /* Key */20 :
        return "key";
    case /* Lambda */21 :
        return "lambda";
    case /* Int */22 :
        return "int";
    case /* Parameter */23 :
        return "parameter";
    case /* Code */24 :
        return "code";
    
  }
}

function decode(json) {
  var str;
  try {
    str = Json_decode.string(json);
  }
  catch (raw_exn){
    var exn = Caml_js_exceptions.internalToOCamlException(raw_exn);
    if (exn[0] === Json_decode.DecodeError) {
      return /* Error */Block.__(1, ["PrimitiveType.decode: " + exn[1]]);
    } else {
      throw exn;
    }
  }
  switch (str) {
    case "address" :
        return /* Ok */Block.__(0, [/* Address */5]);
    case "big_map" :
        return /* Ok */Block.__(0, [/* BigMap */12]);
    case "bool" :
        return /* Ok */Block.__(0, [/* Bool */15]);
    case "bytes" :
        return /* Ok */Block.__(0, [/* Bytes */4]);
    case "code" :
        return /* Ok */Block.__(0, [/* Code */24]);
    case "contract" :
        return /* Ok */Block.__(0, [/* Contract */17]);
    case "int" :
        return /* Ok */Block.__(0, [/* Int */22]);
    case "key" :
        return /* Ok */Block.__(0, [/* Key */20]);
    case "key_hash" :
        return /* Ok */Block.__(0, [/* KeyHash */9]);
    case "lambda" :
        return /* Ok */Block.__(0, [/* Lambda */21]);
    case "list" :
        return /* Ok */Block.__(0, [/* List */7]);
    case "map" :
        return /* Ok */Block.__(0, [/* Map */18]);
    case "mutez" :
        return /* Ok */Block.__(0, [/* Mutez */14]);
    case "nat" :
        return /* Ok */Block.__(0, [/* Nat */19]);
    case "operation" :
        return /* Ok */Block.__(0, [/* Operation */16]);
    case "option" :
        return /* Ok */Block.__(0, [/* Option */11]);
    case "or" :
        return /* Ok */Block.__(0, [/* Or */6]);
    case "pair" :
        return /* Ok */Block.__(0, [/* Pair */3]);
    case "parameter" :
        return /* Ok */Block.__(0, [/* Parameter */23]);
    case "set" :
        return /* Ok */Block.__(0, [/* Set */2]);
    case "signature" :
        return /* Ok */Block.__(0, [/* Signature */1]);
    case "storage" :
        return /* Ok */Block.__(0, [/* Storage */8]);
    case "string" :
        return /* Ok */Block.__(0, [/* String */13]);
    case "timestamp" :
        return /* Ok */Block.__(0, [/* Timestamp */0]);
    case "unit" :
        return /* Ok */Block.__(0, [/* Unit */10]);
    default:
      return /* Error */Block.__(1, ["PrimitiveType.decode: " + str]);
  }
}

exports.encode = encode;
exports.decode = decode;
/* No side effect */
