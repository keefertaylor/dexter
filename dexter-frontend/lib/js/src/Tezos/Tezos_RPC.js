'use strict';

var Json = require("@glennsl/bs-json/lib/js/src/Json.bs.js");
var Block = require("bs-platform/lib/js/block.js");
var Fetch = require("bs-fetch/lib/js/src/Fetch.js");
var Caml_option = require("bs-platform/lib/js/caml_option.js");
var Tezos_Mutez = require("./Tezos_Mutez.js");
var Tezos_Expression = require("./Tezos_Expression.js");
var Tezos_BigMapQuery = require("./Tezos_BigMapQuery.js");

function toRoute(baseUrl, route) {
  var tmp;
  switch (route.tag | 0) {
    case /* Balance */0 :
        tmp = "/chains/" + (route[0] + ("/blocks/" + (route[1] + ("/context/contracts/" + (route[2] + "/balance")))));
        break;
    case /* BigMap */1 :
        tmp = "/chains/" + (route[0] + ("/blocks/" + (route[1] + ("/context/contracts/" + (route[2] + "/big_map_get")))));
        break;
    case /* Storage */2 :
        tmp = "/chains/" + (route[0] + ("/blocks/" + (route[1] + ("/context/contracts/" + (route[2] + "/storage")))));
        break;
    
  }
  return baseUrl + tmp;
}

function getRequest(url, headers) {
  return fetch(url, Fetch.RequestInit.make(/* Get */0, Caml_option.some(headers), undefined, undefined, undefined, undefined, /* Include */2, undefined, undefined, undefined, undefined)(/* () */0)).then((function (prim) {
                    return prim.json();
                  })).then((function (json) {
                  return Promise.resolve(/* Ok */Block.__(0, [json]));
                })).catch((function (e) {
                return Promise.resolve(/* Error */Block.__(1, [e]));
              }));
}

function postRequest(url, headers, body) {
  return fetch(url, Fetch.RequestInit.make(/* Post */2, Caml_option.some(headers), Caml_option.some(body), undefined, undefined, undefined, /* Include */2, undefined, undefined, undefined, undefined)(/* () */0)).then((function (prim) {
                    return prim.json();
                  })).then((function (json) {
                  return Promise.resolve(/* Ok */Block.__(0, [json]));
                })).catch((function (e) {
                return Promise.resolve(/* Error */Block.__(1, [e]));
              }));
}

function getBalance(baseUrl, chainId, blockId, contractId) {
  var url = toRoute(baseUrl, /* Balance */Block.__(0, [
          chainId,
          blockId,
          contractId
        ]));
  return fetch(url, Fetch.RequestInit.make(/* Get */0, /* array */[/* tuple */[
                            "Accept",
                            "application/json"
                          ]], undefined, undefined, undefined, undefined, /* Omit */0, undefined, undefined, undefined, undefined)(/* () */0)).then((function (prim) {
                    return prim.json();
                  })).then((function (json) {
                  return Promise.resolve(Tezos_Mutez.decode(json));
                })).catch((function (e) {
                console.log(e);
                return Promise.resolve(/* Error */Block.__(1, [""]));
              }));
}

function getBigMapValue(baseUrl, chainId, blockId, contractId, query) {
  var url = toRoute(baseUrl, /* BigMap */Block.__(1, [
          chainId,
          blockId,
          contractId
        ]));
  var body = Json.stringify(Tezos_BigMapQuery.encode(query));
  return fetch(url, Fetch.RequestInit.make(/* Post */2, /* array */[/* tuple */[
                            "Accept",
                            "application/json"
                          ]], Caml_option.some(body), undefined, undefined, /* CORS */3, /* Omit */0, undefined, undefined, undefined, undefined)(/* () */0)).then((function (prim) {
                    return prim.json();
                  })).then((function (json) {
                  return Promise.resolve(Tezos_Expression.decode(json));
                })).catch((function (e) {
                console.log(e);
                return Promise.resolve(/* Error */Block.__(1, [""]));
              }));
}

function getStorage(baseUrl, chainId, blockId, contractId) {
  var url = toRoute(baseUrl, /* Storage */Block.__(2, [
          chainId,
          blockId,
          contractId
        ]));
  return fetch(url, Fetch.RequestInit.make(/* Get */0, /* array */[/* tuple */[
                            "Accept",
                            "application/json"
                          ]], undefined, undefined, undefined, undefined, /* Omit */0, undefined, undefined, undefined, undefined)(/* () */0)).then((function (prim) {
                    return prim.json();
                  })).then((function (json) {
                  return Promise.resolve(Tezos_Expression.decode(json));
                })).catch((function (e) {
                console.log(e);
                return Promise.resolve(/* Error */Block.__(1, [""]));
              }));
}

exports.toRoute = toRoute;
exports.getRequest = getRequest;
exports.postRequest = postRequest;
exports.getBalance = getBalance;
exports.getBigMapValue = getBigMapValue;
exports.getStorage = getStorage;
/* No side effect */
