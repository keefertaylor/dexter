'use strict';

var Block = require("bs-platform/lib/js/block.js");
var TezBridge = require("../TezBridge/TezBridge.js");
var Tezos_Mutez = require("../Tezos/Tezos_Mutez.js");
var Tezos_Token = require("../Tezos/Tezos_Token.js");
var Tezos_Timestamp = require("../Tezos/Tezos_Timestamp.js");

function encodeAddLiquidity(minLiquidityMinted, maxTokensDeposited, deadline) {
  return /* ContractExpression */Block.__(4, [
            /* PrimitiveData */Block.__(1, [/* Left */6]),
            /* :: */[
              /* ContractExpression */Block.__(4, [
                  /* PrimitiveData */Block.__(1, [/* Left */6]),
                  /* :: */[
                    /* ContractExpression */Block.__(4, [
                        /* PrimitiveData */Block.__(1, [/* Pair */8]),
                        /* :: */[
                          /* IntExpression */Block.__(0, [Tezos_Token.toInt64(minLiquidityMinted)]),
                          /* :: */[
                            /* ContractExpression */Block.__(4, [
                                /* PrimitiveData */Block.__(1, [/* Pair */8]),
                                /* :: */[
                                  /* IntExpression */Block.__(0, [Tezos_Token.toInt64(maxTokensDeposited)]),
                                  /* :: */[
                                    /* StringExpression */Block.__(1, [Tezos_Timestamp.toString(deadline)]),
                                    /* [] */0
                                  ]
                                ],
                                undefined
                              ]),
                            /* [] */0
                          ]
                        ],
                        undefined
                      ]),
                    /* [] */0
                  ],
                  undefined
                ]),
              /* [] */0
            ],
            undefined
          ]);
}

function encodeRemoveLiquidity(liquidityBurned, minMutezWithdrawn, minTokenWithdrawn, deadline) {
  return /* ContractExpression */Block.__(4, [
            /* PrimitiveData */Block.__(1, [/* Left */6]),
            /* :: */[
              /* ContractExpression */Block.__(4, [
                  /* PrimitiveData */Block.__(1, [/* Right */1]),
                  /* :: */[
                    /* ContractExpression */Block.__(4, [
                        /* PrimitiveData */Block.__(1, [/* Pair */8]),
                        /* :: */[
                          /* ContractExpression */Block.__(4, [
                              /* PrimitiveData */Block.__(1, [/* Pair */8]),
                              /* :: */[
                                /* IntExpression */Block.__(0, [Tezos_Token.toInt64(liquidityBurned)]),
                                /* :: */[
                                  /* IntExpression */Block.__(0, [Tezos_Mutez.toInt64(minMutezWithdrawn)]),
                                  /* [] */0
                                ]
                              ],
                              undefined
                            ]),
                          /* :: */[
                            /* ContractExpression */Block.__(4, [
                                /* PrimitiveData */Block.__(1, [/* Pair */8]),
                                /* :: */[
                                  /* IntExpression */Block.__(0, [Tezos_Token.toInt64(minTokenWithdrawn)]),
                                  /* :: */[
                                    /* StringExpression */Block.__(1, [Tezos_Timestamp.toString(deadline)]),
                                    /* [] */0
                                  ]
                                ],
                                undefined
                              ]),
                            /* [] */0
                          ]
                        ],
                        undefined
                      ]),
                    /* [] */0
                  ],
                  undefined
                ]),
              /* [] */0
            ],
            undefined
          ]);
}

function encodeMutezToTokens(minTokensRequired, deadline) {
  return /* ContractExpression */Block.__(4, [
            /* PrimitiveData */Block.__(1, [/* Right */1]),
            /* :: */[
              /* ContractExpression */Block.__(4, [
                  /* PrimitiveData */Block.__(1, [/* Left */6]),
                  /* :: */[
                    /* ContractExpression */Block.__(4, [
                        /* PrimitiveData */Block.__(1, [/* Pair */8]),
                        /* :: */[
                          /* IntExpression */Block.__(0, [Tezos_Token.toInt64(minTokensRequired)]),
                          /* :: */[
                            /* StringExpression */Block.__(1, [Tezos_Timestamp.toString(deadline)]),
                            /* [] */0
                          ]
                        ],
                        undefined
                      ]),
                    /* [] */0
                  ],
                  undefined
                ]),
              /* [] */0
            ],
            undefined
          ]);
}

function encodeTokensToMutez(tokensSold, minMutezRequired, deadline) {
  return /* ContractExpression */Block.__(4, [
            /* PrimitiveData */Block.__(1, [/* Right */1]),
            /* :: */[
              /* ContractExpression */Block.__(4, [
                  /* PrimitiveData */Block.__(1, [/* Right */1]),
                  /* :: */[
                    /* ContractExpression */Block.__(4, [
                        /* PrimitiveData */Block.__(1, [/* Left */6]),
                        /* :: */[
                          /* ContractExpression */Block.__(4, [
                              /* PrimitiveData */Block.__(1, [/* Pair */8]),
                              /* :: */[
                                /* IntExpression */Block.__(0, [Tezos_Token.toInt64(tokensSold)]),
                                /* :: */[
                                  /* ContractExpression */Block.__(4, [
                                      /* PrimitiveData */Block.__(1, [/* Pair */8]),
                                      /* :: */[
                                        /* IntExpression */Block.__(0, [Tezos_Mutez.toInt64(minMutezRequired)]),
                                        /* :: */[
                                          /* StringExpression */Block.__(1, [Tezos_Timestamp.toString(deadline)]),
                                          /* [] */0
                                        ]
                                      ],
                                      undefined
                                    ]),
                                  /* [] */0
                                ]
                              ],
                              undefined
                            ]),
                          /* [] */0
                        ],
                        undefined
                      ]),
                    /* [] */0
                  ],
                  undefined
                ]),
              /* [] */0
            ],
            undefined
          ]);
}

function addLiquidity(contract, mutez, minLiquidityMinted, maxTokensDeposited, deadline) {
  return TezBridge.postTransaction(contract, mutez, encodeAddLiquidity(minLiquidityMinted, maxTokensDeposited, deadline));
}

function removeLiquidity(contract, liquidityBurned, minMutezWithdrawn, minTokenWithdrawn, deadline) {
  return TezBridge.postTransaction(contract, Tezos_Mutez.zero, encodeRemoveLiquidity(liquidityBurned, minMutezWithdrawn, minTokenWithdrawn, deadline));
}

function mutezToTokens(contract, mutez, minTokensRequired, deadline) {
  return TezBridge.postTransaction(contract, mutez, encodeMutezToTokens(minTokensRequired, deadline));
}

function tokensToMutez(contract, tokensSold, minMutezRequired, deadline) {
  return TezBridge.postTransaction(contract, Tezos_Mutez.zero, encodeTokensToMutez(tokensSold, minMutezRequired, deadline));
}

exports.encodeAddLiquidity = encodeAddLiquidity;
exports.encodeRemoveLiquidity = encodeRemoveLiquidity;
exports.encodeMutezToTokens = encodeMutezToTokens;
exports.encodeTokensToMutez = encodeTokensToMutez;
exports.addLiquidity = addLiquidity;
exports.removeLiquidity = removeLiquidity;
exports.mutezToTokens = mutezToTokens;
exports.tokensToMutez = tokensToMutez;
/* TezBridge Not a pure module */
