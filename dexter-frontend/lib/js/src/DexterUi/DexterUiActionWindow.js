'use strict';

var Css = require("bs-css/lib/js/src/Css.js");
var React = require("react");
var DexterUiSwap = require("./DexterUiSwap.js");
var DexterUiReceive = require("./DexterUiReceive.js");
var DexterUiMainNavBar = require("./DexterUiMainNavBar.js");
var DexterUiAddLiquidity = require("./DexterUiAddLiquidity.js");
var DexterUiRemoveLiquidity = require("./DexterUiRemoveLiquidity.js");

var mainContainer = Css.style(/* :: */[
      Css.paddingLeft(/* `px */[
            25096,
            80
          ]),
      /* :: */[
        Css.paddingRight(/* `px */[
              25096,
              80
            ]),
        /* [] */0
      ]
    ]);

var actionContainer = Css.style(/* :: */[
      Css.backgroundColor(Css.white),
      /* :: */[
        Css.paddingTop(/* `px */[
              25096,
              80
            ]),
        /* :: */[
          Css.paddingBottom(/* `px */[
                25096,
                50
              ]),
          /* :: */[
            Css.borderRadius(/* `px */[
                  25096,
                  5
                ]),
            /* [] */0
          ]
        ]
      ]
    ]);

var Style = {
  mainContainer: mainContainer,
  actionContainer: actionContainer
};

function DexterUiActionWindow(Props) {
  var address = Props.address;
  var action = Props.action;
  var activeBaker = Props.activeBaker;
  var onChange = Props.onChange;
  var onBakerChange = Props.onBakerChange;
  var bakers = Props.bakers;
  var tokenAmounts = Props.tokenAmounts;
  var leftToken = Props.leftToken;
  var rightToken = Props.rightToken;
  var onLeftTokenChange = Props.onLeftTokenChange;
  var onRightTokenChange = Props.onRightTokenChange;
  var onSwapLeftAndRight = Props.onSwapLeftAndRight;
  var body;
  switch (action) {
    case /* Swap */0 :
        body = React.createElement(DexterUiSwap.make, {
              address: address,
              tokenAmounts: tokenAmounts,
              leftToken: leftToken,
              rightToken: rightToken,
              onLeftTokenChange: onLeftTokenChange,
              onRightTokenChange: onRightTokenChange,
              onSwapLeftAndRight: onSwapLeftAndRight
            });
        break;
    case /* AddLiquidity */1 :
        body = React.createElement(DexterUiAddLiquidity.make, {
              activeBaker: activeBaker,
              address: address,
              bakers: bakers,
              onBakerChange: onBakerChange,
              tokenAmounts: tokenAmounts,
              leftToken: leftToken,
              rightToken: rightToken,
              onLeftTokenChange: onLeftTokenChange,
              onRightTokenChange: onRightTokenChange,
              onSwapLeftAndRight: onSwapLeftAndRight
            });
        break;
    case /* RemoveLiquidity */2 :
        body = React.createElement(DexterUiRemoveLiquidity.make, {
              address: address,
              tokenAmounts: tokenAmounts,
              leftToken: leftToken,
              rightToken: rightToken,
              onLeftTokenChange: onLeftTokenChange,
              onRightTokenChange: onRightTokenChange,
              onSwapLeftAndRight: onSwapLeftAndRight
            });
        break;
    case /* Receive */3 :
        body = React.createElement(DexterUiReceive.make, {
              address: address
            });
        break;
    
  }
  return React.createElement("div", {
              className: mainContainer
            }, React.createElement(DexterUiMainNavBar.make, {
                  action: action,
                  onChange: onChange
                }), React.createElement("div", {
                  className: actionContainer
                }, body));
}

var make = DexterUiActionWindow;

exports.Style = Style;
exports.make = make;
/* mainContainer Not a pure module */
