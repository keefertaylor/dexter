'use strict';

var Css = require("bs-css/lib/js/src/Css.js");
var React = require("react");
var DexterUiColor = require("./DexterUiColor.js");
var DexterUiExchange = require("./DexterUiExchange.js");

var row = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* [] */0
    ]);

var column = Css.style(/* :: */[
      Css.flex(/* `num */[
            5496390,
            50.0
          ]),
      /* [] */0
    ]);

var columnSide = Css.style(/* :: */[
      Css.flex(/* `num */[
            5496390,
            40.0
          ]),
      /* [] */0
    ]);

var columnCenter = Css.style(/* :: */[
      Css.flex(/* `num */[
            5496390,
            20.0
          ]),
      /* [] */0
    ]);

var hcenter = Css.style(/* :: */[
      Css.textAlign(/* center */98248149),
      /* :: */[
        Css.marginTop(Css.px(10)),
        /* :: */[
          Css.fontSize(/* `px */[
                25096,
                14
              ]),
          /* [] */0
        ]
      ]
    ]);

var black = Css.color(Css.hex("082344"));

var grey = Css.color(Css.hex("758DA6"));

var blue = Css.color(Css.hex("0258FF"));

var action = Css.style(/* :: */[
      grey,
      /* :: */[
        Css.fontSize(Css.px(30)),
        /* :: */[
          Css.fontFamily("Source Sans Pro, Semibold"),
          /* :: */[
            Css.letterSpacing(/* `px */[
                  25096,
                  1
                ]),
            /* [] */0
          ]
        ]
      ]
    ]);

var space = Css.style(/* :: */[
      Css.marginTop(/* `px */[
            25096,
            10
          ]),
      /* [] */0
    ]);

var balanceGroup = Css.style(/* :: */[
      Css.marginTop(/* `px */[
            25096,
            24
          ]),
      /* [] */0
    ]);

var balanceToken = Css.style(/* :: */[
      Css.display(/* table */182695950),
      /* [] */0
    ]);

var token = Css.style(/* :: */[
      Css.width(/* `px */[
            25096,
            48
          ]),
      /* :: */[
        Css.height(/* `px */[
              25096,
              48
            ]),
        /* [] */0
      ]
    ]);

var tokenText = Css.style(/* :: */[
      black,
      /* :: */[
        Css.fontSize(/* `px */[
              25096,
              42
            ]),
        /* :: */[
          Css.fontFamily("Source Sans Pro, Semibold"),
          /* :: */[
            Css.display(/* tableCell */793912528),
            /* :: */[
              Css.paddingLeft(/* `px */[
                    25096,
                    16
                  ]),
              /* :: */[
                Css.verticalAlign(/* middle */-866200747),
                /* [] */0
              ]
            ]
          ]
        ]
      ]
    ]);

var balance = Css.style(/* :: */[
      grey,
      /* :: */[
        Css.fontSize(Css.px(16)),
        /* :: */[
          Css.fontFamily("Source Sans Pro, Regular"),
          /* [] */0
        ]
      ]
    ]);

var amount = Css.style(/* :: */[
      Css.fontSize(Css.px(18)),
      /* :: */[
        grey,
        /* [] */0
      ]
    ]);

var amountBox = Css.style(/* :: */[
      Css.unsafe("borderRadius", "10px"),
      /* :: */[
        Css.borderColor(Css.hex("C4D0DC")),
        /* :: */[
          Css.borderStyle(Css.solid),
          /* :: */[
            Css.borderWidth(Css.px(1)),
            /* :: */[
              Css.padding2(Css.px(10), Css.px(20)),
              /* :: */[
                Css.marginTop(Css.px(5)),
                /* :: */[
                  Css.marginBottom(Css.px(50)),
                  /* :: */[
                    Css.marginRight(Css.px(100)),
                    /* [] */0
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var verticalBar = Css.style(/* :: */[
      Css.width(/* `px */[
            25096,
            0
          ]),
      /* :: */[
        Css.height(/* `px */[
              25096,
              96
            ]),
        /* :: */[
          Css.borderWidth(/* `px */[
                25096,
                1
              ]),
          /* :: */[
            Css.borderStyle(/* solid */12956715),
            /* :: */[
              Css.borderColor(DexterUiColor.lightGrey),
              /* :: */[
                Css.marginTop(/* `px */[
                      25096,
                      38
                    ]),
                /* :: */[
                  Css.marginBottom(/* `px */[
                        25096,
                        38
                      ]),
                  /* [] */0
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var cc = Css.style(/* :: */[
      Css.width(/* `percent */[
            -119887163,
            100.0
          ]),
      /* [] */0
    ]);

var dd = Css.style(/* :: */[
      Css.display(/* table */182695950),
      /* :: */[
        Css.margin(/* auto */-1065951377),
        /* [] */0
      ]
    ]);

var circle = Css.style(/* :: */[
      Css.backgroundColor(DexterUiColor.blue),
      /* :: */[
        Css.backgroundRepeat(/* noRepeat */-695430532),
        /* :: */[
          Css.backgroundOrigin(/* paddingBox */972575930),
          /* :: */[
            Css.borderRadius(/* `percent */[
                  -119887163,
                  50.0
                ]),
            /* :: */[
              Css.width(/* `px */[
                    25096,
                    48
                  ]),
              /* :: */[
                Css.height(/* `px */[
                      25096,
                      48
                    ]),
                /* :: */[
                  Css.boxShadow(Css.Shadow.box(Css.px(0), Css.px(0), Css.px(12), undefined, undefined, Css.hex("0A28642E"))),
                  /* :: */[
                    Css.position(/* relative */903134412),
                    /* :: */[
                      Css.marginLeft(/* `percent */[
                            -119887163,
                            -50.0
                          ]),
                      /* [] */0
                    ]
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var circleImg = Css.style(/* :: */[
      Css.width(/* `px */[
            25096,
            21
          ]),
      /* :: */[
        Css.height(/* `px */[
              25096,
              25
            ]),
        /* :: */[
          Css.position(/* absolute */-1013592457),
          /* :: */[
            Css.margin(/* auto */-1065951377),
            /* :: */[
              Css.top(/* `px */[
                    25096,
                    0
                  ]),
              /* :: */[
                Css.left(/* `px */[
                      25096,
                      0
                    ]),
                /* :: */[
                  Css.right(/* `px */[
                        25096,
                        0
                      ]),
                  /* :: */[
                    Css.bottom(/* `px */[
                          25096,
                          0
                        ]),
                    /* [] */0
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var profits = Css.style(/* :: */[
      Css.width(/* `px */[
            25096,
            16
          ]),
      /* :: */[
        Css.height(/* `px */[
              25096,
              10
            ]),
        /* [] */0
      ]
    ]);

var profitsText = Css.style(/* :: */[
      Css.marginLeft(/* `px */[
            25096,
            8
          ]),
      /* [] */0
    ]);

var tokenAmount = Css.style(/* :: */[
      Css.fontSize(Css.px(42)),
      /* :: */[
        grey,
        /* :: */[
          Css.fontFamily("Source Sans Pro, Regular"),
          /* [] */0
        ]
      ]
    ]);

var usdAmount = Css.style(/* :: */[
      Css.fontSize(Css.px(18)),
      /* :: */[
        grey,
        /* :: */[
          Css.fontFamily("Source Sans Pro, Semibold"),
          /* [] */0
        ]
      ]
    ]);

var exchangeRateBox = Css.style(/* :: */[
      Css.borderWidth(/* `px */[
            25096,
            1
          ]),
      /* :: */[
        Css.borderStyle(/* solid */12956715),
        /* :: */[
          Css.borderColor(Css.hex("C4D0DC")),
          /* :: */[
            Css.borderRadius(/* `px */[
                  25096,
                  17
                ]),
            /* [] */0
          ]
        ]
      ]
    ]);

var exchangeRate = Css.style(/* :: */[
      Css.textAlign(/* center */98248149),
      /* :: */[
        grey,
        /* :: */[
          Css.fontSize(/* `px */[
                25096,
                16
              ]),
          /* [] */0
        ]
      ]
    ]);

var destinationContainer = Css.style(/* :: */[
      Css.paddingLeft(/* `px */[
            25096,
            47
          ]),
      /* [] */0
    ]);

var destination = Css.style(/* :: */[
      Css.fontSize(/* `px */[
            25096,
            18
          ]),
      /* :: */[
        Css.marginTop(/* `px */[
              25096,
              10
            ]),
        /* :: */[
          grey,
          /* [] */0
        ]
      ]
    ]);

var destinationInternal = Css.style(/* :: */[
      Css.display(/* table */182695950),
      /* [] */0
    ]);

var informationBox = Css.style(/* :: */[
      Css.backgroundColor(Css.hex("0258FF0D")),
      /* :: */[
        Css.backgroundRepeat(/* noRepeat */-695430532),
        /* :: */[
          Css.backgroundOrigin(/* paddingBox */972575930),
          /* :: */[
            Css.borderColor(Css.hex("0258FF")),
            /* :: */[
              Css.borderRadius(/* `px */[
                    25096,
                    5
                  ]),
              /* :: */[
                Css.borderStyle(/* solid */12956715),
                /* :: */[
                  Css.padding2(Css.px(20), Css.px(20)),
                  /* [] */0
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var informationText = Css.style(/* :: */[
      Css.color(Css.hex("0258FF")),
      /* :: */[
        Css.fontFamily("Source Sans Pro, Regular"),
        /* :: */[
          Css.fontSize(/* `px */[
                25096,
                16
              ]),
          /* [] */0
        ]
      ]
    ]);

var center = Css.style(/* :: */[
      Css.display(Css.table),
      /* :: */[
        Css.margin2(Css.px(75), Css.auto),
        /* [] */0
      ]
    ]);

var container = Css.style(/* :: */[
      Css.textAlign(/* center */98248149),
      /* :: */[
        Css.display(/* inlineBlock */-147785676),
        /* [] */0
      ]
    ]);

var slippageMessage = Css.style(/* :: */[
      grey,
      /* :: */[
        Css.fontSize(/* `px */[
              25096,
              16
            ]),
        /* [] */0
      ]
    ]);

var slippageChange = Css.style(/* :: */[
      blue,
      /* :: */[
        Css.fontSize(/* `px */[
              25096,
              16
            ]),
        /* [] */0
      ]
    ]);

var blue$1 = Css.hex("0258FF");

var lightBlue = Css.hex("A3BDFB");

function swapButton(address) {
  return Css.style(/* :: */[
              address !== undefined ? Css.backgroundColor(blue$1) : Css.backgroundColor(lightBlue),
              /* :: */[
                address !== undefined ? Css.cursor(/* pointer */-786317123) : Css.cursor(/* default */465819841),
                /* :: */[
                  Css.backgroundRepeat(/* noRepeat */-695430532),
                  /* :: */[
                    Css.backgroundOrigin(/* paddingBox */972575930),
                    /* :: */[
                      Css.boxShadow(Css.Shadow.box(Css.px(0), Css.px(12), Css.px(26), undefined, undefined, Css.hex("0A28642E"))),
                      /* :: */[
                        Css.borderRadius(/* `px */[
                              25096,
                              10
                            ]),
                        /* :: */[
                          Css.borderStyle(/* none */-922086728),
                          /* :: */[
                            Css.padding2(Css.px(20), Css.px(20)),
                            /* [] */0
                          ]
                        ]
                      ]
                    ]
                  ]
                ]
              ]
            ]);
}

var swapButtonText = Css.style(/* :: */[
      Css.fontSize(/* `px */[
            25096,
            18
          ]),
      /* :: */[
        Css.color(Css.hex("FFFFFF")),
        /* :: */[
          Css.fontFamily("Source Sans Pro, Semibold"),
          /* :: */[
            Css.textAlign(/* center */98248149),
            /* [] */0
          ]
        ]
      ]
    ]);

var acceptButton = Css.style(/* :: */[
      Css.width(/* `px */[
            25096,
            24
          ]),
      /* :: */[
        Css.height(/* `px */[
              25096,
              24
            ]),
        /* [] */0
      ]
    ]);

var destinationText = Css.style(/* :: */[
      Css.paddingLeft(/* `px */[
            25096,
            8
          ]),
      /* :: */[
        Css.display(/* tableCell */793912528),
        /* :: */[
          Css.verticalAlign(/* middle */-866200747),
          /* [] */0
        ]
      ]
    ]);

var Style = {
  row: row,
  column: column,
  columnSide: columnSide,
  columnCenter: columnCenter,
  hcenter: hcenter,
  black: black,
  grey: grey,
  action: action,
  space: space,
  balanceGroup: balanceGroup,
  balanceToken: balanceToken,
  token: token,
  tokenText: tokenText,
  balance: balance,
  amount: amount,
  amountBox: amountBox,
  verticalBar: verticalBar,
  cc: cc,
  dd: dd,
  circle: circle,
  circleImg: circleImg,
  profits: profits,
  profitsText: profitsText,
  tokenAmount: tokenAmount,
  usdAmount: usdAmount,
  exchangeRateBox: exchangeRateBox,
  exchangeRate: exchangeRate,
  destinationContainer: destinationContainer,
  destination: destination,
  destinationInternal: destinationInternal,
  informationBox: informationBox,
  informationText: informationText,
  center: center,
  container: container,
  slippageMessage: slippageMessage,
  slippageChange: slippageChange,
  blue: blue$1,
  lightBlue: lightBlue,
  swapButton: swapButton,
  swapButtonText: swapButtonText,
  acceptButton: acceptButton,
  destinationText: destinationText
};

var iInCircle = ('\u24D8');

function DexterUiSwap(Props) {
  var address = Props.address;
  var tokenAmounts = Props.tokenAmounts;
  var leftToken = Props.leftToken;
  var rightToken = Props.rightToken;
  var onLeftTokenChange = Props.onLeftTokenChange;
  var onRightTokenChange = Props.onRightTokenChange;
  var onSwapLeftAndRight = Props.onSwapLeftAndRight;
  return React.createElement("div", undefined, React.createElement(DexterUiExchange.make, {
                  action: /* Swap */0,
                  tokenAmounts: tokenAmounts,
                  leftToken: leftToken,
                  rightToken: rightToken,
                  onLeftTokenChange: onLeftTokenChange,
                  onRightTokenChange: onRightTokenChange,
                  onSwapLeftAndRight: onSwapLeftAndRight
                }), React.createElement("div", {
                  className: destinationContainer
                }, React.createElement("div", {
                      className: destination
                    }, React.createElement("div", {
                          className: destinationInternal
                        }, React.createElement("img", {
                              className: acceptButton,
                              src: "accept-circular-button-grey.svg"
                            }), React.createElement("span", {
                              className: destinationText
                            }, "Send " + (rightToken[/* symbol */0] + " to another address"))))), React.createElement("div", {
                  className: center
                }, React.createElement("div", {
                      className: container
                    }, React.createElement("div", {
                          className: informationBox
                        }, React.createElement("span", {
                              className: informationText
                            }, iInCircle, " Swapping allows you to exchange XTZ from your wallet to another token.")))), React.createElement("div", {
                  className: center
                }, React.createElement("div", {
                      className: container
                    }, React.createElement("span", {
                          className: slippageMessage
                        }, "Limit additional price slippage: 1% ", React.createElement("sup", undefined, iInCircle)), React.createElement("span", {
                          className: slippageChange
                        }, " Change"))), React.createElement("div", {
                  className: center
                }, React.createElement("button", {
                      className: swapButton(address)
                    }, React.createElement("span", {
                          className: swapButtonText
                        }, "Swap XTZ to ABC"))));
}

var make = DexterUiSwap;

exports.Style = Style;
exports.iInCircle = iInCircle;
exports.make = make;
/* row Not a pure module */
