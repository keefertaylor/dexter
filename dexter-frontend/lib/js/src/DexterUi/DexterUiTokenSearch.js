'use strict';

var Css = require("bs-css/lib/js/src/Css.js");
var List = require("bs-platform/lib/js/list.js");
var $$Array = require("bs-platform/lib/js/array.js");
var Curry = require("bs-platform/lib/js/curry.js");
var React = require("react");
var DexterUiFont = require("./DexterUiFont.js");
var DexterUiColor = require("./DexterUiColor.js");

var container = Css.style(/* [] */0);

var searchRow = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* :: */[
        Css.flexDirection(/* row */5693978),
        /* :: */[
          Css.paddingTop(/* `px */[
                25096,
                16
              ]),
          /* [] */0
        ]
      ]
    ]);

var input = Css.style(/* :: */[
      Css.selector("input:focus", /* :: */[
            Css.unsafe("border", "none"),
            /* [] */0
          ]),
      /* :: */[
        Css.unsafe("border", "none"),
        /* :: */[
          Css.paddingLeft(/* `px */[
                25096,
                16
              ]),
          /* :: */[
            Css.fontSize(/* `px */[
                  25096,
                  18
                ]),
            /* :: */[
              Css.color(DexterUiColor.grey),
              /* :: */[
                DexterUiFont.defaultFont,
                /* [] */0
              ]
            ]
          ]
        ]
      ]
    ]);

var magnifyingGlassIcon = Css.style(/* :: */[
      Css.paddingLeft(/* `px */[
            25096,
            24
          ]),
      /* :: */[
        Css.width(/* `px */[
              25096,
              24
            ]),
        /* :: */[
          Css.height(/* `px */[
                25096,
                24
              ]),
          /* [] */0
        ]
      ]
    ]);

var tokenIcon = Css.style(/* :: */[
      Css.width(/* `px */[
            25096,
            32
          ]),
      /* :: */[
        Css.height(/* `px */[
              25096,
              32
            ]),
        /* :: */[
          Css.paddingRight(/* `px */[
                25096,
                16
              ]),
          /* :: */[
            Css.margin(/* auto */-1065951377),
            /* [] */0
          ]
        ]
      ]
    ]);

var tokenRow = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* :: */[
        Css.flexDirection(/* row */5693978),
        /* :: */[
          Css.borderStyle(Css.solid),
          /* :: */[
            Css.borderColor(Css.hex("C4D0DC")),
            /* :: */[
              Css.borderTopWidth(/* `px */[
                    25096,
                    1
                  ]),
              /* :: */[
                Css.borderRightWidth(/* `px */[
                      25096,
                      0
                    ]),
                /* :: */[
                  Css.borderBottomWidth(/* `px */[
                        25096,
                        0
                      ]),
                  /* :: */[
                    Css.borderLeftWidth(/* `px */[
                          25096,
                          0
                        ]),
                    /* :: */[
                      Css.cursor(/* pointer */-786317123),
                      /* [] */0
                    ]
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var tokenDetails = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* :: */[
        Css.flexDirection(/* column */-963948842),
        /* :: */[
          Css.width(/* `percent */[
                -119887163,
                100.0
              ]),
          /* [] */0
        ]
      ]
    ]);

var tokenRowTop = Css.style(/* [] */0);

var tokenRowBottom = Css.style(/* [] */0);

var tokenSymbol = Css.style(/* :: */[
      Css.$$float(/* left */-944764921),
      /* :: */[
        Css.color(DexterUiColor.black),
        /* :: */[
          DexterUiFont.boldFont,
          /* :: */[
            Css.fontSize(/* `px */[
                  25096,
                  25
                ]),
            /* [] */0
          ]
        ]
      ]
    ]);

var tokenName = Css.style(/* :: */[
      Css.$$float(/* left */-944764921),
      /* :: */[
        Css.color(DexterUiColor.grey),
        /* :: */[
          DexterUiFont.defaultFont,
          /* :: */[
            Css.fontSize(/* `px */[
                  25096,
                  16
                ]),
            /* [] */0
          ]
        ]
      ]
    ]);

var tokenAmount = Css.style(/* :: */[
      Css.$$float(/* right */-379319332),
      /* :: */[
        Css.color(DexterUiColor.grey),
        /* :: */[
          DexterUiFont.defaultFont,
          /* :: */[
            Css.fontSize(/* `px */[
                  25096,
                  25
                ]),
            /* [] */0
          ]
        ]
      ]
    ]);

var tokenAvailability = Css.style(/* :: */[
      Css.$$float(/* right */-379319332),
      /* :: */[
        Css.color(DexterUiColor.grey),
        /* :: */[
          DexterUiFont.defaultFont,
          /* :: */[
            Css.fontSize(/* `px */[
                  25096,
                  16
                ]),
            /* [] */0
          ]
        ]
      ]
    ]);

var Style = {
  container: container,
  searchRow: searchRow,
  input: input,
  magnifyingGlassIcon: magnifyingGlassIcon,
  tokenIcon: tokenIcon,
  tokenRow: tokenRow,
  tokenDetails: tokenDetails,
  tokenRowTop: tokenRowTop,
  tokenRowBottom: tokenRowBottom,
  tokenSymbol: tokenSymbol,
  tokenName: tokenName,
  tokenAmount: tokenAmount,
  tokenAvailability: tokenAvailability
};

function DexterUiTokenSearch(Props) {
  var tokenAmounts = Props.tokenAmounts;
  var onTokenChange = Props.onTokenChange;
  return React.createElement("div", {
              className: container
            }, React.createElement("div", {
                  className: searchRow
                }, React.createElement("div", undefined, React.createElement("img", {
                          className: magnifyingGlassIcon,
                          src: "magnifying-glass.svg"
                        })), React.createElement("input", {
                      className: input,
                      placeholder: "Search token"
                    })), React.createElement("ul", undefined, $$Array.of_list(List.mapi((function (i, tokenAmount$1) {
                            return React.createElement("li", {
                                        key: String(i),
                                        className: tokenRow,
                                        onClick: (function (param) {
                                            return Curry._1(onTokenChange, tokenAmount$1);
                                          })
                                      }, React.createElement("img", {
                                            className: tokenIcon,
                                            src: "abc.png"
                                          }), React.createElement("div", {
                                            className: tokenDetails
                                          }, React.createElement("div", {
                                                className: tokenRowTop
                                              }, React.createElement("span", {
                                                    className: tokenSymbol
                                                  }, tokenAmount$1[/* symbol */0]), React.createElement("span", {
                                                    className: tokenAmount
                                                  }, String(tokenAmount$1[/* amount */2]))), React.createElement("div", {
                                                className: tokenRowBottom
                                              }, React.createElement("span", {
                                                    className: tokenName
                                                  }, tokenAmount$1[/* name */1]), React.createElement("span", {
                                                    className: tokenAvailability
                                                  }, "Available"))));
                          }), tokenAmounts))));
}

var make = DexterUiTokenSearch;

exports.Style = Style;
exports.make = make;
/* container Not a pure module */
