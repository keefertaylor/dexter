'use strict';

var Css = require("bs-css/lib/js/src/Css.js");
var React = require("react");

var navbar = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* :: */[
        Css.justifyContent(Css.spaceBetween),
        /* :: */[
          Css.paddingTop(Css.px(10)),
          /* [] */0
        ]
      ]
    ]);

var tab = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* :: */[
        Css.flex(/* `num */[
              5496390,
              0.5
            ]),
        /* :: */[
          Css.cursor(/* pointer */-786317123),
          /* :: */[
            Css.justifyContent(/* center */98248149),
            /* :: */[
              Css.fontSize(Css.px(18)),
              /* :: */[
                Css.fontFamily("Source Sans Pro, Semibold"),
                /* :: */[
                  Css.paddingTop(Css.px(10)),
                  /* :: */[
                    Css.paddingBottom(Css.px(15)),
                    /* [] */0
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var active = Css.style(/* :: */[
      Css.unsafe("backgroundColor", "white"),
      /* :: */[
        Css.color(Css.hex("0258FF")),
        /* :: */[
          Css.fontSize(/* `px */[
                25096,
                18
              ]),
          /* :: */[
            Css.borderTopLeftRadius(Css.px(5)),
            /* :: */[
              Css.borderTopRightRadius(Css.px(5)),
              /* [] */0
            ]
          ]
        ]
      ]
    ]);

var inactive = Css.style(/* :: */[
      Css.unsafe("backgroundColor", "inherit"),
      /* [] */0
    ]);

var tabInternal = Css.style(/* :: */[
      Css.display(/* table */182695950),
      /* [] */0
    ]);

var addIcon = Css.style(/* :: */[
      Css.width(/* `px */[
            25096,
            24
          ]),
      /* :: */[
        Css.height(/* `px */[
              25096,
              24
            ]),
        /* [] */0
      ]
    ]);

var tabTitle = Css.style(/* :: */[
      Css.display(/* tableCell */793912528),
      /* :: */[
        Css.verticalAlign(/* middle */-866200747),
        /* :: */[
          Css.paddingLeft(/* `px */[
                25096,
                8
              ]),
          /* [] */0
        ]
      ]
    ]);

var Style = {
  navbar: navbar,
  tab: tab,
  active: active,
  inactive: inactive,
  tabInternal: tabInternal,
  addIcon: addIcon,
  tabTitle: tabTitle
};

function isActive(x, y) {
  if (x === y) {
    return active;
  } else {
    return inactive;
  }
}

function DexterUiThirdWindow(Props) {
  var account = Props.account;
  var accountData = Props.accountData;
  var recentTransactions = Props.recentTransactions;
  var poolTokens = Props.poolTokens;
  console.log(recentTransactions);
  console.log(poolTokens);
  return React.createElement("div", undefined, React.createElement("div", {
                  className: navbar
                }, React.createElement("div", {
                      className: Css.merge(/* :: */[
                            tab,
                            /* :: */[
                              isActive(/* RecentTransactions */0, accountData),
                              /* [] */0
                            ]
                          ])
                    }, React.createElement("div", {
                          className: tabInternal
                        }, React.createElement("img", {
                              className: addIcon
                            }), React.createElement("span", {
                              className: tabTitle
                            }, "Recent transactions"))), React.createElement("div", {
                      className: Css.merge(/* :: */[
                            tab,
                            /* :: */[
                              isActive(/* PoolTokens */1, accountData),
                              /* [] */0
                            ]
                          ])
                    }, React.createElement("div", {
                          className: tabInternal
                        }, React.createElement("img", {
                              className: addIcon
                            }), React.createElement("span", {
                              className: tabTitle
                            }, "Your pool tokens")))), accountData ? React.createElement("div", undefined) : (
                account !== undefined ? React.createElement("div", undefined, "Show the account: " + account) : React.createElement("div", undefined, React.createElement("div", undefined, "There are no transactions yet!"))
              ));
}

var make = DexterUiThirdWindow;

exports.Style = Style;
exports.isActive = isActive;
exports.make = make;
/* navbar Not a pure module */
