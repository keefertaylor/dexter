'use strict';

var Css = require("bs-css/lib/js/src/Css.js");
var React = require("react");
var DexterUiColor = require("./DexterUiColor.js");

var exchangeRateBox = Css.style(/* :: */[
      Css.borderWidth(/* `px */[
            25096,
            1
          ]),
      /* :: */[
        Css.borderStyle(/* solid */12956715),
        /* :: */[
          Css.borderColor(Css.hex("C4D0DC")),
          /* :: */[
            Css.borderRadius(/* `px */[
                  25096,
                  17
                ]),
            /* [] */0
          ]
        ]
      ]
    ]);

var exchangeRate = Css.style(/* :: */[
      Css.textAlign(/* center */98248149),
      /* :: */[
        Css.color(DexterUiColor.grey),
        /* :: */[
          Css.fontSize(/* `px */[
                25096,
                16
              ]),
          /* [] */0
        ]
      ]
    ]);

var profits = Css.style(/* :: */[
      Css.width(/* `px */[
            25096,
            16
          ]),
      /* :: */[
        Css.height(/* `px */[
              25096,
              10
            ]),
        /* [] */0
      ]
    ]);

var profitsText = Css.style(/* :: */[
      Css.marginLeft(/* `px */[
            25096,
            8
          ]),
      /* [] */0
    ]);

var Style = {
  exchangeRateBox: exchangeRateBox,
  exchangeRate: exchangeRate,
  profits: profits,
  profitsText: profitsText
};

function DexterUiExchangeRate(Props) {
  return React.createElement("div", {
              className: exchangeRateBox
            }, React.createElement("div", {
                  className: exchangeRate
                }, React.createElement("img", {
                      className: profits,
                      src: "profits.svg"
                    }), React.createElement("span", {
                      className: profitsText
                    }, "1 XTZ = 2 ABC")));
}

var make = DexterUiExchangeRate;

exports.Style = Style;
exports.make = make;
/* exchangeRateBox Not a pure module */
