'use strict';

var Css = require("bs-css/lib/js/src/Css.js");
var Curry = require("bs-platform/lib/js/curry.js");
var React = require("react");
var TezBridge = require("../TezBridge/TezBridge.js");
var DexterUiFont = require("./DexterUiFont.js");

var container = Css.style(/* :: */[
      Css.width(/* `percent */[
            -119887163,
            100.0
          ]),
      /* :: */[
        Css.borderRadius(/* `px */[
              25096,
              10
            ]),
        /* :: */[
          Css.backgroundColor(Css.hex("FFFFFF")),
          /* :: */[
            Css.backgroundRepeat(/* noRepeat */-695430532),
            /* :: */[
              Css.backgroundOrigin(/* paddingBox */972575930),
              /* :: */[
                Css.boxShadow(Css.Shadow.box(Css.px(0), Css.px(10), Css.px(28), undefined, undefined, Css.hex("0A28640D"))),
                /* [] */0
              ]
            ]
          ]
        ]
      ]
    ]);

var status = Css.style(/* :: */[
      Css.paddingTop(/* `px */[
            25096,
            25
          ]),
      /* :: */[
        Css.color(Css.hex("082344")),
        /* :: */[
          Css.fontSize(/* `px */[
                25096,
                25
              ]),
          /* :: */[
            Css.fontFamily("Source Sans Pro, Semibold"),
            /* :: */[
              Css.textAlign(/* center */98248149),
              /* [] */0
            ]
          ]
        ]
      ]
    ]);

var message = Css.style(/* :: */[
      Css.color(Css.hex("758DA6")),
      /* :: */[
        Css.fontSize(/* `px */[
              25096,
              16
            ]),
        /* :: */[
          Css.fontFamily("Source Sans Pro, Semibold"),
          /* :: */[
            Css.textAlign(/* center */98248149),
            /* [] */0
          ]
        ]
      ]
    ]);

var buttonContainer = Css.style(/* :: */[
      Css.marginTop(/* `px */[
            25096,
            18
          ]),
      /* :: */[
        Css.display(/* flex */-1010954439),
        /* :: */[
          Css.justifyContent(/* center */98248149),
          /* [] */0
        ]
      ]
    ]);

var button = Css.style(/* :: */[
      Css.cursor(/* pointer */-786317123),
      /* :: */[
        Css.backgroundColor(Css.hex("0258FF")),
        /* :: */[
          Css.backgroundRepeat(/* noRepeat */-695430532),
          /* :: */[
            Css.backgroundOrigin(/* paddingBox */972575930),
            /* :: */[
              Css.borderRadius(/* `px */[
                    25096,
                    10
                  ]),
              /* :: */[
                Css.height(/* `px */[
                      25096,
                      48
                    ]),
                /* :: */[
                  Css.marginBottom(/* `px */[
                        25096,
                        32
                      ]),
                  /* :: */[
                    Css.width(/* `px */[
                          25096,
                          200
                        ]),
                    /* [] */0
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var buttonText = Css.style(/* :: */[
      Css.color(Css.hex("FFFFFF")),
      /* :: */[
        Css.textAlign(/* center */98248149),
        /* :: */[
          Css.fontSize(/* `px */[
                25096,
                18
              ]),
          /* :: */[
            Css.fontFamily("Source Sans Pro, Semibold"),
            /* [] */0
          ]
        ]
      ]
    ]);

var addressContainer = Css.style(/* :: */[
      Css.paddingTop(/* `px */[
            25096,
            16
          ]),
      /* :: */[
        Css.paddingLeft(/* `px */[
              25096,
              26
            ]),
        /* :: */[
          Css.marginLeft(/* `px */[
                25096,
                80
              ]),
          /* :: */[
            Css.marginRight(/* `px */[
                  25096,
                  80
                ]),
            /* :: */[
              Css.height(/* `px */[
                    25096,
                    128
                  ]),
              /* :: */[
                Css.width(/* `px */[
                      25096,
                      596
                    ]),
                /* :: */[
                  Css.borderRadius(/* `px */[
                        25096,
                        10
                      ]),
                  /* :: */[
                    Css.backgroundColor(Css.hex("FFFFFF")),
                    /* :: */[
                      Css.backgroundRepeat(/* noRepeat */-695430532),
                      /* :: */[
                        Css.backgroundOrigin(/* paddingBox */972575930),
                        /* :: */[
                          Css.boxShadow(Css.Shadow.box(Css.px(0), Css.px(10), Css.px(28), undefined, undefined, Css.hex("0A28640D"))),
                          /* [] */0
                        ]
                      ]
                    ]
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var addressStatus = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* [] */0
    ]);

var ellipse = Css.style(/* :: */[
      Css.alignItems(/* center */98248149),
      /* :: */[
        Css.height(/* `px */[
              25096,
              8
            ]),
        /* :: */[
          Css.width(/* `px */[
                25096,
                8
              ]),
          /* :: */[
            Css.backgroundColor(Css.hex("38C636")),
            /* :: */[
              Css.backgroundRepeat(/* noRepeat */-695430532),
              /* :: */[
                Css.backgroundOrigin(/* paddingBox */972575930),
                /* :: */[
                  Css.borderRadius(/* `percent */[
                        -119887163,
                        50.0
                      ]),
                  /* [] */0
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var addressOrigin = Css.style(/* :: */[
      Css.alignItems(/* center */98248149),
      /* :: */[
        Css.color(Css.hex("C4D0DC")),
        /* :: */[
          Css.fontFamily("Source Sans Pro, Regular"),
          /* :: */[
            Css.fontSize(/* `px */[
                  25096,
                  16
                ]),
            /* :: */[
              Css.paddingLeft(/* `px */[
                    25096,
                    6
                  ]),
              /* [] */0
            ]
          ]
        ]
      ]
    ]);

var managerName = Css.style(/* :: */[
      DexterUiFont.boldFont,
      /* :: */[
        Css.fontSize(/* `px */[
              25096,
              18
            ]),
        /* [] */0
      ]
    ]);

var address = Css.style(/* :: */[
      DexterUiFont.defaultFont,
      /* :: */[
        Css.fontSize(/* `px */[
              25096,
              16
            ]),
        /* [] */0
      ]
    ]);

var Style = {
  container: container,
  status: status,
  message: message,
  buttonContainer: buttonContainer,
  button: button,
  buttonText: buttonText,
  addressContainer: addressContainer,
  addressStatus: addressStatus,
  ellipse: ellipse,
  addressOrigin: addressOrigin,
  managerName: managerName,
  address: address
};

function DexterUiConnectWallet(Props) {
  var address$1 = Props.address;
  var match = React.useReducer((function (state, action) {
          return /* record */[
                  /* address */action[0],
                  /* zed */state[/* zed */1]
                ];
        }), /* record */[
        /* address */address$1,
        /* zed */""
      ]);
  var dispatch = match[1];
  var match$1 = match[0][/* address */0];
  if (match$1 !== undefined) {
    return React.createElement("div", {
                className: addressContainer
              }, React.createElement("div", {
                    className: addressStatus
                  }, React.createElement("div", {
                        className: ellipse
                      }), React.createElement("div", {
                        className: addressOrigin
                      }, "TezBridge address")), React.createElement("div", {
                    className: managerName
                  }, "Manager Name"), React.createElement("div", {
                    className: address
                  }, match$1));
  } else {
    return React.createElement("div", {
                className: container
              }, React.createElement("div", {
                    className: status
                  }, "No wallet connected!"), React.createElement("div", {
                    className: message
                  }, "Please connect your wallet using the available options."), React.createElement("div", {
                    className: buttonContainer
                  }, React.createElement("button", {
                        className: button,
                        onClick: (function (_event) {
                            Curry._1(TezBridge.getSource, /* () */0).then((function (result) {
                                    Curry._1(dispatch, /* UpdateAddress */[result]);
                                    return Promise.resolve(/* () */0);
                                  }));
                            return /* () */0;
                          })
                      }, React.createElement("span", {
                            className: buttonText
                          }, "Connect wallet"))));
  }
}

var make = DexterUiConnectWallet;

exports.Style = Style;
exports.make = make;
/* container Not a pure module */
