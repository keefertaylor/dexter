'use strict';

var Css = require("bs-css/lib/js/src/Css.js");
var React = require("react");
var DexterUiConnectWallet = require("./DexterUiConnectWallet.js");
var DexterUiSecondaryNavBar = require("./DexterUiSecondaryNavBar.js");

var container = Css.style(/* :: */[
      Css.paddingLeft(/* `px */[
            25096,
            80
          ]),
      /* :: */[
        Css.paddingRight(/* `px */[
              25096,
              80
            ]),
        /* [] */0
      ]
    ]);

var Style = {
  container: container
};

function DexterUiSecondaryWindow(Props) {
  var address = Props.address;
  return React.createElement("div", {
              className: container
            }, React.createElement(DexterUiSecondaryNavBar.make, { }), React.createElement(DexterUiConnectWallet.make, {
                  address: address
                }));
}

var make = DexterUiSecondaryWindow;

exports.Style = Style;
exports.make = make;
/* container Not a pure module */
