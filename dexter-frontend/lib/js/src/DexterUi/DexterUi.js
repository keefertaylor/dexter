'use strict';

var Css = require("bs-css/lib/js/src/Css.js");
var List = require("bs-platform/lib/js/list.js");
var Block = require("bs-platform/lib/js/block.js");
var Curry = require("bs-platform/lib/js/curry.js");
var React = require("react");
var DexterUiActionWindow = require("./DexterUiActionWindow.js");
var DexterUiSecondaryWindow = require("./DexterUiSecondaryWindow.js");

var main = Css.style(/* :: */[
      Css.backgroundColor(Css.hex("F7F9FB")),
      /* [] */0
    ]);

var space = Css.style(/* :: */[
      Css.marginBottom(Css.px(50)),
      /* [] */0
    ]);

var row = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* [] */0
    ]);

var leftColumn = Css.style(/* :: */[
      Css.flexBasis(/* `percent */[
            -119887163,
            65.0
          ]),
      /* [] */0
    ]);

var rightColumn = Css.style(/* :: */[
      Css.flexBasis(/* `percent */[
            -119887163,
            35.0
          ]),
      /* [] */0
    ]);

var Style = {
  main: main,
  space: space,
  row: row,
  leftColumn: leftColumn,
  rightColumn: rightColumn
};

var nbsp = ('\u00a0');

var tokenAmounts = /* :: */[
  /* record */[
    /* symbol */"XTZ",
    /* name */"Tez",
    /* amount */300
  ],
  /* :: */[
    /* record */[
      /* symbol */"ABC",
      /* name */"ABC",
      /* amount */12
    ],
    /* :: */[
      /* record */[
        /* symbol */"TZT",
        /* name */"Tezos Tacos",
        /* amount */105
      ],
      /* :: */[
        /* record */[
          /* symbol */"TZG",
          /* name */"Tezos Gold",
          /* amount */2000
        ],
        /* [] */0
      ]
    ]
  ]
];

function DexterUi(Props) {
  var bakers = Props.bakers;
  var match = React.useReducer((function (state, action) {
          if (typeof action === "number") {
            return /* record */[
                    /* action */state[/* action */0],
                    /* address */state[/* address */1],
                    /* activeBaker */state[/* activeBaker */2],
                    /* leftToken */state[/* rightToken */4],
                    /* rightToken */state[/* leftToken */3]
                  ];
          } else {
            switch (action.tag | 0) {
              case /* UpdateAction */0 :
                  return /* record */[
                          /* action */action[0],
                          /* address */state[/* address */1],
                          /* activeBaker */state[/* activeBaker */2],
                          /* leftToken */state[/* leftToken */3],
                          /* rightToken */state[/* rightToken */4]
                        ];
              case /* UpdateActiveBaker */1 :
                  return /* record */[
                          /* action */state[/* action */0],
                          /* address */state[/* address */1],
                          /* activeBaker */action[0],
                          /* leftToken */state[/* leftToken */3],
                          /* rightToken */state[/* rightToken */4]
                        ];
              case /* UpdateLeftToken */2 :
                  return /* record */[
                          /* action */state[/* action */0],
                          /* address */state[/* address */1],
                          /* activeBaker */state[/* activeBaker */2],
                          /* leftToken */action[0],
                          /* rightToken */state[/* rightToken */4]
                        ];
              case /* UpdateRightToken */3 :
                  return /* record */[
                          /* action */state[/* action */0],
                          /* address */state[/* address */1],
                          /* activeBaker */state[/* activeBaker */2],
                          /* leftToken */state[/* leftToken */3],
                          /* rightToken */action[0]
                        ];
              
            }
          }
        }), /* record */[
        /* action : Swap */0,
        /* address */undefined,
        /* activeBaker */undefined,
        /* leftToken */List.hd(tokenAmounts),
        /* rightToken */List.hd(List.tl(tokenAmounts))
      ]);
  var dispatch = match[1];
  var state = match[0];
  return React.createElement("div", {
              className: main
            }, React.createElement("div", {
                  className: row
                }, React.createElement("div", {
                      className: leftColumn
                    }, React.createElement(DexterUiActionWindow.make, {
                          address: state[/* address */1],
                          action: state[/* action */0],
                          activeBaker: state[/* activeBaker */2],
                          onChange: (function (action) {
                              return Curry._1(dispatch, /* UpdateAction */Block.__(0, [action]));
                            }),
                          onBakerChange: (function (baker) {
                              return Curry._1(dispatch, /* UpdateActiveBaker */Block.__(1, [baker]));
                            }),
                          bakers: bakers,
                          tokenAmounts: tokenAmounts,
                          leftToken: state[/* leftToken */3],
                          rightToken: state[/* rightToken */4],
                          onLeftTokenChange: (function (token) {
                              return Curry._1(dispatch, /* UpdateLeftToken */Block.__(2, [token]));
                            }),
                          onRightTokenChange: (function (token) {
                              return Curry._1(dispatch, /* UpdateRightToken */Block.__(3, [token]));
                            }),
                          onSwapLeftAndRight: (function (param) {
                              return Curry._1(dispatch, /* SwapLeftAndRight */0);
                            })
                        })), React.createElement("div", {
                      className: rightColumn
                    }, React.createElement(DexterUiSecondaryWindow.make, {
                          address: state[/* address */1]
                        }))), React.createElement("div", {
                  className: space
                }, nbsp));
}

var make = DexterUi;

exports.Style = Style;
exports.nbsp = nbsp;
exports.tokenAmounts = tokenAmounts;
exports.make = make;
/* main Not a pure module */
