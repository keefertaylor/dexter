'use strict';

var Css = require("bs-css/lib/js/src/Css.js");
var React = require("react");
var DexterUiColor = require("./DexterUiColor.js");

function container(active) {
  return Css.style(/* :: */[
              Css.borderColor(active ? DexterUiColor.blue : DexterUiColor.lightGrey),
              /* :: */[
                Css.borderStyle(Css.solid),
                /* :: */[
                  Css.borderWidth(Css.px(1)),
                  /* :: */[
                    Css.borderRadius(/* `px */[
                          25096,
                          10
                        ]),
                    /* [] */0
                  ]
                ]
              ]
            ]);
}

var flexbox = Css.style(/* :: */[
      Css.display(/* flex */-1010954439),
      /* :: */[
        Css.marginLeft(/* `px */[
              25096,
              24
            ]),
        /* :: */[
          Css.marginRight(/* `px */[
                25096,
                24
              ]),
          /* :: */[
            Css.marginTop(/* `px */[
                  25096,
                  16
                ]),
            /* :: */[
              Css.marginBottom(/* `px */[
                    25096,
                    16
                  ]),
              /* [] */0
            ]
          ]
        ]
      ]
    ]);

var icon = Css.style(/* :: */[
      Css.height(/* `px */[
            25096,
            32
          ]),
      /* :: */[
        Css.width(/* `px */[
              25096,
              32
            ]),
        /* [] */0
      ]
    ]);

var name = Css.style(/* :: */[
      Css.fontSize(/* `px */[
            25096,
            14
          ]),
      /* :: */[
        Css.color(DexterUiColor.black),
        /* [] */0
      ]
    ]);

var address = Css.style(/* :: */[
      Css.fontSize(/* `px */[
            25096,
            14
          ]),
      /* :: */[
        Css.color(DexterUiColor.grey),
        /* [] */0
      ]
    ]);

var Style = {
  container: container,
  flexbox: flexbox,
  icon: icon,
  name: name,
  address: address
};

function DexterUiBakerView(Props) {
  var baker = Props.baker;
  var active = Props.active;
  var match = baker[/* icon */0];
  return React.createElement("div", {
              className: container(active)
            }, React.createElement("div", {
                  className: flexbox
                }, React.createElement("div", undefined, match !== undefined ? React.createElement("img", {
                            className: icon,
                            src: match
                          }) : React.createElement("img", {
                            className: icon
                          })), React.createElement("div", undefined, React.createElement("div", undefined, baker[/* name */1]), React.createElement("div", undefined, baker[/* address */2]))));
}

var make = DexterUiBakerView;

exports.Style = Style;
exports.make = make;
/* flexbox Not a pure module */
