'use strict';

var Css = require("bs-css/lib/js/src/Css.js");
var React = require("react");
var DexterUiFont = require("./DexterUiFont.js");
var DexterUiColor = require("./DexterUiColor.js");

var center = Css.style(/* :: */[
      Css.display(Css.table),
      /* :: */[
        Css.margin2(Css.px(75), Css.auto),
        /* [] */0
      ]
    ]);

var title = Css.style(/* :: */[
      Css.fontSize(/* `px */[
            25096,
            25
          ]),
      /* :: */[
        DexterUiFont.boldFont,
        /* :: */[
          Css.color(DexterUiColor.black),
          /* [] */0
        ]
      ]
    ]);

var addressTitle = Css.style(/* :: */[
      Css.fontSize(/* `px */[
            25096,
            16
          ]),
      /* :: */[
        DexterUiFont.boldFont,
        /* :: */[
          Css.color(DexterUiColor.black),
          /* [] */0
        ]
      ]
    ]);

var address = Css.style(/* :: */[
      Css.fontSize(/* `px */[
            25096,
            16
          ]),
      /* :: */[
        DexterUiFont.defaultFont,
        /* :: */[
          Css.color(DexterUiColor.black),
          /* [] */0
        ]
      ]
    ]);

var button = Css.style(/* :: */[
      Css.backgroundColor(DexterUiColor.blue),
      /* :: */[
        Css.cursor(/* pointer */-786317123),
        /* :: */[
          Css.backgroundRepeat(/* noRepeat */-695430532),
          /* :: */[
            Css.backgroundOrigin(/* paddingBox */972575930),
            /* :: */[
              Css.boxShadow(Css.Shadow.box(Css.px(0), Css.px(12), Css.px(26), undefined, undefined, DexterUiColor.shadowColor)),
              /* :: */[
                Css.borderRadius(/* `px */[
                      25096,
                      10
                    ]),
                /* :: */[
                  Css.borderStyle(/* none */-922086728),
                  /* :: */[
                    Css.padding2(Css.px(20), Css.px(20)),
                    /* [] */0
                  ]
                ]
              ]
            ]
          ]
        ]
      ]
    ]);

var buttonText = Css.style(/* :: */[
      Css.fontSize(/* `px */[
            25096,
            18
          ]),
      /* :: */[
        Css.color(DexterUiColor.white),
        /* :: */[
          DexterUiFont.boldFont,
          /* :: */[
            Css.textAlign(/* center */98248149),
            /* [] */0
          ]
        ]
      ]
    ]);

var Style = {
  center: center,
  title: title,
  addressTitle: addressTitle,
  address: address,
  button: button,
  buttonText: buttonText
};

function DexterUiReceive(Props) {
  var address$1 = Props.address;
  return React.createElement("div", {
              className: center
            }, address$1 !== undefined ? React.createElement(React.Fragment, undefined, React.createElement("div", {
                        className: title
                      }, "Scan QR code to receive"), React.createElement("div", {
                        className: addressTitle
                      }, "Your wallet address:"), React.createElement("div", {
                        className: address
                      }, address$1), React.createElement("div", undefined, "Request amount"), React.createElement("input", undefined), React.createElement("div", {
                        className: center
                      }, React.createElement("div", {
                            className: button
                          }, React.createElement("span", {
                                className: buttonText
                              }, "Generate new QR code")))) : React.createElement(React.Fragment, undefined, React.createElement("div", {
                        className: title
                      }, "Please connect your wallet first!"), React.createElement("div", {
                        className: center
                      }, React.createElement("div", {
                            className: button
                          }, React.createElement("span", {
                                className: buttonText
                              }, "Connect wallet")))));
}

var make = DexterUiReceive;

exports.Style = Style;
exports.make = make;
/* center Not a pure module */
