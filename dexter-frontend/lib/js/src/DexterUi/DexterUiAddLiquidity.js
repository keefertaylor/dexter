'use strict';

var Css = require("bs-css/lib/js/src/Css.js");
var React = require("react");
var DexterUiFont = require("./DexterUiFont.js");
var DexterUiColor = require("./DexterUiColor.js");
var DexterUiExchange = require("./DexterUiExchange.js");
var DexterUiBakerSelector = require("./DexterUiBakerSelector.js");

var center = Css.style(/* :: */[
      Css.display(Css.table),
      /* :: */[
        Css.margin2(Css.px(75), Css.auto),
        /* [] */0
      ]
    ]);

var message = Css.style(/* :: */[
      Css.textAlign(/* center */98248149),
      /* :: */[
        Css.display(/* inlineBlock */-147785676),
        /* :: */[
          DexterUiFont.defaultFont,
          /* :: */[
            Css.fontSize(/* `px */[
                  25096,
                  16
                ]),
            /* :: */[
              Css.color(DexterUiColor.grey),
              /* :: */[
                Css.marginBottom(/* `px */[
                      25096,
                      34
                    ]),
                /* [] */0
              ]
            ]
          ]
        ]
      ]
    ]);

var firstMessage = Css.style(/* :: */[
      Css.marginBottom(/* `px */[
            25096,
            24
          ]),
      /* [] */0
    ]);

var money = Css.style(/* :: */[
      DexterUiFont.boldFont,
      /* [] */0
    ]);

function button(address) {
  return Css.style(/* :: */[
              address !== undefined ? Css.backgroundColor(DexterUiColor.blue) : Css.backgroundColor(DexterUiColor.lightBlue),
              /* :: */[
                address !== undefined ? Css.cursor(/* pointer */-786317123) : Css.cursor(/* default */465819841),
                /* :: */[
                  Css.backgroundRepeat(/* noRepeat */-695430532),
                  /* :: */[
                    Css.backgroundOrigin(/* paddingBox */972575930),
                    /* :: */[
                      Css.boxShadow(Css.Shadow.box(Css.px(0), Css.px(12), Css.px(26), undefined, undefined, DexterUiColor.shadowColor)),
                      /* :: */[
                        Css.borderRadius(/* `px */[
                              25096,
                              10
                            ]),
                        /* :: */[
                          Css.borderStyle(/* none */-922086728),
                          /* :: */[
                            Css.padding2(Css.px(20), Css.px(20)),
                            /* [] */0
                          ]
                        ]
                      ]
                    ]
                  ]
                ]
              ]
            ]);
}

var buttonText = Css.style(/* :: */[
      Css.fontSize(/* `px */[
            25096,
            18
          ]),
      /* :: */[
        Css.color(DexterUiColor.white),
        /* :: */[
          DexterUiFont.boldFont,
          /* :: */[
            Css.textAlign(/* center */98248149),
            /* [] */0
          ]
        ]
      ]
    ]);

var Style = {
  center: center,
  message: message,
  firstMessage: firstMessage,
  money: money,
  button: button,
  buttonText: buttonText
};

function DexterUiAddLiquidity(Props) {
  var activeBaker = Props.activeBaker;
  var address = Props.address;
  var bakers = Props.bakers;
  var onBakerChange = Props.onBakerChange;
  var tokenAmounts = Props.tokenAmounts;
  var leftToken = Props.leftToken;
  var rightToken = Props.rightToken;
  var onLeftTokenChange = Props.onLeftTokenChange;
  var onRightTokenChange = Props.onRightTokenChange;
  var onSwapLeftAndRight = Props.onSwapLeftAndRight;
  return React.createElement("div", undefined, React.createElement(DexterUiExchange.make, {
                  action: /* AddLiquidity */1,
                  tokenAmounts: tokenAmounts,
                  leftToken: leftToken,
                  rightToken: rightToken,
                  onLeftTokenChange: onLeftTokenChange,
                  onRightTokenChange: onRightTokenChange,
                  onSwapLeftAndRight: onSwapLeftAndRight
                }), React.createElement(DexterUiBakerSelector.make, {
                  activeBaker: activeBaker,
                  bakers: bakers,
                  onBakerChange: onBakerChange
                }), React.createElement("div", {
                  className: center
                }, React.createElement("div", {
                      className: message
                    }, React.createElement("div", {
                          className: firstMessage
                        }, "You are adding ", React.createElement("span", {
                              className: money
                            }, "10 XTZ"), " and ", React.createElement("span", {
                              className: money
                            }, "~20 ABC"), " = ", React.createElement("span", {
                              className: money
                            }, "1,000 ABC"), " pool tokens."), React.createElement("div", undefined, "Est. annual reward ", React.createElement("span", {
                              className: money
                            }, "66.60 XTZ"), " ($60)"))), React.createElement("div", {
                  className: center
                }, React.createElement("button", {
                      className: button(address)
                    }, React.createElement("span", {
                          className: buttonText
                        }, "Add liquidity"))));
}

var make = DexterUiAddLiquidity;

exports.Style = Style;
exports.make = make;
/* center Not a pure module */
