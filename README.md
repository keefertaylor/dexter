# dexter

## Resources

[Interesting insight to a few operations](https://tezos.stackexchange.com/questions/1248/how-to-concatenate-a-string-and-tez-in-liquidity/1254#1254)


```bash
~/alphanet.sh start --rpc-port 9323 --cors-header='origin,content-type,x-requested-with,accept,range' --cors-origin='*'

~/alphanet.sh stop

~/alphanet.sh start --rpc-port=9323 --cors-header='Origin, X-Requested-With, Content-Type, Accept, Range' --cors-origin='*' 
```


this will be deprecated
```bash
curl -v 127.0.0.1:9323/chains/main/blocks/head/context/contracts/KT1DmCHxit2bQ2GiHVc24McY6meuJPMTrqD8/storage

curl -v http://127.0.0.1:9323/chains/main/blocks/head/context/contracts/KT1DmCHxit2bQ2GiHVc24McY6meuJPMTrqD8/big_map_get -d '{"key":{"string":"tz1Z1nn9Y7vzyvtf6rAYMPhPNGqMJXw88xGH"},"type":{"prim":"address"}}' --header "Content-Type: application/json"
```
