# FA1.2

Test transfers for development of the FA1.2 contract.

```bash
tezos-client list known addresses

simon: tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3 (unencrypted sk known)
bob: tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i (unencrypted sk known)
alice: tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn (unencrypted sk known)

# originate a fa1.2 contract
tezos-client originate contract tezosGold transferring 0 from alice running ./contracts/fa1.2.tz --init 'Pair {Elt "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" (Pair {} 1000000)} 1000000' --burn-cap 10 --force


# get alice's tezosGold tokens
tezos-client get big map value for '"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"' of type 'address' in tezosGold

# get bob's tezosGold tokens
tezos-client get big map value for '"tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"' of type 'address' in tezosGold

# get simon's tezosGold tokens
tezos-client get big map value for '"tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3"' of type 'address' in tezosGold

# approve bob to handle alice's tokens
tezos-client transfer 0 from alice to tezosGold --entrypoint 'approve' --arg 'Pair "tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i" 200' --burn-cap 1

# bob transfer's 10 of alice's tokens to simon
tezos-client transfer 0 from bob to tezosGold --entrypoint 'transfer' --arg 'Pair (Pair "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3") 150' --burn-cap 1

```
