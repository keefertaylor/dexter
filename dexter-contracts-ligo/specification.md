## Dexter Exchange: An Informal Specification (Version 1)

This document is an informal specification for a contract that can be originated on the Tezos blockchain, a decentralized token exchange for [FA1.2 tokens](https://gitlab.com/tzip/tzip/blob/master/Proposals/TZIP-0007/FA1.2.md). Implementation details will mostly be ignored in this document.


## Terminology

For clarity, we will define and highlight certain terms that are used throughout the document.


*   **XTZ**: the official token on the Tezos blockchain. Also known as tez or colloquially as tezzies. The smallest unit is mutez. The common unit is tez. 1 tez = 1000000 mutez. For clarity, we will refer to this token as XTZ.

 



*   **FA1.2**: tokens from a [FA1.2 contract](https://gitlab.com/tzip/tzip/blob/master/Proposals/TZIP-0007/FA1.2.md).
*   **token**:  tokens from a [FA1.2 contract](https://gitlab.com/tzip/tzip/blob/master/Proposals/TZIP-0007/FA1.2.md) (currently synonym FA1.2)
*   **LQT**: a token that is internal to the dexter exchange contract. It represents the amount of liquidity in a contract relative to the amount **XTZ** and **FA1.2 **held in that contract.
*   **exchange** **contract**: the main dexter exchange contract which provides a decentralized exchange on Tezos for **XTZ** and a particular **FA1.2 token**.

 



*   **token contract**: the contract that handles the **FA1.2 token**.
*   **XTZ pool**: amount of **XTZ** held by an exchange contract, in mutez. Equals the balance of the contract before the addition of the amount received in the current transaction, i.e., `xtz_pool = BALANCE - AMOUNT`.
*   **token pool**: amount of **FA1.2** held by an exchange contract.
*   **LQT total**: total amount of **LQT** in an exchange contract. LQT is owned by contracts/addresses that provide liquidity.
*   **sender**: the contract that initiated the current internal transaction, as returned by `SENDER` in Michelson. The sender could be the owner of the liquidity or an intermediate contract that has an **allowance** for a particular owner.
*   **allowance**: mechanism whereby one contract (the allower) gives permission to another contract (the allowee) to consume the allower’s **LQT**.  


## The Exchange Contract

-- todo: explain how errors are handled


### Entry Points

The exchange contract has the following entry points.



*   approve
*   add liquidity
*   remove liquidity
*   XTZ to token
*   token to XTZ
*   token to token
*   vote


### Storage


#### Mutable



*   **accounts** (big_map of address to pair of amount of LQT it owns and map of address to allowance). For a given address `adr`, we refer to the first component as `accounts[adr].balance` and the second as `accounts[adr].allowance`.
*   **votes** (map of owner address to baker key hash)
*   **current baker** (option key_hash)
*   **lqt_total** (total amount of LQT in a single exchange contract, i.e. the LQT pool)
*   **token_pool** (total amount of token that the exchange owns, it is able to monitor this without querying the token contract because no other contract will have permission to manage the exchange contract’s tokens)


#### Immutable



*   token_address (address of the FA1.2 token contract)


##### Invariants (true statements at the end of each contract execution)



*   lqt_total = sum of the each owner’s LQT balance as given by accounts
*   token_pool = the FA1.2 token pool, i.e., the exchange contract’s balance of tokens in the FA1.2 contract


### Fees

Fees are collected in the three types of trade at the following rates:



*   XTZ to token: 0.3% fee paid in XTZ
*   token to XTZ: 0.3% fee paid in FA1.2 tokens
*   token to token: this is composed of two transactions (T1 and T2) in two exchanges (E1 and E2). For T1 in E1, the sender sells the FA1.2 token of E1 for XTZ and pays a 0.3% fee in that token, then it creates the T2 transaction on E2 and calls XTZ to token where the token is the one traded on E2, the sender pays a 0.3% fee in XTZ on E2.

Fees collected during trades (XTZ to token and, token to XTZ and token to token) add value to XTZ and FA1.2 without increasing the number of LQT tokens.


### Elect Baker

Currently in Tezos smart contracts we cannot get information about the block or the cycle. We have decided to implement a live voting system. Any time there is a change to the amount of votes or to the amount of LQT owned by a voting owner, the exchange contract re-calculates the winner and sets the winner as the new delegate. Each LQT counts as a vote and each owner can vote for only one baker.

The elect baker logic is run any time the amount of LQT an owner owns is changed (add liquidity and remove liquidity entry points) or their vote changes (the vote entry point).

For each owner-baker pair in store.votes, get the the LQT for that owner and tally it for that particular baker, then find the baker with the highest count of votes. Set the exchange contract's delegate to that key hash. If there are no votes, then any delegation is removed. If there is a tie, the incumbent (the current baker) will be kept. In this case no operation is returned.

TODO: figure out in which circumstances delegation might fail, and reflect on the consequences on the exchange contract.


### Implicit parameters

For all entrypoint calls, we will refer to the following implicit parameters:



*   **sender**: the sender of the entrypoint, as returned by the `SENDER` instruction.
*   **amount**: the amount of XTZ, in mutez, in the call, as returned by the `AMOUNT` instruction.

Note also that all addresses that sent as parameters (e.g. to address in _Remove Liquidity Entry Point_) should be contracts of type unit. Otherwise the contract will fail.


### Approve Entry Point

The approve entry point sets up an allowance, allowing one contract to burn the liquidity of an owner.


##### Parameters



*   **spender** address: the address of who will be allowed to spend the LQT allowance.
*   **allowance** natural: the amount of LQT the spender is allowed to spend for the owner.
*   **current_allowance** natural: used to prevent transfer attack as described in https://docs.google.com/document/d/1YLPtQxZu1UAvO9cZ1O2RPXBbT0mooh4DYKjA_jp-RLM/edit

If `accounts[sender].allowance[spender]` is equal to **current_allowance**, or is undefined and current_allowance is zero, then update the map of address to allowance accounts for the sender to include the key spender with the value **allowance**. 


```
    accounts[sender].allowance[spender] := allowance
```



##### Operations



*   No operations.


##### Errors



*   **sender** == **spender** (there is no reason for an owner to give them self an allowance)
*   accounts[sender].allowance[spender] != **current_allowance**


### Liquidity Entry Points


#### Add Liquidity Entry Point


##### Parameters

- **owner** address: the address of the account that will own the LQT

- **min_lqt_created** natural: minimum number of LQT the sender will mint if total LQT is greater than 0.

- **max_tokens_deposited** natural: maximum number of tokens deposited

- **deadline** timestamp: the time after which this transaction can no longer be executed.

Note: This will trigger an allowance check in the FA1.2 contract for transferring FA1.2 from the **owner** to the exchange if the sender is not the **owner**.

Requires running elect baker at the end because it changes the amount of LQT.

tokens_deposited is the amount of FA1.2 tokens deposited from the owner to the exchange contract. It differs between first liquidity provider and non-first liquidity provider.


##### Operations

For both First and Non-First Liquidity Providers.



*   FA1.2 Transfer tokens_deposited from owner to exchange contract
*   Set delegate if necessary via elect baker.


##### Errors

For both First and Non-First Liquidity Providers.



*   now > **deadline**
*   **max_tokens_deposited** == 0 
*   **min_lqt_created** == 0 (no reason to run this if the sender allows zero liquidity to be added) 


##### First Liquidity Provider

The first liquidity provider sets the initial exchange rate of XTZ to FA1.2.

This is called when **lqt_total** is zero.

If the deadline has passed, i.e. `NOW > timestamp`, then the transaction fails.

The initial **lqt_total** is set to be equivalent to the amount of XTZ sent by

the first liquidity provider, i.e.:


```
    lqt_total := amount 
```


The amount XTC sent by sender is added  to the balance of the owner in the exchange.


```
    accounts[owner].balance := amount
```


The owner sends tokens_deposited to the exchange contract.


```
tokens_deposited := max_tokens_deposited
```


An internal transaction is generated that calls the token contract, instructing it to transfer **max_tokens_deposited** tokens from the owner to the exchange contract. 


```
	token_pool += max_tokens_deposited
```


Note: this internal transaction will fail unless the owner has sent a message to the token contract before to set up an allowance for the exchange to transfer **max_tokens_deposited** from the owner to exchange.

Note: If the owner does not have at least **max_tokens_deposited** at the token contract, then the token contract will fail, hence cancelling the whole operation.  


###### Errors

For only first liquidity provider.



*   amount < 1tz


##### Non-First Liquidity Provider

Note: Adding liquidity requires the contract sender to provide an equivalent exchange value of XTZ and FA1.2, meaning x of XTZ can be traded for y of FA1.2.

This is called when **lqt_total** is greater than zero.

If the deadline has passed, i.e. NOW > timestamp, then the transaction fails.

The sender provides a minimum amount of LQT they want to create (min_lqt_created), the amount of XTZ they want to provide (amount) and the maximum number of FA1.2 they want to provide (**max_tokens_deposited)**.

The amount of LQT created is:


```
    lqt_created = floor(lqt_total * amount / XTZ_pool)
```


If `lqt_created` is lower than the amount of LQT they want to create, min_lqt_created, the transaction will fail.

Otherwise, **lqt_total** is updated to contain the newly created liquidity:


```
    lqt_total := lqt_total + lqt_created
```


The newly created liquidity is added to the balance of the owner in exchange:


```
    accounts[owner].balance += lqt_created
```


The amount of FA1.2 to be deposited is:


```
    tokens_deposited = floor(token_pool * amount / XTZ_pool).
```


If `tokens_deposited` is greater than the maximum number they want to provide, `max_tokens_deposited`, the transaction will fail with the error message "addLiquidityContinuation: Expected max_tokens_deposited >= tokens_deposited".

An internal transaction is generated that calls the token contract, instructing it to transfer `tokens_deposited` tokens from the owner to the exchange contract. 


```
	token_pool += tokens_deposited
```



###### Errors

For only non-first liquidity provider.



*   **min_lqt_created** == 0
*   **max_tokens_deposited** < tokens_deposited
*   lqt_minted < **min_lqt_created**


#### Remove Liquidity Entry Point

LQT tokens can be burned (or destroyed) in order to withdraw a proportional

amount of XTZ and FA1.2. They are withdrawn at the current exchange rate


##### Parameters



*   **owner**: address which owns the LQT that will be burned.
*   **to**: address where to send the XTZ and FA1.2 tokens.
*   **lqt_burned** natural: amount of LQT the sender wants to burn
*   **min_xtz_withdrawn** mutez: the minimum amount of XTZ (in Mutez) the sender wants to withdraw and send.
*   **min_tokens_withdrawn** natural: the minimum amount of FA1.2 Tokens the sender wants to withdraw and send.
*   **deadline** timestamp: the time after which this transaction can no longer be executed


##### Allowance Check

Burning LQT requires an allowance check if the sender is not the owner. If the sender does not have permission then it will fail with an error “sender does not have permission to spend the owner’s liquidity”. If the sender has permission, but the amount exceeds their allowance then it will fail with error “the amount exceeds the sender’s allowance”.

Allowance is added through the approve entry point. When a contract A is given an allowance of X LQT for contract B, it is allowed to burn up to X LQT for A. 

Note: A liquidity owner will also need to give a Dexter contract permission to spend its FA1.2 tokens. The rules are defined in the [FA1.2 contract](https://gitlab.com/tzip/tzip/blob/master/A/FA1.2.md). This should be handled by a dApp.


##### Logic

Requires running elect baker at the end because it changes the amount of LQT.

If the deadline has passed, i.e. NOW > timestamp, then the transaction fails.

Requires allowance check for lqt_burned. That is, unless sender = owner, verify that


```
    Accounts[owner].allowance[sender] >= lqt_burned
```


And otherwise fail (as described below). If successful, remove the burnt LQT from allowance:


```
    Accounts[owner].allowance[sender] -= lqt_burned
```


The sender provides the amount of LQT they want to burn in the lqt_burned parameter, and the minimum amount of XTZ and FA1.2 they want to withdraw, in the min_xtz_withdrawn and min_tokens_withdrawn parameters respectively. If either limits is not met then the transaction will fail.

The amount of XTZ withdrawn is 


```
    xtz_withdrawn = floor(xtz_pool * lqt_burned / lqt_total). 
```


This is implemented by the emission of an internal transaction that transfers xtz_withdrawn to the to address.

The amount of FA1.2 withdrawn is:


```
     tokens_withdrawn = floor(token_pool * lqt_burned / lqt_total).
```


This is implemented by the emission of an internal operation that calls token, instructing it to transfer the tokens_withdrawn amount of tokens from the exchange contract to the to address.

Finally, the balance of the owner and the total supply is updated :


```
    accounts[owner].balance -= lqt_burned
    lqt_total -= lqt_burned 
```


If lqt_burned is greater than the account owner’s balance, it will fail.


##### Operations



*   Send tez amount to to address.
*   FA1.2 Transfer tokens_deposited from exchange contract to to address.
*   Set delegate if necessary via elect baker.


##### Errors



*   now > deadline
*   min_xtz_withdrawn == 0
*   min_tokens_withdrawn == 0 
*   xtz_withdrawn < min_xtz_withdrawn
*   FA1.2_withdrawn < max_tokens_withdrawn
*   lqt_burned > accounts[owner].balance
*   lqt_burned > account[owner].allowances[sender] when sender != owner
*   lqt_total == 0 (before withdrawing)


### Exchange Entry Points

The exchange rate of a particular XTZ-FA1.2 exchange contract is relative to the

size of their pools. I.e., disregarding fees, the exchange rate is XTZ to FA1.2 is token_pool / XTC pool. 


#### XTZ to Token Entry Point


##### Parameters



*   **to** address: address to send the FA1.2 tokens to. 
*   **min_tokens_bought** natural: minimum amount of tokens the sender wants to buy.
*   **deadline** timestamp: the time after which this transaction can no longer be executed.

If the deadline has passed, i.e. NOW > timestamp, then the transaction fails.

A fee of %0.3 is taken. This is implemented thus:


```
    tokens_bought = floor((amount * 997 * token_pool) / 
            (xtz_pool * 1000 + (amount * 997)))
    token_pool -= tokens_bought
```


tokens_bought must be greater than or equal to min_tokens_bought or it will fail.

The `to address` receives the FA1.2 equivalent to `tokens_bought`. This is implemented by the emission of an internal operation that calls token, instructing it to transfer `FA1.2_bought` tokens from the exchange contract to `to address`.

lqt_total and balance are unaffected. 


##### Operations



*   FA1.2 Transfer tokens_bought from exchange contract to to address.


##### Errors



*   now > deadline
*   min_tokens_bought > tokens_bought
*   token_pool - tokens_bought is negative


#### Token to XTZ Entry Point


##### Parameters



*   **owner** address: the address that owns that FA1.2 that are being sold
*   **to** address: address to send the XTZ tokens to. 
*   **tokens_sold** natural: the number of tokens the sender wants to sell.
*   **min_xtz_bought** mutez: minimum amount of XTZ (in Mutez) the sender wants to purchase.
*   **deadline** timestamp: the time after which this transaction can no longer be executed.

If the deadline has passed, i.e. `NOW > timestamp`, then the transactions fails.

Note: Requires allowance check in the FA1.2 contract for transferring FA1.2 from the owner to the exchange.

A fee of %0.3 is taken. This is implemented thus:


```
    xtz_bought = floor((tokens_sold * 997 * Balance) / 
            (token_pool * 1000 + (tokens_sold * 997)))
    token_pool += tokens_sold
```


The to address receives XTZ equivalent to `XTZ_bought`. This is implemented by the emission

of an internal operation that transfers this sum to the `to address`.

The exchange contract receives FA1.2 equivalent to `tokens_sold`. This is implemented by the emission of an internal operation that calls token, instructing it to transfer `tokens_sold` tokens from the `owner` to the exchange contract.

`lqt_total` and `balance` are unaffected (note: assuming that amount is zero). 


##### Operations



*   Transfer XTZ from exchange to to address. (with type unit)


##### Errors



*   now > deadline
*   min_xtz_bought > xtz_bought


#### Token to Token Entry Point


##### Parameters



*   **target_token_exchange **address: the address of the exchange for the tokens the user wants to purchase
*   **min_tokens_bought** nat: the minimum amount of tokens we want to buy from target_token_exchange
*   **owner** address: the address that owns the FA1.2 tokens that are being sold
*   **to address**: the address that will receive the purchased FA1.2 tokens from target_token_exchange
*   **tokens_sold** nat: the amount of FA1.2 tokens that the owner is selling
*   **deadline** timestamp: the time after which this transaction can no longer be executed.

Requires allowance check in the FA1.2 contract for transferring FA1.2 from the owner to the exchange for the FA1.2 that is traded in.

If the deadline has passed, i.e. NOW > timestamp, then the transactions fails.

This is currently unimplemented, but the idea is that when a sender wants to trade

FA1.2 x for FA1.2 y, they perform FA1.2 to XTZ for x and call XTZ to FA1.2

for y with the amount of XTZ gained from the first transaction and the amount

gained from the second transaction, in FA1.2 y, is given to the sender.


```
    xtz_bought = floor((tokens_sold * 997 * balance) / (store.s.token_pool * 1000n + (tokens_sold * 997n)));
    token_pool := token_pool + tokens_sold;
```



##### Operations



*   transaction(XtzToToken(to_, min_tokens_bought, deadline), xtz_bought, target_token_exchange_contract);
*   transaction(Transfer(owner, self_address, tokens_sold), 0mutez, token_contract);


##### Errors



*   now > deadline


#### Vote Entry Point

Each liquidity supplier (owner of LQT) has the right to vote for a baker. Since a single contract can only delegate its tokens to one contract, the dexter exchange contracts will delegate its XTZ to the baker with the most votes. The number of votes are equivalent to the number of LQT. An owner votes for a single baker with all of their LQT. 


#### Parameters



*   **baker** option(key hash): the baker who the sender votes for.

Note: Requires running elect baker because an owner’s voted baker changes.

The provided `address` must correspond to an existing contract. Otherwise, the contract fails with an “Invalid baker address”  error. 

The sender must be an owner of LQT, otherwise the contract fails with an “Sender has no LQT” error. 

If baker is not none:


```
	Votes[sender] := baker
```


Else


```
	Votes[sender] := undefined
```



#### Errors



*   Invalid baker address
*   Sender has no LQT


### What to Prove


#### Account for the amount of FA1.2

Because of the challenge and gas cost of inter contract communication, we have decided that the exchange should be responsible for knowing how much FA1.2 it owns. We want to prove that we are calculating it correctly. The amount of FA1.2 owned is changed by the Add Liquidity, Remove Liquidity, XTZ to FA1.2, FA1.2 to XTZ and FA1.2 to FA1.2 entry points.


#### Account for LQT

Verify the invariant on lqt_total (described above).


#### Allowance system 

Just maintain the specification.


#### Voting system

Make sure that the delegate is always equal to the baker having received the maximum number of votes (or the incumbent one in case of tie).
