// baker_votes map(key_hash, vote_count)
// current_baker(key_hash, vote_count)
// account (nat, map(), option key_hash)

// elect_baker
// iterate baker_votes, set current_baker to key_hash and votes

type baker_address is key_hash; // change this to key_hash once it is comparable

// the type of the FA1.2 contract
type token_contract_action is
| Transfer of (address * address * nat)
| Approve of (address * nat)
| GetAllowance of (address * address * contract(nat))
| GetBalance of (address * contract(nat))
| GetTotalSupply of (unit * contract(nat))

// the exchange contract entry points
type action is
| Approve of (address * nat * nat)
| AddLiquidity of (address * nat * nat * timestamp)
| RemoveLiquidity of (address * address * nat * tez * nat * timestamp)
| XtzToToken of (address * nat * timestamp)
| TokenToXtz of (address * address * nat * tez * timestamp)
| Vote of (baker_address * bool)

type account is record
  balance   : nat;
  allowances: map(address, nat);
  vote      : option(baker_address);
end

// hash_key

// this is just to force big_maps as the first item of a pair on the top
type s is record
  votes: map(baker_address, nat); // baker address to vote counts
  current_baker: option(baker_address * nat);
  total_liquidity: nat;
  token_address: address;
  token_pool: nat;
end

type store is record
  s: s;
  accounts: big_map(address, account); // key_hash is the bakerthey vote fort
end

function mutez_to_natural(const a: tez): nat is
  block {skip} with a / 1mutez

function natural_to_mutez(const a: nat): tez is
  block {skip} with a * 1mutez

const empty_allowances : map(address,nat) = map end;

const empty_ops : list(operation) = list end;

const no_vote : option(baker_address) = None;

const no_votes : option(nat) = None;

// this will fail if provided a negative number
function int_to_nat(const error: string ; const a: int): nat is
  block {
    var result : nat := 0n;
    if (a >= 0) then block {
      result := abs(a);
    } else block {
      failwith(error)
    };    
  } with result;

// get an account from the big_map, if one does not exist for a particular
// address then create one.
function get_account(const a: address ; const m: big_map(address, account)): account is
  block { skip } with (
    case (m[a]) of
      | None          -> record balance = 0n; allowances = empty_allowances; vote = no_vote; end
      | Some(account) -> account
    end
  );

function update_allowance(const owner            : address;
                          const spender          : address;
                          const updated_allowance: nat;
                          var   store            : store):
                          store is
  block {
    if (spender =/= owner) then block {
      var account: account        := get_account(owner, store.accounts);
      account.allowances[spender] := updated_allowance;
      store.accounts[owner]       := record balance = account.balance; allowances = account.allowances; vote = account.vote; end;
    } else {
      skip;
    }
  } with store;


function approve(const spender  : address;
                 const allowance: nat;
                 const current_allowance: nat;
                 var store      : store):
                 (list(operation) * store) is
  block {
    if (spender =/= sender) then block {
      // get the sender's account
      // if the account does not exist, fail, we do not want to create accounts here
      // creating accounts should be done in add_liquidity
      const account: account = get_account(sender, store.accounts);

      var sender_allowances: map(address, nat) := account.allowances;
      sender_allowances[spender] := allowance;
      store.accounts[sender]  := record balance = account.balance; allowances = sender_allowances; vote = account.vote; end;
    } else block {
      failwith("approve: the spender must not be the sender. The owner already has rights to all of the LQT");
    }
  } with (empty_ops, store);

// if sender is owner, return amount, otherwise check if sender has permission
// if true then return amount, otherwise fail
function get_sender_allowance(const owner: address ; const store: store): nat is
  block {
    var result: nat := 0n;
    case store.accounts[owner] of
    | None -> failwith("check_approval: owner account does not exist.")
    | Some(account) -> block {
        if sender =/= owner then block {
          case account.allowances[sender] of
            | None -> failwith("check_approval: allowance for sender does not exist.")
            | Some(allowance) -> result := allowance
          end;
        } else block {
          result := account.balance
        }
      }
    end;
  } with result;

// Check which baker has the most votes. One vote is equal to the number of LQT.
// Set the new baker
function elect_baker(var store: store):
                    (list(operation) * store) is
  block {
    var o_top_baker : option(baker_address * nat) := None;
  
    function get_baker_with_most_votes(const baker : baker_address ; const votes : nat) : unit
      is block {
        case (o_top_baker) of
          | None    -> o_top_baker := Some((baker, votes))
          | Some(top_baker) ->
            if (votes > top_baker.1) then block {
              o_top_baker := Some((baker, votes));
            } else block {
              skip;
            }
        end;
      } with unit;
    map_iter(get_baker_with_most_votes, store.s.votes);

    // check iff baker changed
    
    // set the top baker
    store.s.current_baker := o_top_baker;

    var op_list: list(operation) := nil;

    // not supported in ligo yet
    case (store.s.current_baker) of
      | None -> skip
      | Some(current_baker) -> {
         const op : operation = set_delegate(Some(current_baker.0));
         op_list := list op; end;    
        }
    end;

  } with (op_list, store);

// it is assumed that the exchange contract has permission from the FA1.2 token
// to manage the assets of the user. It is the responsibility of the dApp
// developer to handle permissions.
function add_liquidity(const owner               : address;
                       const min_lqt_created     : nat;
                       const max_tokens_deposited: nat;
                       const deadline            : timestamp;
                       var   store               : store):
                       (list(operation) * store) is
  block {
    // add_liquidity performs a transfer to the token contract, we need to
    // return the operations
    var op_list: list(operation) := nil;
    
    if (now < deadline) then block {
      if (max_tokens_deposited > 0n) then block {
        if (amount > 0mutez) then block {
          if (store.s.total_liquidity > 0n) then block {
            // total_liquidity greater than zero
            if (min_lqt_created > 0n) then block {
              const tez_pool        : nat = mutez_to_natural(balance - amount);
              const nat_amount      : nat = mutez_to_natural(amount);
              const tokens_deposited    : nat = nat_amount * store.s.token_pool      / tez_pool;
              const lqt_minted: nat = nat_amount * store.s.total_liquidity / tez_pool;

              if (max_tokens_deposited >= tokens_deposited) then block {
                if (lqt_minted >= min_lqt_created) then block {
                  const account: account   = get_account(owner, store.accounts);
                  const new_balance: nat   = account.balance + lqt_minted;
                  store.accounts[owner]   := record balance = new_balance; allowances = account.allowances; vote = account.vote; end;
                  store.s.total_liquidity := store.s.total_liquidity + lqt_minted;
                  store.s.token_pool      := store.s.token_pool      + tokens_deposited;

                  // send FA1.2 from owner to exchange
                  const token_contract: contract(token_contract_action) = get_contract(store.s.token_address);
                  const op1: operation = transaction(Transfer(owner, self_address, tokens_deposited), 0mutez, token_contract);

                  // the amount of LQT has changed, elect a baker
                  const result: (list(operation) * store) = elect_baker(store);
                  store := result.1;
                  op_list := list op1; end;
                  // op_list := list op1; result.0; end;
                } else block {
                  failwith("add_liquidity: lqt_minted must be greater than min_lqt_created.");
                }
              } else block {
                failwith("add_liquidity: max_tokens_deposited must be greater than or equal to tokens_deposited.");
              }
            } else block {
              failwith("add_liquidity: min_lqt_created must be greater than zero.");
            }
          } else block {
            // initial add liquidity
            if (amount >= 1tz) then block {
              const tokens_deposited     : nat = max_tokens_deposited;
              const initial_liquidity: nat = mutez_to_natural(balance);

              store.s.total_liquidity := initial_liquidity;
              store.accounts[owner]   := record balance = initial_liquidity; allowances = empty_allowances; vote = no_vote; end;
              store.s.token_pool      := tokens_deposited;

              // send FA1.2 tokens from owner to exchange
              const token_contract: contract(token_contract_action) = get_contract(store.s.token_address);
              const op1: operation = transaction(Transfer(owner, self_address, tokens_deposited), 0mutez, token_contract);

              // the amount of LQT has changed, elect a baker
              const result: (list(operation) * store) = elect_baker(store);
              store := result.1;
              op_list := list op1; end;
              // op_list := list op1; result.0; end;
            } else block {
              failwith("add_liquidity: when intiating the liquidity pool the amount must be at least 1 XTZ.");
            }
          }          
        } else block {
          failwith("add_liquidity: Expected the amount sent to the contract to be greater than 0.");
        }
      } else block {
        failwith("add_liquidity: Expected maxTokens to be greater than 0.");
      }
    } else block {
      failwith("add_liquidity: Expected the deadline to be greater than now.");
    }
  } with (op_list, store);

function remove_liquidity(const owner                : address;
                          const to_                  : address;
                          const lqt_burned           : nat;
                          const min_xtz_withdrawn    : tez;
                          const min_tokens_withdrawn : nat;
                          const deadline             : timestamp;
                          var   store                : store):
                          (list(operation) * store) is
  block {
    var op_list: list(operation) := nil;
    if (now < deadline) then block {
      if (min_xtz_withdrawn > 0mutez) then block {
        if (min_tokens_withdrawn > 0n) then block {
          if (lqt_burned > 0n) then block {
            // returns total if sender is owner, otherwise looks it up
            const lqt: nat = get_sender_allowance(owner, store);
            if (lqt >= lqt_burned) then block {
              if (store.s.total_liquidity > 0n) then block {
                const xtz_withdrawn : tez = natural_to_mutez(lqt_burned * balance / natural_to_mutez(store.s.total_liquidity));

                if (xtz_withdrawn >= min_xtz_withdrawn) then block {
                  const tokens_withdrawn: nat = lqt_burned * store.s.token_pool / store.s.total_liquidity;

                  if (tokens_withdrawn >= min_tokens_withdrawn) then block {                  
                    const account: account = get_account(owner, store.accounts);

                    if (account.balance >= lqt_burned) then block {
                      const new_balance: nat = int_to_nat("remove_liquidity: new_balance: int_to_nat: cannot convert a negative number to a nat.", account.balance - lqt_burned);
                      store.accounts[owner] := record balance = new_balance; allowances = account.allowances; vote = account.vote; end;
                                    
                      store.s.total_liquidity  := int_to_nat("remove_liquidity: total_liquidity: int_to_nat: cannot convert a negative number to a nat.", store.s.total_liquidity - lqt_burned);
                      store.s.token_pool       := int_to_nat("remove_liquidity: token_pool: int_to_nat: cannot convert a negative number to a nat.", store.s.token_pool - tokens_withdrawn);

                      // update allowance
                      // lqt - lqt_burned is safe, we have already checed that lqt >= lqt_burned
                      store := update_allowance(owner, sender, int_to_nat("remove_liquidity: approve: int_to_nat: cannot convert a negative number to a nat.", lqt - lqt_burned), store);
                    
                      // send xtz_withdrawn to to_ address
                      const to_contract: contract(unit) = get_contract(to_);
                      const op1: operation = transaction(unit, xtz_withdrawn, to_contract);

                      // send tokens_withdrawn to to address
                      // if tokens_withdrawn if greater than store.s.token_pool, this will fail
                      const token_contract: contract(token_contract_action) = get_contract(store.s.token_address);
                      const op2: operation = transaction(Transfer(self_address, to_, tokens_withdrawn), 0mutez, token_contract);

                      // the amount of LQT has changed, elect a baker
                      const result2: (list(operation) * store) = elect_baker(store);
                      store := result2.1;

                      // append internal operations
                      op_list := list op1; op2; end;
                      // op_list := list op1; op2; result.0; end;
                    } else block {
                      failwith("remove_liquidity: Expected owner balance to be greater than or equal to burn_lqt.");
                    }
                  } else block {
                    failwith("remove_liquidity: Expected tokens_withdrawn to be greater than or equal to min_tokens_withdrawn.");
                  }
                
                } else block {
                  failwith("remove_liquidity: Expected xtz_withdrawn to be greater than or equal to min_xtz_withdrawn.");
                }              
              } else block {
                failwith("remove_liquidity: Expected total_liquidity to be greater than zero.");
              }
            } else block {
              failwith("remove_liquidity: sender tried to burn more than their allowance.");              
            }
          } else block {
            failwith("remove_liquidity: Expected lqt_burned to be greater than zero.");
          }
        } else block {
          failwith("remove_liquidity: Expected mint_tokens to be greater than zero.");
        }
      } else block {
        failwith("remove_liquidity: Expected min_xtz_withdrawn to be greater than zero.");
      }
    } else block {
      failwith("remove_liquidity: Expected now to be less than the deadline.");
    }
  } with (op_list, store);

function xtz_to_token(const to_              : address;
                      const min_tokens_bought: nat;
                      const deadline         : timestamp;
                      var store              : store):                      
                      (list(operation) * store) is
  block {
    var op_list: list(operation) := nil;
    if (now < deadline) then block {
      const xtz_pool     : nat = mutez_to_natural(balance - amount);
      const nat_amount   : nat = mutez_to_natural(amount);
      const tokens_bought: nat = (nat_amount * 997n * store.s.token_pool) / (xtz_pool * 1000n + (nat_amount * 997n));
      
      if (tokens_bought >= min_tokens_bought) then block {
        store.s.token_pool        := int_to_nat("xtz_to_token: token_pool: int_to_nat: cannot convert a negative number to a nat.", store.s.token_pool - tokens_bought);

        // send tokens_withdrawn to to address
        // if tokens_bought is greater than store.s.token_pool, this will fail
        const token_contract: contract(token_contract_action) = get_contract(store.s.token_address);
        const op: operation = transaction(Transfer(self_address, to_, tokens_bought), 0mutez, token_contract);

        // append internal operations
        op_list := list op; end;
      } else block {
        failwith("xtz_to_token: Expected tokens_bought to be greater than or equal to min_tokens_bought.");
      }
    } else block {
      failwith("xtz_to_token: Expected now to be less than the deadline.");
    }
  } with (op_list, store);

function token_to_xtz(const owner         : address; // the address of the owner of FA1.2
                      const to_           : address;
                      const tokens_sold   : nat;
                      const min_xtz_bought: tez;
                      const deadline      : timestamp;
                      var store           : store):                      
                      (list(operation) * store) is
  block {
    var op_list: list(operation) := nil;
    if (now < deadline) then block {
      const xtz_bought : tez = natural_to_mutez((tokens_sold * 997n * balance) / natural_to_mutez(store.s.token_pool * 1000n + (tokens_sold * 997n)));
      if (xtz_bought >= min_xtz_bought) then block {
        store.s.token_pool      := store.s.token_pool + tokens_sold;

        // send xtz_bought to to_ address
        const to_contract: contract(unit) = get_contract(to_);
        const op1: operation = transaction(unit, xtz_bought, to_contract);

        // send tokens_sold to the exchange address
        // this assumes that the exchange has an allowance for the token and owner in FA1.2
        const token_contract: contract(token_contract_action) = get_contract(store.s.token_address);
        const op2: operation = transaction(Transfer(owner, self_address, tokens_sold), 0mutez, token_contract);

        // append internal operations
        op_list := list op1; op2; end;
      } else block {
        failwith("token_to_xtz: Expected xtz_bought to be greater than or equal to min_xtz_bought.");
      }
    } else block {
      failwith("token_to_xtz: Expected now to be less than the deadline.");
    }
  } with (op_list, store);
  
// update senders vote
function vote(const baker   : baker_address;
              const update_ : bool;
              var   store   : store):
              (list(operation) * store) is
  block {
    const op_list: list(operation) = nil;
    case (store.accounts[sender]) of
    | None          -> skip
    | Some(account) ->
        block {
          // update the baker that the owner chooses to vote for
          if (update_) then block {
            // skip;
            var baker_votes : nat := 0n;
            case (store.s.votes[baker]) of
              | Some(baker_votes_) -> baker_votes := baker_votes_
              | None -> skip
            end;
            store.s.votes := map_update(baker, Some(baker_votes + account.balance), store.s.votes);
          } else block {
            // the user is choosing to not vote, remove their old votes if they exist
            var baker_votes : nat := 0n;
            case (store.s.votes[baker]) of
              | Some(baker_votes_) ->
                block {
                  baker_votes := int_to_nat("vote: int_to_nat: cannot convert a negative number to a nat.", (baker_votes_ - account.balance));
                  if (baker_votes > 0n) then block {
                    store.s.votes := map_update(baker, Some(baker_votes), store.s.votes);
                  } else block {
                    store.s.votes := map_update(baker, no_votes, store.s.votes);
                  }
                }
              | None               -> skip
            end;
          };

          // elect baker
          const result: (list(operation) * store) = elect_baker(store);
          op_list := result.0;
          store   := result.1;
        }
    end;
  } with (op_list, store)

function main (const action : action ; const store : store) : (list(operation) * store) is
  (case action of
  | Approve(xs)         -> approve(xs.0,xs.1,xs.2,store)
  | AddLiquidity(xs)    -> add_liquidity(xs.0,xs.1,xs.2,xs.3,store)
  | RemoveLiquidity(xs) -> remove_liquidity(xs.0,xs.1,xs.2,xs.3,xs.4,xs.5,store)
  | XtzToToken(xs)      -> xtz_to_token(xs.0,xs.1,xs.2,store)
  | TokenToXtz(xs)      -> token_to_xtz(xs.0,xs.1,xs.2,xs.3,xs.4,store)
  | Vote(xs)            -> vote(xs.0,xs.1,store)
  end);
