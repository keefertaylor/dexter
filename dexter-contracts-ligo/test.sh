#!/bin/bash
# chmod u+x test.sh

ligo evaluate-value -s pascaligo gitlab-pages/docs/language-basics/src/variables-and-constants/const.ligo age

ligo compile-storage SOURCE_FILE ENTRY_POINT EXPRESSION

ligo compile-storage examples/counter.ligo main 5


ligo compile-parameter examples/counter.ligo main "Increment(5)"

# Outputs: (Right 5)

ligo compile-parameter exchange.ligo main "AddLiquidity(tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn,1,100,\"2020-06-29T18:00:21Z\")"
AddLiquidity(tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn,1,100,"2020-06-29T18:00:21Z")

tezos-client transfer 1 from alice to tezosGoldExchange --entrypoint 'addLiquidity' --arg 'Pair (Pair "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" 1) (Pair 100 "2020-06-29T18:00:21Z")' --burn-cap 1


ligo dry-run --amount=100 exchange.ligo main "AddLiquidity(tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn,1,100,\"2020-06-29T18:00:21Z\")" "Pair {} (Pair (Pair (Pair None KT1TG5hvwqKBHWoagJVcKjKhv6pM1QtDLD2m) (Pair 0 0)) {})"


ligo dry-run src/counter.ligo main "Increment(5)" 5


Pair {} (Pair (Pair (Pair None "KT1TG5hvwqKBHWoagJVcKjKhv6pM1QtDLD2m") (Pair 0 0)) {})

I tried this but  function increment (const i : int) : int is block { skip } with i + 1; is not  accepted...

type s is record
  votes: map(baker_address, nat); // baker address to vote counts
  current_baker: option(baker_address * nat);
  total_liquidity: nat;
  token_address: address;
  token_pool: nat;
end

type store is record
  s: s;
  accounts: big_map(address, account); // key_hash is the bakerthey vote fort
end

ss = record
votes = map end;
current_baker = None;
total_liquidty = 0;
token_address = "";
token_pool = 0;
end;
record
  s = ss;
  accounts = map end;
end;

```
ligo dry-run --amount=100 --sender=tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn exchange.ligo main "AddLiquidity((\"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn\" : address),1,100,\"2020-06-29T18:00:21Z\")" "ss = record
  votes = map end;
  current_baker = None;
"

ligo dry-run taco-shop.ligo --syntax pascaligo --amount 1 buy_taco 1n "map
    1n -> record
        current_stock = 50n;
        max_price = 50000000mutez;
    end;
    2n -> record
        current_stock = 20n;
        max_price = 75000000mutez;
    end;
end"

```


ligo dry-run --amount=100 --sender=tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn src/dynamic_storage.ligo mainDynamicStorage 'CallMethod(record
    owner = ("tz1ibMpWS6n6MJn73nQHtK5f4ogyYC1z9T9z" : address);
    value = 5n;
end)' 'record
    data = record
        balances = map ("tz1ibMpWS6n6MJn73nQHtK5f4ogyYC1z9T9z" : address) -> 0n; end;
        totalSupply = 0n;
    end;
    customMethod = function increment (const i : int) : int is block { skip } with i + 1;
end'


ligo dry-run taco-shop.ligo --syntax pascaligo --amount 1 buy_taco 1n "map
    1n -> record
        current_stock = 50n;
        max_price = 50000000mutez;
    end;
    2n -> record
        current_stock = 20n;
        max_price = 75000000mutez;
    end;
end"


ligo dry-run fa1.2.ligo --syntax pascaligo --amount=0 --sender=tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn main 'Approve(("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx": address), 100n)' 'record
  totalSupply = 1000000n;
  accounts = big_map 
    ("tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn": address) -> record
      balance = 1000000n;
      amounts = (map end : map(address, nat));
    end;
  end;
end
'


ligo dry-run fa1.2.ligo --syntax=pascaligo --amount=0 --sender=tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn main 'Transfer(("tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn": address), ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx": address), 100n)' 'record
  totalSupply = 1000000n;
  accounts = big_map 
    ("tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn": address) -> record
      balance = 1000000n;
      amounts = (map end : map(address, nat));
    end;
  end;
end
'



ligo dry-run fa1.2.ligo --format=json --syntax=pascaligo --amount=0 --sender=tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn main 'Approve(("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx": address), 100n)' 'record
totalSupply = 1000000n;
accounts = big_map 
("tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn": address) -> record
balance = 1000000n;
amounts = (map end : map(address, nat));
end;
end;
end
'


ligo dry-run fa1.2.ligo --format=json --syntax=pascaligo --amount=0 --sender=tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn main 'Transfer(("tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn": address), ("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx": address), 100n)' 'record
totalSupply = 1000000n;
accounts = big_map 
("tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn": address) -> record
balance = 1000000n;
allowances = (map end : map(address, nat));
end;
end;
end
'

ligo dry-run fa1.2.ligo --format=json --syntax=pascaligo --amount=0 --sender=tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn main 'Transfer(("tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn": address),("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx": address),100n)' 'record total_supply = 1000000n; accounts = big_map ("tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn": address) -> record balance = 1000000n; allowances = (map end : map(address, nat)); end; end; end'

ligo dry-run fa1.2.ligo --format=json --syntax=pascaligo --amount=0 --sender=tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn main 'Transfer(("tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn": address),("tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx": address),100n)' 'record total_supply = 1000000n; accounts = big_map ("tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn": address) -> record balance = 1000000n; allowances = (map end : map(address, nat)); end; end;; end'

ligo dry-run fa1.2.ligo --format=json --syntax=pascaligo --amount=0 --sender=tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn main "Transfer((\"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn\": address),(\"tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx\": address),100n)" "record total_supply = 1000000n; accounts = big_map (\"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn\": address) -> record balance = 1000000n; allowances = (map end : map(address, nat)); end; end; end"


ligo dry-run fa1.2.ligo --format=json --syntax=pascaligo --amount=0 --sender=tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn main "Transfer((\"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn\": address),(\"tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx\": address),100n)" "record total_supply = 1000000n; accounts = big_map (\"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn\": address) -> record balance = 1000000n; allowances = (map end : map(address, nat)); end; end; end"






AT, --display-format=DISPLAY_FORMAT
       (absent=human-readable)
           DISPLAY_FORMAT is the format that will be used by the CLI.
           Available formats are 'dev', 'json', and 'human-readable'
           (default). When human-readable lacks details (we are still
           tweaking it), please contact us and use another format in the
           meanwhile.
