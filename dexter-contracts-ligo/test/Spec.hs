{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Data.Map.Strict as Map
import           Data.Text (Text)
import qualified Ligo.FA1_2 as Token
import           Ligo.Test
import           Numeric.Natural (Natural)
import           Test.Hspec
import           Test.Hspec.Attoparsec

import            Ligo.Account (alice, bob, simon)

action1 :: Token.Action
action1 =
  Token.Transfer
  alice
  bob
  100

storage1 :: Token.Storage
storage1 =
  Token.Storage
  1000000
  (BigMap "address" "account" $
   Map.fromList
   [( alice
    , Token.Account 1000000 (fromList "address" "nat" []))])

main :: IO ()
main =
  hspec $ do
    Token.transferSpec
    Token.approveSpec
    
    parallel $ describe "ligo to text" $ do
      it "convert Address" $ do
        fromLigo (LComparable (LAddress alice)) `shouldBe` Right alice
        fromLigo (LComparable (LAddress simon)) `shouldBe` Right simon

      it "convert Integer" $ do
        fromLigo (LComparable (LInt (-100))) `shouldBe` Right (-100 :: Integer)
        fromLigo (LComparable (LInt 0)) `shouldBe` Right (0 :: Integer)
        fromLigo (LComparable (LInt 100)) `shouldBe` Right (100 :: Integer)
      
      it "convert Natural" $ do
        fromLigo (LComparable (LNatural 0)) `shouldBe` Right (0 :: Natural)
        fromLigo (LComparable (LNatural 100)) `shouldBe` Right (100 :: Natural)

      it "convert Text" $ do
        fromLigo (LComparable (LString "Hello")) `shouldBe` Right ("Hello" :: Text)
        fromLigo (LComparable (LString "Goodbye")) `shouldBe` Right ("Goodbye" :: Text)

      it "convert Map Text Integer" $ do
        fromLigo (LMap (Map "" "" $ Map.fromList [(LString "a", LComparable (LInt 1)),(LString "b", LComparable (LInt 2))])) `shouldBe` Right (fromList "" "" [("a", 1),("b", 2)] :: Map Text Integer)

      it "convert BigMap Text Integer" $ do
        fromLigo (LBigMap (BigMap "" "" $ Map.fromList [(LString "a", LComparable (LInt 1)),(LString "b", LComparable (LInt 2))])) `shouldBe` Right (BigMap "" "" $ Map.fromList [("a", 1),("b", 2)] :: BigMap Text Integer)


    parallel $ describe "parsing dryRunLigo output" $ do 
      it "should parse addresses" $ do
        ("address \"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn\"" :: Text) ~> parseLigo `shouldParse` (LComparable $ LAddress alice)
        ("      address    \"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn\"" :: Text) ~> parseLigo `shouldParse` (LComparable $ LAddress alice)
        ("address    \"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn\"   " :: Text) ~> parseLigo `shouldParse` (LComparable $ LAddress alice)

      it "should natural numbers" $ do
        ("+0" :: Text) ~> parseLigo `shouldParse` (LComparable $ LNatural 0)
        ("+100" :: Text) ~> parseLigo `shouldParse` (LComparable $ LNatural 100)

      it "should parse records" $ do
        ("{balance = +100 , total = +200}" :: Text) ~> parseLigo `shouldParse` (LRecord "" . Map.fromList $ [("balance", LComparable $ LNatural 100),("total", LComparable $ LNatural 200)])


      it "should parse big_map" $ do        
        ("big_map[]" :: Text) ~> parseLigo `shouldParse` (LBigMap . BigMap "" "" . Map.fromList $ [])

        ("big_map[ address \"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn\" -> +100 ]" :: Text) ~> parseBigMap `shouldParse` (LBigMap . BigMap "" "" . Map.fromList $ [(LAddress alice, LComparable $ LNatural 100)])

        ("big_map[ +1 -> +100 ]" :: Text) ~> parseBigMap `shouldParse` (LBigMap . BigMap "" "" . Map.fromList $ [(LNatural 1, LComparable $ LNatural 100)])

        ("big_map[ +1 -> address \"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn\" ]" :: Text) ~> parseBigMap `shouldParse` (LBigMap . BigMap "" "" . Map.fromList $ [(LNatural 1, LComparable $ LAddress alice)])

        ("big_map[ address \"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn\" -> address \"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn\" ]" :: Text) ~> parseBigMap `shouldParse` (LBigMap . BigMap "" "" . Map.fromList $ [(LAddress alice, LComparable $ LAddress alice)])

        ("big_map[ address \"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn\" -> {balance = +999900 , allowances = []} ; address \"tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3\" -> {balance = +100 , allowances = []} ]" :: Text) ~> parseLigo `shouldParse`
          (LBigMap . BigMap "" "" . Map.fromList $
           [ (LAddress alice, LRecord "" . Map.fromList $ [("balance", LComparable $ LNatural 999900), ("allowances", LMap (fromList "" "" []))])
           , (LAddress simon, LRecord "" . Map.fromList $ [("balance", LComparable $ LNatural 100),    ("allowances", LMap (fromList "" "" []))])
           ])

    parallel $ describe "dryRunLigo" $ do
      it "return result in Ligo data type" $ do
        result1 <- dryRunLigo "fa1.2.ligo" 0 alice action1 storage1
        result1 `shouldBe`
          Right (
          DryRunSuccess
          ( []
          , LRecord
            ""
            (Map.fromList
             [("accounts",
               LBigMap
               (BigMap "" ""
                (Map.fromList
                 [(LAddress (Address "tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"),
                   LRecord
                   ""
                   (Map.fromList
                     [("allowances",LMap (Map "" "" (Map.fromList [])))
                     ,("balance",LComparable (LNatural 100))])),
                   (LAddress (Address "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"),
                    LRecord
                    ""
                    (Map.fromList
                     [("allowances",LMap (Map "" "" (Map.fromList [])))
                     ,("balance",LComparable (LNatural 999900))]))]))),
               ("total_supply",
                LComparable (LNatural 1000000))])))
