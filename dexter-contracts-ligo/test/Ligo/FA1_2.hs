{-# LANGUAGE OverloadedStrings #-}

module Ligo.FA1_2 where

import qualified Data.Aeson           as A
import qualified Data.Aeson.Types     as A
import qualified Data.Text    as T
import Ligo.Account (alice, bob, simon)
import Ligo.Test
import Numeric.Natural (Natural)
import qualified Data.Map.Strict as Map
import Data.Text (Text)
import Safe (headMay)
import Test.Hspec

data Action
  = Transfer       Address Address Natural
  | Approve        Address Natural
  | GetAllowance   Address Address Natural
  | GetBalance     Address Address
  | GetTotalSupply ()      Address

data Account =
  Account
    { balance :: Natural
    , allowances :: Map Address Natural
    } deriving (Eq, Read, Show)

data Storage =
  Storage
    { totalSupply :: Natural
    , accounts    :: BigMap Address Account
    } deriving (Eq, Read, Show)

instance ToLigo Action where
  toLigo (Transfer       a1 a2 n) = entrypoint "Transfer"       [toLigo a1, toLigo a2, toLigo n]
  toLigo (Approve        a1 n)    = entrypoint "Approve"        [toLigo a1, toLigo n]
  toLigo (GetAllowance   a1 a2 n) = entrypoint "GetAllowance"   [toLigo a1, toLigo a2, toLigo n] 
  toLigo (GetBalance     a1 a2)   = entrypoint "GetBalance"     [toLigo a1, toLigo a2]
  toLigo (GetTotalSupply u  a1)   = entrypoint "GetTotalSupply" [toLigo u, toLigo a1]

instance ToLigo Account where
  toLigo (Account b a) = record "account" ["balance" .= b , "allowances" .= a]

instance ToLigo Storage where
  toLigo (Storage t a) = record "storage" ["total_supply" .= t, "accounts" .= a ] 

instance FromLigo Account where
  fromLigo = withRecord "account" $ \r ->
    Account <$> r .: "balance"
            <*> r .: "allowances"

instance FromLigo Storage where
  fromLigo = withRecord "storage" $ \r ->
    Storage <$> r .: "total_supply"
            <*> r .: "accounts"

transfer1 :: Action
transfer1 = Transfer alice bob 100

withoutTypeTags :: Storage -> Storage
withoutTypeTags storage =
  case accounts storage of
    BigMap _ _ innerBigMap ->
      let innerBigMap' = fmap (\account ->
                                 case allowances account of
                                   Map _ _ innerMap -> account { allowances = Map "" "" innerMap }
                              ) innerBigMap
      in storage { accounts = BigMap "" "" innerBigMap' } 

storage1 :: Storage
storage1 =
  Storage
  1000000
  (BigMap "address" "account" $
   Map.fromList
   [( alice
    , Account 1000000 (fromList "address" "nat" []))])

storage2 :: Storage
storage2 =
  Storage
  10
  (BigMap "address" "account" $
   Map.fromList
   [( alice
    , Account 10 (fromList "address" "nat" []))])

storage3 :: Storage
storage3 =
  Storage
  1000000
  (BigMap "address" "account" $
   Map.fromList
   [( alice
    , Account 1000000 (fromList "address" "nat" [(bob, 100)]))])

setBigMap :: Storage -> [(Address, Account)] -> Storage
setBigMap storage newAccounts =
  storage { accounts = BigMap "" "" (Map.fromList newAccounts) }

getFA1_2Failure :: DryRunError -> Maybe Text
getFA1_2Failure err =
  let children = eChildren err in
  case headMay children of
    Nothing -> Nothing
    Just child ->
      case withError . cMessage $ child of
        (A.Object o) -> A.parseMaybe (\obj -> obj A..: "string") o
        _ -> Nothing


runFA1_2 :: Address -> Action -> Storage -> IO (Either Text Storage)
runFA1_2 sender action storage = do
  eLigoResult <- dryRunLigo "fa1.2.ligo" 0 sender action storage
  case eLigoResult of
    Left  err                        -> pure . Left $ ("ligo dry-run fa1.2.ligo failed: " <> err)
    Right (DryRunSuccess ligoResult) -> pure . fromLigo . snd $ ligoResult
    Right (DryRunFailure err) ->
      case getFA1_2Failure err of
        Just err' -> pure . Left $ err'
        Nothing  -> pure . Left . T.pack . show $ err

transferSpec :: Spec
transferSpec =
  parallel . describe "Transfer" $ do
    it "transfer token" $ do
      runFA1_2 alice transfer1 storage1 >>=
        flip shouldBe
        (Right
         (setBigMap
          storage1
          [( alice
           , Account 999900 (fromList "" "" [])),
           ( bob
           , Account 100 (fromList "" "" []))
          ]))

    it "transfer zero tokens" $
      runFA1_2 alice (Transfer alice bob 0) storage1 >>=
        flip shouldBe
        (Right
         (setBigMap
          storage1
          [( alice
           , Account 1000000 (fromList "" "" [])),
           ( bob
           , Account 0 (fromList "" "" []))
          ]))

    it "transfer tokens to self" $
      runFA1_2 alice (Transfer alice alice 100) storage1 >>=
        flip shouldBe (Right . withoutTypeTags $ storage1)

    it "transfer more tokens than you have" $
      runFA1_2 alice transfer1 storage2 >>=
        flip shouldBe (Left "transfer: owner balance is too low")

    -- what should the behavior of the owner sending tokens to them self be?
    -- it "transfer more tokens than you have to your self" $
    --   runFA1_2 alice (Transfer alice alice 10000000) storage1 >>=
    --     flip shouldBe (Left "transfer: owner balance is too low")

approveSpec :: Spec
approveSpec =
  parallel . describe "Approve" $ do
    it "cannot transfer tokens without approval" $ do
      runFA1_2 bob (Transfer alice simon 100) storage1 >>=
        flip shouldBe (Left "Sender not allowed to spend token from owner")
      
    it "can transfer tokens with approval" $ do
      runFA1_2 bob (Transfer alice simon 100) storage3 >>=
        flip shouldBe
        (Right
         (setBigMap
          storage3
          [( alice
           , Account 999900 (fromList "" "" [(bob, 0)])),
           ( simon
           , Account 100 (fromList "" "" []))
          ]))        

    it "cannot transfer amount of tokens that exceed the approval amount" $ do
      runFA1_2 bob (Transfer alice simon 200) storage3 >>=
        flip shouldBe (Left "Sender not allowed to spend token from owner")

    it "can approve an allowance then perform a transfer" $ do 
      runFA1_2 alice (Approve bob 100) storage1 >>= \storageWithAllowance
        runFA1_2 bob (Transfer alice simon 100) storageWithAllowance >>=
        flip shouldBe
        (Right
         (setBigMap
          storageWithAllowance
          [( alice
           , Account 999900 (fromList "" "" [(bob, 0)])),
           ( simon
           , Account 100 (fromList "" "" []))
          ]))                
