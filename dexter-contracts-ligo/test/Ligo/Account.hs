{-# LANGUAGE OverloadedStrings #-}

module Ligo.Account where

import Ligo.Test (Address(Address))

alice :: Address
alice = Address "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"

bob :: Address
bob = Address "tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"

simon :: Address
simon = Address "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3"
