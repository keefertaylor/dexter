#!/bin/bash
# chmod u+x compile.sh

ligo compile-contract exchange.ligo main | dos2unix > contracts/exchange.tz
ligo compile-contract --michelson-format=json exchange.ligo main | dos2unix > contracts/exchange.json

ligo compile-contract fa1.2.ligo main | dos2unix > contracts/fa1.2.tz
ligo compile-contract --michelson-format=json fa1.2.ligo main | dos2unix > contracts/fa1.2.json

rm exchange.pp.ligo
rm fa1.2.pp.ligo

# x=0
# prev=""
# prev2=""
# out="./contracts/exchange-clean.tz"
# echo "" > "$out"

# while IFS= read -r line
# do
#     # there are three cases

#     # PUSH string, "" ;, and FAILWITH are on the same line
#     if grep -q "PUSH string \".*\" ; FAILWITH" <<< "$line"; then
#         NEW_LINE="$(sed "s/string \".*\"/nat $x/g" <<< "$line")"
#         echo "$NEW_LINE" >> "$out"
#         # increment index
#         x=$((x+1))
#         # reset prev
#         prev=""
#         prev2=""
        
#     # PUSH string and "" ; are on the same line, capture these, then check that the next line is FAILWITH
#     elif grep -q "PUSH string \".*\" ;" <<< "$line"; then
#         prev="$line"

#     elif [ -z "$prev2" ] && [ -n "$prev" ] && grep -q "FAILWITH" <<< "$line"; then
#         NEW_LINE="$(sed "s/string \".*\"/nat $x/g" <<< "$prev")"
#         echo "$NEW_LINE" >> "$out"
#         echo "$line" >> "$out"
#         # increment index
#         x=$((x+1))
#         # reset prev
#         prev=""
#         prev2=""

#     # PUSH string and "" ; are on the separate lines
#     elif grep -q "PUSH string" <<< "$line"; then
#         prev2="$line"

#     elif [ -n "$prev2" ] && grep -q "\".*\" ;" <<< "$line"; then
#         prev="$line"

#     elif [ -n "$prev2" ] && [ -n "$prev" ] && grep -q "FAILWITH" <<< "$line"; then
#         NEW_LINE2="$(sed "s/string/nat/g" <<< "$prev2")"
#         NEW_LINE="$(sed "s/\".*\"/$x/g" <<< "$prev")"
#         echo "$NEW_LINE2" >> "$out"
#         echo "$NEW_LINE" >> "$out"
#         echo "$line" >> "$out"
#         # increment index
#         x=$((x+1))
#         # reset prev
#         prev=""
#         prev2=""

#     else
#         if [ -n "$prev2" ]; then
#             echo "$prev2" >> "$out"
#         fi
#         if [ -n "$prev" ]; then
#             echo "$prev" >> "$out"
#         fi
#         echo "$line" >> "$out"
#         # reset prev
#         prev=""
#         prev2=""
        
#     fi
# done < ./contracts/exchange.tz
