{-# LANGUAGE OverloadedStrings #-}

module Ligo.Test where

import           Control.Applicative  ((<|>))

import qualified Data.Aeson           as A
import           Data.Attoparsec.Text
import qualified Data.ByteString.Lazy as BS
import           Data.Char            (isNumber)
import           Data.Either          (either)
import qualified Data.Map.Strict      as Map
import           Data.Text            (Text)
import qualified Data.Text            as T
import qualified Data.Text.Encoding   as T

import           Numeric.Natural (Natural)

import           Prelude hiding (takeWhile)

import           Safe            (readMay)

import           Test.QuickCheck (Arbitrary(..), Gen, elements, vectorOf)
import           Turtle          (empty, procStrict)

data Address = Address Text deriving (Eq, Ord, Read, Show)

instance Arbitrary Address where
  arbitrary = do
    r <- vectorOf 33 arbitraryBase58Char
    pure $ Address ("tz1" <> T.pack r)

arbitraryBase58Char :: Gen Char
arbitraryBase58Char = elements "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"

data Map k v = Map Text Text (Map.Map k v) deriving (Eq, Ord, Read, Show)

toStrict :: Map k v -> Map.Map k v
toStrict (Map _ _ m) = m

fromList :: (Ord k) => Text -> Text -> [(k, v)] -> Map k v
fromList keyType valueType = Map keyType valueType . Map.fromList

data BigMap k v = BigMap Text Text (Map.Map k v) deriving (Eq, Ord, Read, Show)

data LigoOperation
  = LigoOperation
  deriving (Eq, Ord, Read, Show)

data LigoComparable
  = LAddress Address
  | LInt     Integer
  | LNatural Natural
  | LString  Text
  deriving (Eq, Ord, Read, Show)

-- naive
data Ligo
  = LEntryPoint Text [Ligo]
  | LRecord     Text (Map.Map Text Ligo)
  | LComparable LigoComparable
  | LUnit
  | LBigMap     (BigMap LigoComparable Ligo)
  | LMap        (Map LigoComparable Ligo)
  deriving (Eq, Ord, Read, Show)

class ToLigoComparable a where
  toLigoComparable :: a -> LigoComparable

instance ToLigoComparable Address where
  toLigoComparable = LAddress

instance ToLigoComparable Integer where
  toLigoComparable = LInt

instance ToLigoComparable Text where
  toLigoComparable = LString

instance ToLigoComparable Natural where
  toLigoComparable = LNatural

class ToLigo a where
  toLigo :: a -> Ligo

instance ToLigo Address where
  toLigo = LComparable . toLigoComparable

instance ToLigo Integer where
  toLigo = LComparable . toLigoComparable

instance ToLigo Text where
  toLigo = LComparable . toLigoComparable

instance ToLigo Natural where
  toLigo = LComparable . toLigoComparable

instance ToLigo () where
  toLigo _ = LUnit

instance (ToLigoComparable k, ToLigo v) => ToLigo (Map k v) where
  toLigo (Map kType vType m) = LMap . Map kType vType . Map.fromList . fmap (\(k, v) -> (toLigoComparable k, toLigo v)) $ Map.toList m

instance (ToLigoComparable k, ToLigo v) => ToLigo (BigMap k v) where
  toLigo (BigMap kType vType m) = LBigMap . BigMap kType vType . Map.fromList . fmap (\(k, v) -> (toLigoComparable k, toLigo v)) $ Map.toList m

(.=) :: ToLigo a => Text -> a -> (Text, Ligo)
(.=) label value = (label, toLigo value)
infixr 8 .=

withRecord :: Text -> (Map.Map Text Ligo -> Either Text a) -> Ligo -> Either Text a
withRecord _ f (LRecord _ m) = f m
withRecord recordName _ ligo = Left ("expected " <> recordName <> ", but encountered " <> (T.pack . show $ ligo))
  
(.:) :: FromLigo a => Map.Map Text Ligo -> Text -> Either Text a
(.:) m key =
  case Map.lookup key m of
    Nothing -> Left ("Did not find a value for key '" <> key <> "' in " <> (T.pack . show $ m) <> ".")
    Just value -> fromLigo value
infixr 8 .:
  
class FromLigoComparable a where
  fromLigoComparable :: LigoComparable -> Either Text a

instance FromLigoComparable Address where
  fromLigoComparable (LAddress address) = Right address
  fromLigoComparable ligo = Left ("Expected LAddress 'address', found: " <> (T.pack . show $ ligo))

instance FromLigoComparable Integer where
  fromLigoComparable (LInt int) = Right int
  fromLigoComparable ligo = Left ("Expected LInteger <Integer>, found: " <> (T.pack . show $ ligo))

instance FromLigoComparable Natural where
  fromLigoComparable (LNatural natural) = Right natural
  fromLigoComparable ligo = Left ("Expected LNatural <Natural>, found: " <> (T.pack . show $ ligo))

instance FromLigoComparable Text where
  fromLigoComparable (LString text) = Right text
  fromLigoComparable ligo = Left ("Expected LString 'text', found: " <> (T.pack . show $ ligo))

class FromLigo a where
  fromLigo :: Ligo -> Either Text a

instance FromLigo Address where
  fromLigo (LComparable (LAddress address)) = Right address
  fromLigo ligo = Left ("Expected LComparable LAddress 'address', found: " <> (T.pack . show $ ligo))

instance FromLigo Integer where
  fromLigo (LComparable (LInt int)) = Right int
  fromLigo ligo = Left ("Expected LComparable LInt <Integer>, found: " <> (T.pack . show $ ligo))

instance FromLigo Natural where
  fromLigo (LComparable (LNatural natural)) = Right natural
  fromLigo ligo = Left ("Expected LComparable LNatural <Natural>, found: " <> (T.pack . show $ ligo))

instance FromLigo Text where
  fromLigo (LComparable (LString text)) = Right text
  fromLigo ligo = Left ("Expected LComparable LString 'text', found: " <> (T.pack . show $ ligo))

instance FromLigo () where
  fromLigo LUnit = Right ()
  fromLigo ligo  = Left ("Expected LUnit, found: " <> (T.pack . show $ ligo))

instance (Ord k, FromLigoComparable k, FromLigo v) => FromLigo (Map k v) where
  fromLigo (LMap (Map kType vType m)) =
    let eMap = mapM (\(k, v) -> do
                        k' <- fromLigoComparable k
                        v' <- fromLigo v
                        pure (k', v')
                    ) (Map.toList m)
    in case eMap of
      Left  err  -> Left err
      Right map' -> Right . Map kType vType . Map.fromList $ map'
  fromLigo ligo  = Left ("Expected LMap, found: " <> (T.pack . show $ ligo))

instance (Ord k, FromLigoComparable k, FromLigo v) => FromLigo (BigMap k v) where
  fromLigo (LBigMap (BigMap kType vType m)) =
    let eMap = mapM (\(k, v) -> do
                        k' <- fromLigoComparable k
                        v' <- fromLigo v
                        pure (k', v')
                    ) (Map.toList m)
    in case eMap of
      Left  err  -> Left err
      Right map' -> Right . BigMap kType vType . Map.fromList $ map'
  fromLigo ligo  = Left ("Expected LBigMap, found: " <> (T.pack . show $ ligo))
   
-- =============================================================================
-- Convert Ligo type into pascalligo
-- =============================================================================

getTypeName :: Ligo -> Text
getTypeName (LEntryPoint _ _)          = ""
getTypeName (LRecord typeName _)       = typeName
getTypeName (LComparable (LAddress _)) = "address"
getTypeName (LComparable (LInt     _)) = "int"
getTypeName (LComparable (LNatural _)) = "nat"
getTypeName (LComparable (LString  _)) = "string"
getTypeName LUnit                      = "unit"
getTypeName (LBigMap  (BigMap keyTypeName valueTypeName _)) = "big_map(" <>  keyTypeName <> ", " <> valueTypeName <> ")"
getTypeName (LMap     (Map keyTypeName valueTypeName _))    = "map(" <> keyTypeName <> ", " <> valueTypeName <> ")"

encode :: Ligo -> Text
encode ligo =
  let encoded = encode' ligo
      l = T.last encoded
  in  if l == ';' then T.init encoded else encoded

encodeComparable :: LigoComparable -> Text
encodeComparable (LAddress (Address address)) = "(\"" <> address <> "\": address)"
encodeComparable (LInt     i)                 = T.pack . show $ i
encodeComparable (LNatural n)                 = (T.pack . show $ n) <> "n"
encodeComparable (LString  t)                 = "\"" <> t <> "\""

encode' :: Ligo -> Text
encode' (LEntryPoint entryPoint parameters) = entryPoint <> "(" <>  T.intercalate ", " (fmap encode' parameters) <> ")"
encode' (LRecord _typeName m)        = "record " <> T.intercalate "; " (fmap (\(k,v) -> k <> " = " <> encode' v) $ Map.toList m) <> "; end;"
encode' (LComparable c)              = encodeComparable c
encode' LUnit                        = "unit"
encode' b@(LBigMap  (BigMap  _ _ m)) =
  if Map.null m
  then "(big_map end : " <> getTypeName b <> ")"
  else "big_map " <> (T.concat ((\(k, v) -> encodeComparable k <> " -> " <> encode' v) <$> Map.toList m)) <> " end"

encode' l@(LMap     (Map _ _ m))   =
  if Map.null m
  then "(map end : " <> getTypeName l <> ")"
  else "map " <> (T.concat ((\(k, v) -> encodeComparable k <> " -> " <> encode' v) <$> Map.toList m)) <> " end"


-- =============================================================================
-- Convert ligo dry-run output to the Ligo type
-- =============================================================================

parseAddress :: Parser LigoComparable
parseAddress = do
  _ <- many' space
  _ <- string "address"
  _ <- many1 space
  _ <- char '"'
  address <- takeTill (\c -> c == '"')
  _ <- char '"'
  pure . LAddress . Address $ address

parseNatural :: Parser LigoComparable
parseNatural = do
  _ <- many' space
  _ <- char '+'
  natString <- takeWhile isNumber
  case readMay . T.unpack $ natString of
    Just nat -> pure . LNatural $ nat
    Nothing  -> fail "parseNatural"

parseLigoComparable :: Parser LigoComparable
parseLigoComparable = parseAddress <|> parseNatural

parseRecord :: Parser Ligo
parseRecord = do
  _ <- many' space
  _ <- char '{'
  _ <- many' space
  keyValuePairs <- parseRecordKeyValue `sepBy` (char ',')
  _ <- many' space
  _ <- char '}'
  pure $ LRecord "" (Map.fromList keyValuePairs)

parseRecordKeyValue :: Parser (Text, Ligo)
parseRecordKeyValue = do
  _ <- many' space
  key <- underscoredWord
  _ <- many' space
  _ <- char '='
  _ <- many' space
  value <- parseLigo
  _ <- many' space

  pure (T.pack key, value)
  where
    underscoredWord = many' (letter <|> char '_')

parseBigMap :: Parser Ligo
parseBigMap = do
  _ <- many' space
  _ <- string "big_map"
  _ <- many' space
  _ <- char '['
  _ <- many' space  
  keyValues <- parseMapKeyValue `sepBy` (char ';')
  _ <- many' space
  _ <- char ']'
  pure (LBigMap . BigMap "" "" . Map.fromList $ keyValues)

parseMapKeyValue :: Parser (LigoComparable, Ligo)
parseMapKeyValue = do
  _   <- many' space
  key <- parseLigoComparable
  _   <- many' space
  _   <-  string "->"
  value <- parseLigo
  _   <- many' space
  pure (key, value)

-- Map

parseMap :: Parser Ligo
parseMap = do
  _ <- many' space
  _ <- char '['
  _ <- many' space
  keyValues <- parseMapKeyValue `sepBy` (char ';')
  _ <- many' space
  _ <- char ']'
  pure (LMap $ fromList "" "" keyValues)

parseLigo :: Parser Ligo
parseLigo = do
  (LComparable <$> parseLigoComparable) <|> parseBigMap <|> parseMap <|> parseRecord

parseOperationStoragePair :: Parser ([LigoOperation], Ligo)
parseOperationStoragePair = do
  _ <- many' space    
  _ <- char '('
  _ <- many' space
  _ <- string "[]"
  _ <- many' space  
  _ <- char ','
  storage <- parseLigo
  _ <- many' space  
  _ <- char ')'
  _ <- many' space    
  pure ([], storage)

decode :: Text -> Either Text Ligo
decode t =
  case parseOnly (parseLigo <* endOfInput) t of
    Left  s -> Left . T.pack $ s
    Right l -> Right l

decodeDryRun :: Text -> Either Text ([LigoOperation], Ligo)
decodeDryRun t =
  case parseOnly (parseOperationStoragePair <* endOfInput) t of
    Left  s -> Left . T.pack $ s
    Right l -> Right l

-- =============================================================================
-- ligo dry-run
-- =============================================================================

entrypoint :: Text -> [Ligo] -> Ligo
entrypoint e = LEntryPoint e

record :: Text -> [(Text, Ligo)] -> Ligo
record typeName = LRecord typeName . Map.fromList

doubleQuoted :: Text -> Text
doubleQuoted t = "\"" <> t <> "\""

previewDryRunLigo :: (ToLigo a, ToLigo b) => Text -> Natural -> Address -> a -> b -> Text
previewDryRunLigo source amount (Address sender) action storage = 
 "ligo " <>  (
   T.intercalate
   " "
   [ "dry-run"
   , source
   , "--format=json"
   , "--syntax=pascaligo"
   , "--amount=" <> (T.pack . show $ amount)
   , "--sender=" <> sender
   , "main"
   , doubleQuoted (encode . toLigo $ action)
   , doubleQuoted (encode . toLigo $ storage) ])

dryRunLigo :: (ToLigo a, ToLigo b) => Text -> Natural -> Address -> a -> b -> IO (Either Text DryRunResult)
dryRunLigo source amount (Address sender) action storage = do
  (_exitCode, output) <-
    procStrict
    "unbuffer"
    [ "ligo"
    , "dry-run"
    , source
    , "--format=json"
    , "--syntax=pascaligo"
    , "--amount=" <> (T.pack . show $ amount)
    , "--sender=" <> sender
    , "main"
    , encode . toLigo $ action
    , encode . toLigo $ storage ]
    empty

  -- for some reason if there is an error it prepends the json output with 'ligo: '
  let jsonOutput =
        if T.length output > 6
        then
          let prefix = T.take 6 output in
            if prefix == "ligo: " then T.drop 6 output else output
        else output

  pure (maybe (Left output) Right (A.decode . BS.fromStrict . T.encodeUtf8 $ jsonOutput))

data DryRunErrorMessage =
  DryRunErrorMessage
    { withError :: A.Value
    } deriving (Eq, Read, Show)

instance A.FromJSON DryRunErrorMessage where
  parseJSON = A.withObject "DryRunErrorMessage" $ \o -> do
    DryRunErrorMessage <$> o A..: "with"    

data DryRunErrorChild =
  DryRunErrorChild
    { cTitle   :: Text
    , cMessage :: DryRunErrorMessage
    } deriving (Eq, Read, Show)

instance A.FromJSON DryRunErrorChild where
  parseJSON = A.withObject "DryRunErrorChild" $ \o -> do
    embeddedString <- o A..: "message"
    p <- case embeddedString of
      A.String s ->
        case A.decode . BS.fromStrict . T.encodeUtf8 $ s of
          Just p -> pure p
          Nothing -> fail "expected string"
      _ -> fail "expected string"
    DryRunErrorChild <$> o A..: "title"
                     <*> pure p

data DryRunError =
  DryRunError
    { etitle    :: Text
    , eType     :: Text
    , eChildren :: [DryRunErrorChild]
    } deriving (Eq, Read, Show)

instance A.FromJSON DryRunError where
  parseJSON = A.withObject "DryRunError" $ \o -> do
    DryRunError <$> o A..: "title"
                <*> o A..: "type"
                <*> o A..: "children"

data DryRunResult
  = DryRunSuccess ([LigoOperation], Ligo)
  | DryRunFailure DryRunError
  deriving (Eq, Read, Show)

instance A.FromJSON DryRunResult where
  parseJSON = A.withObject "DryRunResult" $ \o -> do
    status   <- o A..: "status"
    case status of
      "ok"    -> do
        content' <- o A..: "content"
        DryRunSuccess <$> (either (fail "parse content failed") pure (decodeDryRun content'))
      "error" -> DryRunFailure <$> o A..: "content"
      _ -> fail . T.unpack $ ("unexpected status: " <> status)
