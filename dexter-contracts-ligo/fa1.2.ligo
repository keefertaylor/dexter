// https://gitlab.com/tzip/tzip/blob/master/Proposals/TZIP-0005/FA1.md
// https://gitlab.com/tzip/tzip/blob/master/Proposals/TZIP-0007/FA1.2.md
// https://docs.google.com/document/d/1YLPtQxZu1UAvO9cZ1O2RPXBbT0mooh4DYKjA_jp-RLM/edit

// This is an implimentation of the FA1.2.1 specification in PascaLIGO

type account is record
  balance : nat;
  allowances: map(address, nat);
end

type store is record
  total_supply: nat;
  accounts: big_map(address, account);
end

type action is
| Transfer of (address * address * nat)
| Approve of (address * nat)
| GetAllowance of (address * address * contract(nat))
| GetBalance of (address * contract(nat))
| GetTotalSupply of (unit * contract(nat))

function is_allowed (const owner : address ; const value : nat ; var s : store) : bool is 
  block {
    var allowed: bool := False;
    if sender =/= owner then block {
      // Checking if the sender is allowed to spend in name of owner
      case (s.accounts[owner]) of
        | None      -> failwith("is_allowed: the owner does not own any tokens.")
        | Some(owner_account) -> block {
          case (owner_account.allowances[sender]) of
            | None -> allowed := False
            | Some(allowance_amount) -> allowed := allowance_amount >= value
          end;
        }
      end;
    };
    else allowed := True;
  } with allowed

function get_account (const error_source: string ; const owner : address ; const accounts: big_map(address, account)) : account is
  block { skip } with
    (case (accounts[owner]) of
      | Some(a) -> a
      | None    -> (failwith(error_source ^ ": owner does not exist in accounts") : account)
     end)

function get_allowance_amount (const error_source: string ; const owner : address ; const account : account) : nat is
  block {
    var allowance_amount : nat := 0n;
    if (owner =/= sender) then block {
      case (account.allowances[sender]) of
        | Some(a) -> allowance_amount := a
        | None    -> failwith(error_source ^ ": owner does not have an allowance")
      end
    } else block {
       allowance_amount := account.balance;
    }
  } with allowance_amount;

// this will fail if provided a negative number
function int_to_nat(const error_source: string ; const a: int): nat is
  block {
    var result : nat := 0n;
    if (a >= 0) then block {
      result := abs(a);
    } else block {
      failwith(error_source ^ ": int_to_nat: cannot convert a negative number to a nat.")
    };    
  } with result;

// Transfer a specific amount of tokens from the accountFrom address to a destination address
// Pre conditions:
//  The sender address is the account owner or is allowed to spend x in the name of accountFrom
//  The owner account has a balance higher than amount
// Post conditions:
//  The balance of owner is decreased by amount
//  The balance of destination is increased by amount
//  The allowance of the sender is decreased if the sender is not the owner
function transfer (const owner      : address ;
                   const to_        : address ;
                   const value      : nat  ;
                   var store        : store) :
                   store is
 block {
  // If owner = destination transfer is not necessary
  if owner = to_ then skip;
  else block {
    // Is sender allowed to spend value in the name of owner
    case is_allowed(owner, value, store) of
    | False -> failwith ("Sender not allowed to spend token from owner")
    | True -> skip
    end;
    
    // fetch owner account
    const owner_account: account = get_account("transfer", owner, store.accounts);

    // check that the owner can spend that much
    if value > owner_account.balance 
    then failwith ("transfer: owner balance is too low");
    else skip;

    // check that the allowance is large enough, if the owner is the sender then it is the
    // balance
    const allowance_amount: nat = get_allowance_amount("transfer", owner, owner_account);
    if value > allowance_amount
    then failwith ("transfer: allowance is too low");
    else skip;

    // update the owner's balance
    owner_account.balance := int_to_nat("transfer: balance cannot be negative", owner_account.balance - value);

    // decrease the allowance of sender in owner account if necessary
    if (owner =/= sender) then block {
       owner_account.allowances[sender] := int_to_nat("transfer", allowance_amount - value);
    } else skip;
    store.accounts[owner] := owner_account;

    // create an empty to_account
    var to_account: account := record 
      balance = 0n;
      allowances = (map end : map(address, nat));
    end;

    // fetch the account for the to_ address account
    case store.accounts[to_] of
    | None    -> skip
    | Some(n) -> to_account := n
    end;

    // update the to_account balance, set it in accounts
    to_account.balance  := to_account.balance + value;
    store.accounts[to_] := to_account;
  }
 } with store

// Approve an amount to be spent by another address in the name of the sender
// Pre conditions:
//  The spender account is not the sender account
// Post conditions:
//  The allowance of spender in the name of sender is value
function approve (const spender : address ;
                  const value   : nat ;
                  var   store   : store) :
                  store is
 block {
  // if sender is the spender approving is not necessary
  if sender = spender then skip;
  else block {
    const owner_account: account = get_account("approve", sender, store.accounts);
    owner_account.allowances[spender] := value;
    store.accounts[sender] := owner_account; // Not sure if this last step is necessary
  }
 } with store

// View function that forwards the allowance amount of spender in the name of tokenOwner to a contract
// Pre conditions:
//  None
// Post conditions:
//  The state is unchanged
function get_allowance (const owner   : address         ;
                        const spender : address         ;
                        const contr   : contract(nat);
                        var   store   : store) :
                        list(operation) is
 block {
  const owner_account: account = get_account("get_allowance", owner, store.accounts);
  const spender_allowance: nat = get_allowance_amount("get_allowance", spender, owner_account);
 } with list [transaction(spender_allowance, 0tz, contr)]

// View function that forwards the balance of owner to a contract
// Pre conditions:
//  None
// Post conditions:
//  The state is unchanged
function get_balance (const owner : address ;
                      const contr : contract(nat) ;
                      var   store : store) :
                      list(operation) is
 block {
  const owner_account: account = get_account("get_account", owner, store.accounts);
 } with list [transaction(owner_account.balance, 0tz, contr)]

// View function that forwards the total_supply to a contract
// Pre conditions:
//  None
// Post conditions:
//  The state is unchanged
function get_total_supply (const contr : contract(nat) ;
                           var   store : store) :
                           list(operation) is
  list [transaction(store.total_supply, 0tz, contr)]

function main (const p : action ;
               const s : store) :
  (list(operation) * store) is
 block { 
   // Reject any transaction that try to transfer token to this contract
   if amount =/= 0tz then failwith ("This contract do not accept token");
   else skip;
  } with case p of
  | Transfer(n) -> ((nil : list(operation)), transfer(n.0, n.1, n.2, s))
  | Approve(n) -> ((nil : list(operation)), approve(n.0, n.1, s))
  | GetAllowance(n) -> (get_allowance(n.0, n.1, n.2, s), s)
  | GetBalance(n) -> (get_balance(n.0, n.1, s), s)
  | GetTotalSupply(n) -> (get_total_supply(n.1, s), s)
 end