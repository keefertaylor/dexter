```bash
# you will need to change the contract addresses

# list the addresses
tezos-client list known addresses

simon: tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3 (unencrypted sk known)
bob: tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i (unencrypted sk known)
alice: tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn (unencrypted sk known)

# type check the fa1.2 contract
tezos-client typecheck script ./contracts/fa1.2.tz

# originate a fa1.2 contract
tezos-client originate contract tezosGold transferring 0 from alice running ./contracts/fa1.2.tz --init 'Pair {Elt "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" (Pair {} 1000000)} 1000000' --burn-cap 10 --force

# send 100 tezosGold from alice to simon
tezos-client transfer 0 from alice to tezosGold --entrypoint 'transfer' --arg 'Pair (Pair "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3") 100' --burn-cap 1

# get alice's tezosGold tokens
tezos-client get big map value for '"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"' of type 'address' in tezosGold

# get simon's tezosGold tokens
tezos-client get big map value for '"tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3"' of type 'address' in tezosGold

# type check the dexter exchange contract
tezos-client typecheck script ./contracts/exchange.tz
tezos-client typecheck script ./contracts/exchange-clean.tz

# list token contract addresses
tezos-client list known contracts | grep tezosGold
# tezosGold: KT1XjSoibW59pCAELdH9QEEgFzRb59MNbJiH

# originate a dexter exchange contract for tezosGold
tezos-client originate contract tezosGoldExchange transferring 0 from alice running ./contracts/exchange.tz --init 'Pair {} (Pair (Pair (Pair None "KT1TG5hvwqKBHWoagJVcKjKhv6pM1QtDLD2m") (Pair 0 0)) {})' --burn-cap 20 --force

tezos-client originate contract tezosGoldExchange transferring 0 from alice running ./contracts/exchange-clean.tz --init 'Pair {} (Pair (Pair (Pair None "KT1TG5hvwqKBHWoagJVcKjKhv6pM1QtDLD2m") (Pair 0 0)) {})' --burn-cap 20 --force


# list exchange contract addresses
tezos-client list known contracts | grep tezosGoldExchange
# tezosGoldExchange: KT1Cwpm9FeqEwe74REMSg4ETYVeGk1pg1ksX

# approve tezosGoldExchange to handle alice's tokens
tezos-client transfer 0 from alice to tezosGold --entrypoint 'approve' --arg 'Pair "KT1Cwpm9FeqEwe74REMSg4ETYVeGk1pg1ksX" 200' --burn-cap 1

# approve tezosGoldExchange to handle simon's tokens
tezos-client transfer 0 from simon to tezosGold --entrypoint 'approve' --arg 'Pair "KT1Cwpm9FeqEwe74REMSg4ETYVeGk1pg1ksX" 50' --burn-cap 1

# alice adds 10 XTZ and 100 TezosGold to the exchange
tezos-client transfer 1 from alice to tezosGoldExchange --entrypoint 'addLiquidity' --arg 'Pair (Pair "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" 1) (Pair 100 "2020-06-29T18:00:21Z")' --burn-cap 1

# simon adds 5 XTZ and 50 TezosGold to the exchange
tezos-client transfer 5 from simon to tezosGoldExchange --entrypoint 'addLiquidity' --arg 'Pair (Pair "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" 1) (Pair 100 "2020-06-29T18:00:21Z")' --burn-cap 1

# bob wants to buy TezosGold (xtzToToken) for himself
tezos-client transfer 2 from bob to tezosGoldExchange --entrypoint 'xtzToToken' --arg 'Pair (Pair "tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i" 1) "2020-06-29T18:00:21Z"' --burn-cap 1

# simon wants to buy XTZ (tokenToXtz) for himself
tezos-client transfer 0 from simon to tezosGoldExchange --entrypoint 'tokenToXtz' --arg 'Pair (Pair (Pair "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3") (Pair 40 1)) "2020-06-29T18:00:21Z"' --burn-cap 1

# alice burns some liquidity tokens
tezos-client transfer 0 from alice to tezosGoldExchange --entrypoint 'removeLiquidity' --arg 'Pair (Pair (Pair "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn" "tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn") (Pair 5000000 1)) (Pair 1 "2020-06-29T18:00:21Z")' --burn-cap 1

# originate tezos tacos
tezos-client originate contract tezosTacos transferring 0 from simon running ./contracts/fa1.2.tz --init 'Pair {Elt "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" (Pair {} 1000000)} 1000000' --burn-cap 5 --force
# KT1RNEHug9Vk2Vuur2Az3AaFL95cjvmJkQJC

# originate tezos tacos exchange
tezos-client originate contract tezosTacosExchange transferring 0 from alice running ./contracts/exchange.tz --init 'Pair {} (Pair (Pair (Pair None "KT1RNEHug9Vk2Vuur2Az3AaFL95cjvmJkQJC") (Pair 0 0)) {})' --burn-cap 20 --force
# KT1Lg2hnVvdkYuQ6E2DDj4MNZYzVYwstwZop

# approve tezosTacosExchange to handle simon's tokens
tezos-client transfer 0 from simon to tezosTacos --entrypoint 'approve' --arg 'Pair "KT1Lg2hnVvdkYuQ6E2DDj4MNZYzVYwstwZop" 500' --burn-cap 1

# simon adds 10 XTZ and 100 TezosGold to the tezos tacos exchange
tezos-client transfer 10 from simon to tezosTacosExchange --entrypoint 'addLiquidity' --arg 'Pair (Pair "tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3" 1) (Pair 100 "2020-06-29T18:00:21Z")' --burn-cap 1

# get data

# get LQT amount for alice
tezos-client get big map value for '"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"' of type 'address' in tezosGoldExchange

# get LQT amount for simon
tezos-client get big map value for '"tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3"' of type 'address' in tezosGoldExchange

# get TezosGold amount for alice
tezos-client get big map value for '"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"' of type 'address' in tezosGold

# get TezosGold amount for bob
tezos-client get big map value for '"tz1UqygaLR3HEPj9HaGV3QWjkFzjKM4Ri18i"' of type 'address' in tezosGold

# get TezosGold amount for simon
tezos-client get big map value for '"tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3"' of type 'address' in tezosGold

# get TezosTacos amount for alice
tezos-client get big map value for '"tz1boB5GrcAVRtjQQLrpjM4Xpp1E438La4Wn"' of type 'address' in tezosTacos

# get TezosTacos amount for simon
tezos-client get big map value for '"tz1bqV1wz5mJmBRSDkYiRZxX7yBDLKV7HKo3"' of type 'address' in tezosTacos

# get TezosGold amount held by TezosGoldExchange
tezos-client get big map value for '"KT1Cwpm9FeqEwe74REMSg4ETYVeGk1pg1ksX"' of type 'address' in tezosGold

# get TezosGold amount held by tezosTacosExchange
tezos-client get big map value for '"KT1K883bX7pf8HLpXY82hBhEgaMzYDDUTbMg"' of type 'address' in tezosTacos

# get XTZ amount
tezos-client get balance for alice
tezos-client get balance for bob
tezos-client get balance for simon
tezos-client get balance for tezosGoldExchange

# get storage for tezosGoldExchange
tezos-client get script storage for tezosGoldExchange
```
