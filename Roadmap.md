# Dexter Technical Roadmap

Dexter is an on-chain decentralized exchange for Tezos.

A list of tasks in order to achieve version 1 as describe in `Proposal.md`.

The primary dependency is `Morley`, a Haskell EDSL that compiles to Michelson.

## Smart contracts

Currently these are based on a mock ERC20 token contract written in Tezos, but
they will be adjusted to the standard when available.

- [ ] Write an exchange smart contract 
  - [ ] Tezos to token exchange
  - [ ] Token to token exchange
  - [ ] Token to tezos exchange
  - [ ] Add liquidity
  - [ ] Remove liquidity
- [ ] Write a factory smart contract to manage exchange contracts for each contract
- [ ] Unit testing
- [ ] Formal specification
- [ ] Formal verification

## Haskell to tezos-client interface (servant-tezos-client)

This is a set of type-safe functions (via servant) that communicate with the tezos-client RPC to query
data from the Tezos mainnet, alphanet or zeronet. Some routes return Michelson, ideally these can 
return Michelson as Morley. This library is also useful for 3rd parties who want to create 
Haskell projects using the tezos-client.

- [ ] Shell
- [ ] Protocol Alpha
- [ ] In routes that return Michelson, return it as Morley in Haskell

## Dexter website

The Dexter website allows you to make decentralized trades directly in the browser via Tezbox.
It also displays data about liquidity.

- [ ] dexter-types, a typed api (servant)
- [ ] dexter-servant-server, the backend for the Dexter webiste (depends on dexter-types)
- [ ] dexter-reasonml, the frontend for the dexter website (dependes directly on dexter-types, Tezbox and bs-dexter)
- [ ] Interface Tezbox to browser

## Dexter interfaces

The Dexter interfaces help 3rd parties build applications that use the Dexter.

- [ ] dexter-servant-client, a Haskell library for querying the Dexter smart contracts (depends on dexter-types).
- [ ] bs-dexter, a BuckleScript/ReasonML library for query the Dexter smart contracts (automated from dexter-types, used by dexter-reasonml).
- [ ] dexter-swagger-ui, an automated UI that helps 3rd parties build against dexter-servant-server's JSON API.
