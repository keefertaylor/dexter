module Main where

import Dexter.Store.Manager (runDexterStoreManager)

main :: IO ()
main = runDexterStoreManager

-- Make sure it is pointing to a running tezos node
-- ~/alphanet.sh start --rpc-port 9323


